#include "Algorithms.h"

#include <map>
#include <time.h>
#include <cmath>
#include <vector>
#include <iostream>

#include <omp.h>

namespace monteKarlo {

const int COUNT_ITERATION = 500;

std::vector<Point3> randoms;

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start transform MonteKarlo\n";

	unsigned int randInitiate = static_cast<unsigned int>(time(NULL));
	std::cout << "rand initiate " << randInitiate << "\n";
	srand(randInitiate);

	randoms.clear();
	for (int i = 0; i < COUNT_ITERATION; i++) {
		const float x = nextRandom();
		const float y = nextRandom();

		randoms.push_back(sc.getConversion(Point3(x, y, 0.f)));
	}

	check(randoms.size() == COUNT_ITERATION);
}

inline void work(float x0, float y0, const LinearConversion &sc, const Image &image, float &resR, float &resG, float &resB) {
	resR = 0;
	resG = 0;
	resB = 0;

	const Point3 point1 = sc.getConversion(Point3(x0, y0));

	for (int k = 0; k < COUNT_ITERATION; k++) {
		const Point3 point2 = randoms[k];

		const Point3 pointResult = point1 + point2;

		const float x = pointResult.getX();
		const float y = pointResult.getY();

		if (0.f <= x && x < image.width && 0.f <= y && y < image.height) {
			// ��� ������������� ����� ���������� (int) �������� ��� floor.
			const int xRes = (int)x;
			const int yRes = (int)y;

			resR += image.r[xRes + yRes * image.width];
			resG += image.g[xRes + yRes * image.width];
			resB += image.b[xRes + yRes * image.width];
		} else {
			// ���� ������� �� �������, �� ������ ������ �� ���������.
		}
	}

	resR /= COUNT_ITERATION;
	resG /= COUNT_ITERATION;
	resB /= COUNT_ITERATION;
}

#include "transform.inc"

}
