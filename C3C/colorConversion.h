#pragma once

float lin2Lin(float c);

float sRGB2Lin(float c);

float adobeRGB2Lin(float c);

typedef float(*loadPtr)(int);
typedef int(*savePtr)(float);

loadPtr colorConversionInitLoad(int maxValue, float(*fLoad)(float));
savePtr colorConversionInitSave(int maxValue, float(*fSave)(float));

float lin2sRGBAdobeRGB(float c);

float lin2sRGB(float c);
