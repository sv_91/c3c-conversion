set testBat=tests.bat
echo > %testBat%

set testName=normal
set notL=0
set maskRR=0.
set scaleRR=1
set angleRR=1
call :testGlobal

set testName=angleR
set notL=0
set maskRR=0.
set scaleRR=1
set angleRR=23
call :testGlobal

set testName=scaleR
set notL=0
set maskRR=0.
set scaleRR=2
set angleRR=1
call :testGlobal

set testName=maskR
set notL=0
set maskRR=1.
set scaleRR=1
set angleRR=1
call :testGlobal

set testName=notLianiarize
set notL=1
set maskRR=0.
set scaleRR=1
set angleRR=1
call :testGlobal

exit

:testGlobal
set folderOut="C:\Tests\Out\tests\%testName%\5"
set fileName=5.tif
echo MD %folderOut%  >> %testBat%
call :test

set folderOut="C:\Tests\Out\tests\%testName%\1"
set fileName=1.bmp
echo MD %folderOut% >> %testBat%
call :test

set folderOut="C:\Tests\Out\tests\%testName%\6"
set fileName=6.tif
echo MD %folderOut% >> %testBat%
call :test

set folderOut="C:\Tests\Out\tests\%testName%\7"
set fileName=7.tif
echo MD %folderOut% >> %testBat%
call :test

set folderOut="C:\Tests\Out\tests\%testName%\9"
set fileName=9.tif
echo MD %folderOut% >> %testBat%
call :test
exit /b

:test
rem C3C.exe  -m transform360 -u "all" -o %folderOut% -i "C:\Tests\In\%fileName%" -s %scaleRR% -a 0 -c %maskRR% --angle %angleRR% -l %notL%
echo C3C.exe  -m transform360 -u "all" -o %folderOut% -i "C:\Tests\In\%fileName%" -s %scaleRR% -a 2 -c %maskRR% --angle %angleRR% -l %notL% --saveAll 0 >> %testBat%
echo C3C.exe  -m transform360 -u "all" -o %folderOut% -i "C:\Tests\In\%fileName%" -s %scaleRR% -a 3 -c %maskRR% --angle %angleRR% -l %notL% --saveAll 0 >> %testBat%
echo C3C.exe  -m transform360 -u "all" -o %folderOut% -i "C:\Tests\In\%fileName%" -s %scaleRR% -a 4 -c %maskRR% --angle %angleRR% -l %notL% --saveAll 0 >> %testBat%
exit /b
