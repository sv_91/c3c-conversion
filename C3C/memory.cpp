#include "Image.h"

unsigned char* alloc(const int sizeBytes) {
	return new unsigned char[sizeBytes];
}

void memcpyArray(unsigned char* dst, unsigned char *src, int sizeBytes) {
	memcpy(dst, src, sizeBytes);
}

void freeMemory(void* memory) {
	if (memory != NULL) {
		delete[] memory;
	}
}
