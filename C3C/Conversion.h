#pragma once

#include "common.h"

#include <vector>
#include <iostream>
#include <string>

#include "matrix.h"

const std::string alg1("No focus 1");
const std::string alg2("No focus 2");
const std::string alg3("Photoshop patent");
const std::string alg4("Tochno My formula");
const std::string alg5("Formula iz diploma");

namespace SH {
class Polygon;
}

struct Point3 {
private:

	float x;
	float y;
	float z;

public:

	Point3(float x, float y)
		: x(x)
		, y(y)
		, z(1.f)
	{}

	Point3(float x, float y, float z)
		: x(x)
		, y(y)
		, z(z)
	{}

	float getX() const {
		check(z != 0.f);
		return x / z;
	}

	float getY() const {
		check(z != 0.f);
		return y / z;
	}

	float x_() const {
		return x;
	}

	float y_() const {
		return y;
	}

	float z_() const {
		return z;
	}

	Point3 operator+ (const Point3 &y) const {
		return Point3(this->x + y.x, this->y + y.y, this->z + y.z);
	}

};

struct PointMy {
	friend class SH::Polygon;
private:

	PointMy()
		: x(0)
		, y(0)
	{}

public:

	float x;
	float y;

	PointMy(float x_, float y_)
		: x(x_)
		, y(y_)
	{}

	PointMy(float x_, float y_, float z_)
		: x(x_ / z_)
		, y(y_ / z_)
	{
		check(z_ != 0.f);
	}

	inline PointMy operator- (const PointMy &y) const {
		return PointMy(this->x - y.x, this->y - y.y);
	}

	bool equals(const PointMy &second) {
		const float EPSILON = 1e-5f;
		return isEqualsRelative(this->x, second.x, EPSILON) && isEqualsRelative(this->y, second.y, EPSILON);
	}

};

struct Four {
	const float xMin;
	const float xMax;
	const float yMin;
	const float yMax;

	Four(float xMin, float xMax, float yMin, float yMax)
		: xMin(xMin)
		, xMax(xMax)
		, yMin(yMin)
		, yMax(yMax)
	{}
};

class LinearConversion;

inline Four getFourConversion(float xMin_, float yMin_, float xMax_, float yMax_, const LinearConversion &sc);

inline int getWidthConversion(int width, int height, const LinearConversion &sc);

inline int getHeightConversion(int width, int height, const LinearConversion &sc);

class LinearConversion {
private:

	std::vector<std::vector<float> > matrix;

	// ��, ����������� ������� ����� �� ��������.
	static std::vector<double> getSolve(std::vector<std::vector<double> > matrix) {
		// �������� ������� �� ��������.
		for (int i = 0; i < matrix.size(); i++) {
			check(matrix.size() == matrix[i].size() - 1);
		}

		// ������ ������.
		for (int n = 0; n < matrix.size(); n++) {
			// ����� ������������ ����������� ������ n
			if (matrix[n][n] == 0) { // ���� ������� �������.
				int max = static_cast<int>(matrix.size()) - 1;
				// ���� ��������� ������� �����.
				while (max >= 0) {
					if (matrix[max][n] == 0) {
						max--;
					} else {
						break;
					}
				}
				check(max > n); // ���� �� ��������� �� ���� ������� �������.
				// ������ �������
				std::vector<double> buf = matrix[n];
				matrix[n] = matrix[max];
				matrix[max] = buf;
			}
			double a = matrix[n][n];
			for (int k = n + 1; k < matrix.size(); k++) {
				// ���������� �� ���� �������, �������� �� ��� ������
				if (matrix[k][n] != 0) { // �� ������ �������� ������ �������, ������� ���� �� ��� 0, �� ��� ����. 
					double b = matrix[k][n];
					for (int i = n; i < matrix[k].size(); i++) {
						matrix[k][i] = matrix[k][i] * a / b - matrix[n][i];
					}
				}
			}
		}

		// �������� ������
		std::vector<double> x = std::vector<double>(matrix.size(), 0);
		for (int n = static_cast<int>(matrix.size()) - 1; n >= 0; n--) {
			double r = matrix[n][matrix[n].size() - 1]; // ��������� ������� ������.
			for (int i = static_cast<int>(matrix[n].size()) - 1 - 1; i > n; i--) {
				r -= matrix[n][i] * x[i];
			}

			x[n] = r / matrix[n][n];
		}

		return x;
	}

	static PointMy multiplie(const std::vector<std::vector<float> > &matrix, const PointMy &point) {
		return PointMy(
			matrix[0][0] * point.x + matrix[0][1] * point.y + matrix[0][2] * 1.f, 
			matrix[1][0] * point.x + matrix[1][1] * point.y + matrix[1][2] * 1.f,
			matrix[2][0] * point.x + matrix[2][1] * point.y + matrix[2][2] * 1.f
		);
	}
	
	static Point3 multiplie(const std::vector<std::vector<float> > &matrix, const Point3 &point) {
		return Point3(
			matrix[0][0] * point.x_() + matrix[0][1] * point.y_() + matrix[0][2] * point.z_(),
			matrix[1][0] * point.x_() + matrix[1][1] * point.y_() + matrix[1][2] * point.z_(),
			matrix[2][0] * point.x_() + matrix[2][1] * point.y_() + matrix[2][2] * point.z_()
			);
	}

	LinearConversion(const std::vector<std::vector<float> > &matrix_)
		: matrix(matrix_)
	{}

	LinearConversion(const PointMy &p00, const PointMy &p10, const PointMy &p11, const PointMy &p01) {
		std::vector<std::vector<double> > mS = {
			{ 0, 1, 0, 0, 0, 0, -p01.x, 0, 0, -p00.x },
			{ 0, 0, 0, 1, 0, 0, -p01.y, 0, 0, -p00.y },
			{ 0, 0, 0, 0, 0, 1, -1, 0, 0, -1 },
			{ 1, 0, 0, 0, 0, 0, 0, -p10.x, 0, -p00.x },
			{ 0, 0, 1, 0, 0, 0, 0, -p10.y, 0, -p00.y },
			{ 0, 0, 0, 0, 1, 0, 0, -1, 0, -1 },
			{ 1, 1, 0, 0, 0, 0, 0, 0, -p11.x, -p00.x },
			{ 0, 0, 1, 1, 0, 0, 0, 0, -p11.y, -p00.y },
			{ 0, 0, 0, 0, 1, 1, 0, 0, -1, -1 },
		};

		std::vector<double> solve = getSolve(mS);

		matrix = {
			{ (float)solve[0], (float)solve[1], p00.x },
			{ (float)solve[2], (float)solve[3], p00.y },
			{ (float)solve[4], (float)solve[5], 1.f }
		};

		PointMy ps00 = multiplie(matrix, PointMy(0, 0));
		check(ps00.equals(p00));

		PointMy ps01 = multiplie(matrix, PointMy(0, 1));
		check(ps01.equals(p01));

		PointMy ps10 = multiplie(matrix, PointMy(1, 0));
		check(ps10.equals(p10));

		PointMy ps11 = multiplie(matrix, PointMy(1, 1));
		check(ps11.equals(p11));
	}

	static LinearConversion make04(
		float angleRotate, float xAngleRotate, float yAngleRotate,
		float z
		) {
		logDebug("%s\n", "My formula");

		const float alpha = xAngleRotate;
		const float betta = yAngleRotate;

		std::vector<std::vector<float> > matrix1 = {
			{ std::cos(angleRotate), -std::sin(angleRotate), 0.f },
			{ std::sin(angleRotate), std::cos(angleRotate), 0.f },
			{ 0.f, 0.f, 1.f }
		};
		std::vector<std::vector<float> > matrix2 = {
			{ 1.f, 0.f, 0.f },
			{ 0.f, std::cos(xAngleRotate), -std::sin(xAngleRotate) },
			{ 0.f, std::sin(xAngleRotate), std::cos(xAngleRotate) }
		};
		std::vector<std::vector<float> > matrix3 = {
			{ std::cos(yAngleRotate), 0.f, std::sin(yAngleRotate) },
			{ 0.f, 1.f, 0.f },
			{ -std::sin(yAngleRotate), 0.f, std::cos(yAngleRotate) }
		};

		std::vector<std::vector<float> > matrix = ::multiplie(matrix2, matrix3);

		std::vector<float> x0 = ::multiplie(matrix, std::vector<float>({ 0.f, 0.f, z }));
		std::vector<std::vector<float> > matrixPreobr = {
			{ (x0[2] * std::cos(betta) + x0[0] * std::cos(alpha) * std::sin(betta)), -x0[0] * std::sin(alpha), 0 },
			{ (x0[2] * std::sin(alpha) * std::sin(betta) + x0[1] * std::cos(alpha) * std::sin(betta)), (x0[2] * std::cos(alpha) - x0[1] * std::sin(alpha)), 0 },
			{ std::cos(alpha) * std::sin(betta), -std::sin(alpha), x0[2] }
		};
		const LinearConversion transformConversion = LinearConversion::make(::multiplie(matrixPreobr, matrix1));

		return transformConversion;
	}
	
	static LinearConversion make03(
		float angleRotate, float xAngleRotate, float yAngleRotate,
		float z,
		float xMin, float yMin,
		int width, int height
		) {
		logDebug("%s\n", "Photoshop patent");

		std::vector<std::vector<float> > k = {
			{ z, 0, -xMin },
			{ 0, z, -yMin },
			{ 0, 0, 1 }
		};

		std::vector<std::vector<float> > matrix1 = {
			{ std::cos(angleRotate), -std::sin(angleRotate), 0.f },
			{ std::sin(angleRotate), std::cos(angleRotate), 0.f },
			{ 0.f, 0.f, 1.f }
		};
		std::vector<std::vector<float> > matrix2 = {
			{ 1.f, 0.f, 0.f },
			{ 0.f, std::cos(xAngleRotate), -std::sin(xAngleRotate) },
			{ 0.f, std::sin(xAngleRotate), std::cos(xAngleRotate) }
		};
		std::vector<std::vector<float> > matrix3 = {
			{ std::cos(yAngleRotate), 0.f, std::sin(yAngleRotate) },
			{ 0.f, 1.f, 0.f },
			{ -std::sin(yAngleRotate), 0.f, std::cos(yAngleRotate) }
		};

		std::vector<std::vector<float> > matrixR = ::multiplie(matrix2, ::multiplie(matrix3, matrix1));

		const LinearConversion res1(::multiplie(k, ::getObr(::multiplie(k, matrixR))));

		float xMinNew = getFourConversion(0.f, 0.f, static_cast<float>(width), static_cast<float>(height), res1.getObr()).xMin;
		float yMinNew = getFourConversion(0.f, 0.f, static_cast<float>(width), static_cast<float>(height), res1.getObr()).yMin;

		std::vector<std::vector<float> > c1s = {
			{ 1, 0, xMinNew },
			{ 0, 1, yMinNew },
			{ 0, 0, 1 }
		};

		return LinearConversion(::multiplie(res1.getMatrix(), c1s));
	}

	static LinearConversion make04(
		float angleRotate, float xAngleRotate, float yAngleRotate,
		float z,
		float xMin, float yMin,
		int width, int height
		) {
		return make04(angleRotate, xAngleRotate, yAngleRotate, z).toImage(xMin, yMin, width, height);
	}

	static LinearConversion make01(
		float angleRotate, float xAngleRotate, float yAngleRotate,
		float z,
		float xMin, float yMin,
		int width, int height
		) {
		logDebug("%s\n", "No focus 1");

		PointMy p00(0, 0);
		PointMy p10(1, 0 + yAngleRotate / height);
		PointMy p01(0 + xAngleRotate / width, 1);
		PointMy p11(1 - xAngleRotate / width, 1 - yAngleRotate / height);

		std::vector<std::vector<float> > matrix1 = {
			{ std::cos(angleRotate), -std::sin(angleRotate), 0.f },
			{ std::sin(angleRotate), std::cos(angleRotate), 0.f },
			{ 0.f, 0.f, 1.f }
		};

		LinearConversion conv1(p00, p10, p11, p01);

		const LinearConversion &result = LinearConversion::make(::multiplie(conv1.getMatrix(), matrix1));

		return result.toImage(xMin, yMin, width, height);
	}

	static LinearConversion make42(
		float angleRotate,
		int height
		) {
		angleRotate /= height;
		PointMy p00(
			static_cast<float>(-0.5 * std::cos(angleRotate) - 0 * std::sin(angleRotate) + 0.5), 
			static_cast<float>(-0.5 * std::sin(angleRotate) + 0 * std::cos(angleRotate))
		);
		PointMy p01(0.f, 1.f);
		PointMy p10(
			static_cast<float>(0.5 * std::cos(angleRotate) - 0 * std::sin(angleRotate) + 0.5), 
			static_cast<float>(0.5 * std::sin(angleRotate) + 0 * std::cos(angleRotate))
		);
		PointMy p11(1.f, 1.f);

		return LinearConversion(p00, p10, p11, p01);
	}

	static LinearConversion make41(
		float angleRotate,
		int width
		) {
		angleRotate /= width;
		PointMy p00(0.f, 0.f);
		PointMy p01(0.f, 1.f);
		PointMy p10(
			static_cast<float>(0 * std::cos(angleRotate) + 0.5 * std::sin(angleRotate) + 1), 
			static_cast<float>(0 * std::sin(angleRotate) - 0.5 * std::cos(angleRotate) + 0.5)
		);
		PointMy p11(
			static_cast<float>(0 * std::cos(angleRotate) - 0.5 * std::sin(angleRotate) + 1), 
			static_cast<float>(0 * std::sin(angleRotate) + 0.5 * std::cos(angleRotate) + 0.5)
		);

		return LinearConversion(p00, p10, p11, p01);
	}

	static LinearConversion make02(
		float angleRotate, float xAngleRotate, float yAngleRotate,
		float z,
		float xMin, float yMin,
		int width, int height
		) {
		logDebug("%s\n", "No focus 2");

		std::vector<std::vector<float> > matrix1 = {
			{ std::cos(angleRotate), -std::sin(angleRotate), 0.f },
			{ std::sin(angleRotate), std::cos(angleRotate), 0.f },
			{ 0.f, 0.f, 1.f }
		};

		std::vector<std::vector<float> > matrix2 = make41(xAngleRotate, width).getMatrix();
		std::vector<std::vector<float> > matrix3 = make42(yAngleRotate, height).getMatrix();

		std::vector<std::vector<float> > matrixResult = ::multiplie(matrix2, ::multiplie(matrix3, matrix1));

		return LinearConversion::make(matrixResult).toImage(xMin, yMin, width, height);
	}

	static LinearConversion make05(
		float angleRotate, float xAngleRotate, float yAngleRotate,
		float z,
		float xMin, float yMin,
		int width, int height
		) {
		logDebug("%s\n", "iz diploma");

		std::vector<std::vector<float> > matrix1 = {
			{ std::cos(angleRotate), -std::sin(angleRotate), 0.f },
			{ std::sin(angleRotate), std::cos(angleRotate), 0.f },
			{ 0.f, 0.f, 1.f }
		};

		std::vector<std::vector<float> > matrix2 = {
			{ 1.f , 0.f, 0.f },
			{ 0.f, 1.f, 0.f },
			{ yAngleRotate / height, xAngleRotate / width, 1.f }
		};

		std::vector<std::vector<float> > matrixResult = ::multiplie(matrix2, matrix1);

		return LinearConversion::make(matrixResult).toImage(xMin, yMin, width, height);
	}

	void printMatrix() {
		std::cout << "matrix\n";
		for (int i = 0; i < matrix.size(); i++) {
			for (int j = 0; j < matrix[i].size(); j++) {
				std::cout << matrix[i][j] << " ";
			}
			std::cout << "\n";
		}
	}

public:
	
	static LinearConversion make(const PointMy &p00, const PointMy &p10, const PointMy &p11, const PointMy &p01) {
		return LinearConversion(p00, p10, p11, p01);
	}

	static LinearConversion make(float a00, float a01, float a10, float a11) {
		std::vector<std::vector<float> > matrix = {
			{a00, a01, 0.f},
			{a10, a11, 0.f},
			{0.f, 0.f, 1.f}
		};
		return LinearConversion(matrix);
	}

	static LinearConversion getRotate(float alpha) {
		return make(std::cos(alpha), std::sin(alpha), -std::sin(alpha), std::cos(alpha));
	}

	static LinearConversion getMultiplier(float multiplieX, float multiplieY) {
		return make(multiplieX, 0, 0, multiplieY);
	}

	static LinearConversion make(const std::vector<std::vector<float> > &matrix) {
		return LinearConversion(matrix);
	}

	PointMy getConversion(float x, float y) const {
		return multiplie(matrix, PointMy(x, y));
	}

	Point3 getConversion(const Point3 &p) const {
		return multiplie(matrix, p);
	}

	LinearConversion getObr() const {
		return LinearConversion(::getObr(this->matrix));
	}

	std::vector<std::vector<float> > getMatrix() const {
		return matrix;
	}

	LinearConversion toImage(float xMin, float yMin, int width, int height) const {
		std::vector<std::vector<float> > mObr = this->getObr().getMatrix();
		std::vector<std::vector<float> > c1 = {
			{ 1, 0, xMin },
			{ 0, 1, yMin },
			{ 0, 0, 1 }
		};

		const LinearConversion forImage = LinearConversion::make(::multiplie(mObr, c1));

		const int widthNew = ::getWidthConversion(width, height, forImage);
		const int heightNew = ::getHeightConversion(width, height, forImage);

		std::cout << widthNew << " " << heightNew << "\n";

		const float xMinNew = ::getFourConversion(0.f, 0.f, static_cast<float>(width), static_cast<float>(height), forImage).xMin;
		const float yMinNew = ::getFourConversion(0.f, 0.f, static_cast<float>(width), static_cast<float>(height), forImage).yMin;

		std::cout << "xMinNew " << xMinNew << " " << yMinNew << "\n";

		std::vector<std::vector<float> > c2 = {
			{ 1, 0, xMinNew },
			{ 0, 1, yMinNew },
			{ 0, 0, 1 }
		};

		std::vector<std::vector<float> > c1s = {
			{ 1, 0, -xMin },
			{ 0, 1, -yMin },
			{ 0, 0, 1 }
		};

		const LinearConversion forProcede = LinearConversion::make(::multiplie(c1s, ::multiplie(this->getMatrix(), c2)));

		return forProcede;
	}

	static LinearConversion make(
		float angleRotate, float xAngleRotate, float yAngleRotate,
		float z,
		float xMin, float yMin,
		int width, int height,
		const std::string &algorithmName
		) {
		if (algorithmName == alg3) {
			return make03(-angleRotate, xAngleRotate, yAngleRotate, z, xMin, yMin, width, height);
		} else if (algorithmName == alg4) {
			return make04(angleRotate, xAngleRotate, yAngleRotate, z, xMin, yMin, width, height);
		} else if (algorithmName == alg1) {
			return make01(angleRotate, -xAngleRotate, yAngleRotate, z, xMin, yMin, width, height);
		} else if (algorithmName == alg2) {
			return make02(angleRotate, -xAngleRotate, yAngleRotate, z, xMin, yMin, width, height);
		} else if (algorithmName == alg5) {
			return make05(angleRotate, -xAngleRotate, yAngleRotate, z, xMin, yMin, width, height);
		} else {
			check(false);
		}
	}

};

inline Four getFourConversion(float xMin_, float yMin_, float xMax_, float yMax_, const LinearConversion &sc) {
	const PointMy p1 = sc.getConversion(xMin_, yMin_);
	const PointMy p2 = sc.getConversion(xMin_, yMax_);
	const PointMy p3 = sc.getConversion(xMax_, yMax_);
	const PointMy p4 = sc.getConversion(xMax_, yMin_);

	const float xMin = std::fminf(p1.x, std::fminf(p2.x, std::fminf(p3.x, p4.x)));
	const float xMax = std::fmaxf(p1.x, std::fmaxf(p2.x, std::fmaxf(p3.x, p4.x)));
	const float yMin = std::fminf(p1.y, std::fminf(p2.y, std::fminf(p3.y, p4.y)));
	const float yMax = std::fmaxf(p1.y, std::fmaxf(p2.y, std::fmaxf(p3.y, p4.y)));

	return Four(xMin, xMax, yMin, yMax);
}

inline float getWidthConversionFloat(int width, int height, const LinearConversion &sc) {
	// �������������� ����� ������ �� width � height, ��� ��� ����������� �� �� ������ ��������, � �����.
	Four four = getFourConversion(0.f, 0.f, static_cast<float>(width), static_cast<float>(height), sc);

	return std::abs(four.xMax - four.xMin);
}

inline float getHeightConversionFloat(int width, int height, const LinearConversion &sc) {
	Four four = getFourConversion(0.f, 0.f, static_cast<float>(width), static_cast<float>(height), sc);

	return std::abs(four.yMax - four.yMin);
}

inline int getWidthConversion(int width, int height, const LinearConversion &sc) {
	return static_cast<int>(std::ceil(getWidthConversionFloat(width, height, sc)));
}

inline int getHeightConversion(int width, int height, const LinearConversion &sc) {
	return static_cast<int>(std::ceil(getHeightConversionFloat(width, height, sc)));
}
