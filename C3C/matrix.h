#pragma once

#include <vector>
#include "common.h"

inline std::vector<float> multiplie(const std::vector<std::vector<float> > &matrix, const std::vector<float> &vect) {
	check(vect.size() == matrix.size());
	std::vector<float> result(vect.size(), 0.f);
	for (int i = 0; i < result.size(); i++) {
		for (int j = 0; j < result.size(); j++) {
			result[i] += matrix[i][j] * vect[j];
		}
	}
	return result;
}

inline std::vector<std::vector<float> > multiplie(const std::vector<std::vector<float> > &matrix1, const std::vector<std::vector<float> > &matrix2) {
	std::vector<std::vector<float> > result(matrix1.size(), std::vector<float>(matrix2[0].size(), 0.f));
	for (int i = 0; i < result.size(); i++) {
		for (int j = 0; j < result[i].size(); j++) {
			for (int k = 0; k < matrix1[i].size(); k++) {
				result[i][j] += matrix1[i][k] * matrix2[k][j];
			}
		}
	}
	return result;
}

static double getDet(const std::vector<std::vector<float> > &matrix) {
	check(matrix.size() == 3);
	for (int i = 0; i < matrix.size(); i++) {
		check(matrix[i].size() == 3);
	}

	return
		(double)matrix[0][0] * matrix[1][1] * (double)matrix[2][2]
		+ (double)matrix[0][1] * matrix[1][2] * (double)matrix[2][0]
		+ (double)matrix[1][0] * matrix[2][1] * (double)matrix[0][2]
		- (double)matrix[0][2] * matrix[1][1] * (double)matrix[2][0]
		- (double)matrix[0][1] * matrix[1][0] * (double)matrix[2][2]
		- (double)matrix[1][2] * matrix[2][1] * (double)matrix[0][0];
}

static double getDet(float a11, float a12, float a21, float a22) {
	return (double)a11 * a22 - (double)a12 * a21;
}

static void checkEMultiplie(const std::vector<std::vector<float> > &matrix1, const std::vector<std::vector<float> > &matrix2) {
	check(matrix1[0].size() == matrix2.size());
	std::vector<std::vector<double> > result(matrix1.size(), std::vector<double>(matrix2[0].size(), 0.f));
	for (int i = 0; i < matrix1.size(); i++) {
		for (int j = 0; j < matrix2[0].size(); j++) {
			double res = 0.f;
			for (int k = 0; k < matrix1[i].size(); k++) {
				res += (double)matrix1[i][k] * matrix2[k][j];
			}
			result[i][j] = res;
		}
	}

	for (int i = 0; i < result.size(); i++) {
		const float EPSILON = 1e-3f;
		for (int j = 0; j < result[i].size(); j++) {
			if (i == j) {
				check(isEqualsRelative((float)result[i][j], 1.f, EPSILON));
			} else {
				check(isEqualsRelative((float)result[i][j], 0.f, EPSILON));
			}
		}
	}
}

static std::vector<std::vector<float> > getObr(const std::vector<std::vector<float> > &matrix) {
	const float EPSILON = 1e-4f;
	double det = getDet(matrix);
	check(!isEqualsRelative((float)det, 0.f, EPSILON));
	std::vector<std::vector<float> > result({
		{ (float)(getDet(matrix[1][1], matrix[1][2], matrix[2][1], matrix[2][2]) / det),
		(float)(-getDet(matrix[0][1], matrix[0][2], matrix[2][1], matrix[2][2]) / det),
		(float)(getDet(matrix[0][1], matrix[0][2], matrix[1][1], matrix[1][2]) / det) },

		{ (float)(-getDet(matrix[1][0], matrix[1][2], matrix[2][0], matrix[2][2]) / det),
		(float)(getDet(matrix[0][0], matrix[0][2], matrix[2][0], matrix[2][2]) / det),
		(float)(-getDet(matrix[0][0], matrix[0][2], matrix[1][0], matrix[1][2]) / det) },

		{ (float)(getDet(matrix[1][0], matrix[1][1], matrix[2][0], matrix[2][1]) / det),
		(float)(-getDet(matrix[0][0], matrix[0][1], matrix[2][0], matrix[2][1]) / det),
		(float)(getDet(matrix[0][0], matrix[0][1], matrix[1][0], matrix[1][1]) / det) }
	});

	//checkEMultiplie(result, matrix);

	return result;
}
