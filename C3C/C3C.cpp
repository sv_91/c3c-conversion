#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <math.h>
#include <iostream>
#include "EasyBMP.h"
#include <map>
#include <ctime>
#include <time.h>
#include <algorithm>

#include "Algorithms.h"

#include "Image.h"
#include "common.h"
#include "colorConversion.h"
#include "Conversion.h"

#include "tiffio.h"

enum FileType {
	BMP_FILe,
	TIFF_File
};

FileType getFileType(const std::string &fileName) {
	int pos = static_cast<int>(fileName.rfind("."));
	std::string sub = fileName.substr(pos + 1);
	std::transform(sub.begin(), sub.end(), sub.begin(), ::tolower);
	if (sub == "bmp") {
		return BMP_FILe;
	} else if (sub == "tif" || sub == "tiff") {
		return TIFF_File;
	} else {
		throw std::string("sub == \"bmp\" || sub == \"tif\" || sub == \"tiff\"");
	}
}

FileType fileType;
int algorithmNumber;
std::string postfixFolder;
std::string unsharpMaskFlag;
float unsharpCoeff;
int angleRotation;
bool notLinearizing;
bool saveAll;
bool skipFirst;

Image loadImage(const std::string &fileName, FileType ft) {
	float(*funcLinearize)(float);
	if (notLinearizing) {
		funcLinearize = lin2Lin;
	} else {
		funcLinearize = sRGB2Lin;
	}

	if (ft == FileType::BMP_FILe) {
		BMP img;
		img.ReadFromFile(fileName.c_str());
		int width = img.TellWidth();
		int height = img.TellHeight();
		long long sizeImageG = width * height;

		const int MAX_VALUE = 256;

		loadPtr sRGB2Lin = colorConversionInitLoad(MAX_VALUE, funcLinearize);

		Image image(width, height);
		//#pragma omp parallel for
		for (int x = 0; x < img.TellWidth(); x++){
			for (int y = 0; y < img.TellHeight(); y++){
				image.setColors(
					x, y, 
					sRGB2Lin(img.GetPixel(x, y).Red), 
					sRGB2Lin(img.GetPixel(x, y).Green), 
					sRGB2Lin(img.GetPixel(x, y).Blue)
				);
			}
		}

		return image;
	} else {
		TIFF* tif = TIFFOpen(fileName.c_str(), "r");
		if (tif) {
			uint32 imagelength, imagewidth;
			tdata_t buf;
			uint16 config, nsamples;

			TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
			TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imagewidth);
			TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &config);
			TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &nsamples);
			
			Image image(imagewidth, imagelength);

			const int MAX_VALUE = 256 * 256 - 1;

			loadPtr sRGB2Lin = colorConversionInitLoad(MAX_VALUE, funcLinearize);

			buf = _TIFFmalloc(TIFFScanlineSize(tif));
			check(config == PLANARCONFIG_CONTIG)
			//#pragma omp parallel for
			for (int row = 0; row < static_cast<int>(imagelength); row++){
				TIFFReadScanline(tif, buf, row);

				for (int col = 0; col < static_cast<int>(imagewidth); col++){
					check(nsamples == 3);
					uint16 red = static_cast<uint16*>(buf)[col*nsamples + 0];
					uint16 green = static_cast<uint16*>(buf)[col*nsamples + 1];
					uint16 blue = static_cast<uint16*>(buf)[col*nsamples + 2];
					image.setColors(
						col, row, 
						sRGB2Lin(red), 
						sRGB2Lin(green), 
						sRGB2Lin(blue)
					);
				}
			}
			_TIFFfree(buf);
			TIFFClose(tif);

			return image;
		} else {
			throw std::string("Unknown tiff error");
		}
	}
}

void save(const std::string &fileName, const Image &image, FileType ft) {
	float(*funcLinearize)(float);
	if (notLinearizing) {
		funcLinearize = lin2Lin;
	} else {
		funcLinearize = lin2sRGB;
	}

	if (ft == FileType::BMP_FILe) {
		BMP imgOut;
		imgOut.SetSize(image.getWidth(), image.getHeight());
		const int MAX_VALUE = 256 - 1;

		savePtr Lin2sRGBSave = colorConversionInitSave(MAX_VALUE, funcLinearize);

		//#pragma omp parallel for
		for (int x = 0; x < image.getWidth(); x++){
			for (int y = 0; y < image.getHeight(); y++){
				RGBApixel pixel;
				pixel.Red = Lin2sRGBSave(image.getRed(x, y));
				pixel.Green = Lin2sRGBSave(image.getGreen(x, y));
				pixel.Blue = Lin2sRGBSave(image.getBlue(x, y));
				imgOut.SetPixel(x, y, pixel);
			}
		}
		imgOut.WriteToFile((fileName + ".bmp").c_str());
	} else {
		const int MAX_VALUE = 256 * 256 - 1;

		savePtr Lin2sRGBSave = colorConversionInitSave(MAX_VALUE, funcLinearize);

		TIFF *tif = TIFFOpen((fileName + ".tiff").c_str(), "w");
		TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, image.getWidth());
		TIFFSetField(tif, TIFFTAG_IMAGELENGTH, image.getHeight());
		TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 3);
		TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 16);
		TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
		TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
		TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
		//TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_LZW);

		uint16 *dataArray = new uint16[3 * image.getWidth()];

		//#pragma omp parallel for
		for (int y = 0; y < image.getHeight(); y++){
			for (int x = 0; x < image.getWidth(); x++) {
				dataArray[3 * x + 0] = Lin2sRGBSave(image.getRed(x, y));
				dataArray[3 * x + 1] = Lin2sRGBSave(image.getGreen(x, y));
				dataArray[3 * x + 2] = Lin2sRGBSave(image.getBlue(x, y));
			}
			TIFFWriteScanline(tif, &dataArray[0], y, 0);
		}
		delete[] dataArray;
		
		TIFFClose(tif);
	}
}

inline Image transform(const Image &image1, const LinearConversion &sc) {
	if (algorithmNumber == 1) {
		return monteKarlo::transform(image1, sc);
	} else if (algorithmNumber == 2) {
		return SH::transform(image1, sc);
	} else if (algorithmNumber == 3) {
		return bilinearInterpolation::transform(image1, sc);
	} else if (algorithmNumber == 4) {
		return bicubicInterpolation::transform(image1, sc);
	} else if (algorithmNumber == 5) {
		return MKNormal4::transform(image1, sc);
	} else if (algorithmNumber == 6) {
		return sinc::transform(image1, sc);
	} else {
		throw std::string("Unknown mode");
	}
}

void setPostfixFolder() {
	if (algorithmNumber == 0) {
		postfixFolder = "MKLinear";
	} else if (algorithmNumber == 1) {
		postfixFolder = "MK";
	} else if (algorithmNumber == 2) {
		postfixFolder = "SH";
	} else if (algorithmNumber == 3) {
		postfixFolder = "LinearInt";
	} else if (algorithmNumber == 4) {
		postfixFolder = "BicubickInt";
	} else if (algorithmNumber == 5) {
		postfixFolder = "MKNormal";
	} else if (algorithmNumber == 6) {
		postfixFolder = "sinc";
	} else {
		throw std::string("Unknown mode");
	}
}

Image transform360(Image &image, const std::string &folder) {
	std::string fileNameOutPrefix = folder;

	const float PI = 3.141592653589793238463f;

	const LinearConversion transformConversion = LinearConversion::getRotate((angleRotation * 2.f * PI) / 360.f);
	for (int count = 0; count < 360; count += 1) {
		std::cout << "Iterations: " << count + 1 << "\n";
		double tt1 = clock();
		image = transform(image, transformConversion.toImage(image.xMin, image.yMin, image.width, image.height));
		double tt2 = clock();
		std::cout << tt2 - tt1 << "\n";

		image.crop();

		if (unsharpMaskFlag == "all") {
			unsharpMask(image.width, image.height, image.r, unsharpCoeff);
			unsharpMask(image.width, image.height, image.g, unsharpCoeff);
			unsharpMask(image.width, image.height, image.b, unsharpCoeff);
		}

		if (notLinearizing) {
			int maxValue;
			if (fileType == FileType::BMP_FILe) {
				maxValue = 256 - 1;
			} else {
				maxValue = 256 * 256 - 1;
			}
			image.roundArr(maxValue);
		}

		// ���������� �����������.
		if (!saveAll) {
			if (count != 0 && count != 24 && count != 44 && count != 89 && count != 179) {
				continue;
			}
		}
		save(fileNameOutPrefix + intToStr(count + 1), image, fileType);
	}

	if (unsharpMaskFlag == "one") {
		unsharpMask(image.width, image.height, image.r, unsharpCoeff);
		unsharpMask(image.width, image.height, image.g, unsharpCoeff);
		unsharpMask(image.width, image.height, image.b, unsharpCoeff);
	}

	return image;
}

Image test(Image &image) {
	const float PI = 3.141592653589793238463f;

	std::cout << image.xMin << " " << image.yMin << "\n";
	//const SquareConversion transformConversion = SquareConversion::make(PointMy(0, 0), PointMy(1, 0), PointMy(1.0001f, 0.9999f), PointMy(-.0001f, 1));
	const LinearConversion transformConversion = LinearConversion::make(0.f, 0.411430, 0.f,
		2589.849121,
		image.xMin, image.yMin,
		image.width, image.height,
		alg3);
	//const SquareConversion transformConversion = SquareConversion::make(PointMy(0, 0), PointMy(1, 0), PointMy(1.001f, 0.999f), PointMy(-.001f, 1));
	const LinearConversion transformC = transformConversion;// .getObr();

	image.xMin = -image.width / 3.f;

	if (skipFirst) {
		double tt1 = clock();
		Image imageOut = transform(image, transformConversion);
		double tt2 = clock();
		std::cout << "First " << tt2 - tt1 << "\n";
	}
	double tt1 = clock();
	Image imageOut = transform(image, transformConversion);
	double tt2 = clock();
	std::cout << tt2 - tt1 << "\n";

	if (unsharpMaskFlag == "all" || unsharpMaskFlag == "one") {
		unsharpMask(imageOut.width, imageOut.height, imageOut.r, unsharpCoeff);
		unsharpMask(imageOut.width, imageOut.height, imageOut.g, unsharpCoeff);
		unsharpMask(imageOut.width, imageOut.height, imageOut.b, unsharpCoeff);
	} else {
		check(unsharpMaskFlag == "none")
	}

	return imageOut;
}

void solve(const std::string &fileName, const std::string &folder, const std::string &mode, const int incSize) {
	std::string fileNameOut = folder + "out";

	Image image = loadImage(fileName, fileType);

	if (incSize != 1) {
		image.increraseSize(incSize);
	}
	// ������������
	if (mode == "test") {
		image = test(image);
	} else if (mode == "transform360") {
		image = transform360(image, fileNameOut);
	} else {
		throw std::string("error mode ") + mode;
	}
	if (incSize != 1) {
		const LinearConversion sc = LinearConversion::getMultiplier(static_cast<float>(incSize), static_cast<float>(incSize));
		image = SH::transform(image, sc);
	}
	save(fileNameOut, image, fileType);
}

#include "cmdline.h"
#include <direct.h>

void parseParams(int argc, char *argv[]) {
	gengetopt_args_info ai;
	if (cmdline_parser(argc, argv, &ai) != 0) {
		exit(1);
	}

	skipFirst = true;
	algorithmNumber = ai.algorithm_arg;
	setPostfixFolder();
	const std::string folderOut = std::string(ai.folderOut_arg) + "/" + postfixFolder + "/";

	unsharpMaskFlag = ai.unsharp_arg;
	unsharpCoeff = ai.unsharpCoeff_arg;
	angleRotation = ai.angle_arg;
	notLinearizing = ai.notLinearizing_arg == 1;
	saveAll = ai.saveAll_arg == 1;

	int result = _mkdir(folderOut.c_str());

	fileType = getFileType(ai.filenameIn_arg);

	solve(ai.filenameIn_arg, folderOut, ai.mode_arg, ai.resize_arg);
}

void testRun(int argc, char *argv[]) {
	/*
	} else if (algorithmNumber == 1) {
	postfixFolder = "MK";
	} else if (algorithmNumber == 2) {
	postfixFolder = "SH";
	} else if (algorithmNumber == 3) {
	postfixFolder = "LinearInt";
	} else if (algorithmNumber == 4) {
	postfixFolder = "BicubickInt";
	} else if (algorithmNumber == 5) {
	postfixFolder = "MKNormal";
	} else if (algorithmNumber == 6) {
	postfixFolder = "sinc";
	*/
	skipFirst = false;
	algorithmNumber = 4;
	setPostfixFolder();
	const std::string folderOut = "C:/� ����� D/����������/1 �������/������/Out/";
	//const std::string fileIn = "C:/� ����� D/����������/1 �������/������/In/1.bmp";
	const std::string fileIn = "C:/� ����� D/����������/1 �������/������/In/_2.bmp";
	//const std::string fileIn = "C:/� ����� D/����������/1 �������/������/In/9.tif";
	//std::string mode = "transform360";
	std::string mode = "test";
	int resize = 1;
	unsharpMaskFlag = "none";
	unsharpCoeff = 0;
	angleRotation = 1;
	notLinearizing = false;
	saveAll = false;

	int result = _mkdir(folderOut.c_str());

	fileType = getFileType(fileIn);

	solve(fileIn, folderOut, mode, resize);
}

int main(int argc, char *argv[]) {
	try {
		std::cout << "max threads " << omp_get_max_threads() << "\n";
		omp_set_num_threads(omp_get_max_threads());

		//parseParams(argc, argv);
		testRun(argc, argv);
		return 0;
	} catch (std::string &e) {
		std::cout << "Error! " <<  e << "\n";
		return 1;
	}
}
