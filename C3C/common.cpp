#include "common.h"

#include <stdarg.h>

#ifndef LOG_DEBUG_NO
void createFile(const std::string &s) {
}
#else
void createFile(const std::string &s) {
}
#endif

#ifndef LOG_DEBUG_NO
void logDebug(const char *format, ...) {
	va_list vl;
	va_start(vl, format);
	vprintf(format, vl);
	va_end(vl);
}
#else 
void logDebug(const char *format, ...) {}
#endif
