#include <math.h>
#include <algorithm>
#include <vector>
#include <map>
#include "common.h"
#include "colorConversion.h"

typedef std::pair<int, float(*)(float)> MapPair;

std::map<MapPair, std::vector<float> > colorsMapLoad;

std::map<MapPair, std::vector<unsigned short> > colorsMapSave;

const float* ptrLoad;
int lenLoad;
const unsigned short* ptrSave;
int lenSave;

float lin2Lin(float c) {
	return c;
}

float sRGB2Lin(float c) {
	if (c <= 0.04045f) {
		return c / 12.92f;
	} else {
		return pow((c + 0.055f) / 1.055f, 2.4f);
	}
}

float adobeRGB2Lin(float c) {
	return pow(c, 2.19921875f);
}

void genLoadMap(int maxValue, float(*f)(float)) {
	std::vector<float> &mapColorsLoad = colorsMapLoad[MapPair(maxValue, f)];
	if (mapColorsLoad.size() != 0) {
		logDebug("get mapColorsLoad\n");
		check(mapColorsLoad.size() == maxValue + 1);
		return;
	} else {
		logDebug("Initialize mapColorsLoad %d %ld\n", maxValue + 1, f);
	}

	mapColorsLoad.resize(maxValue + 1);

	for (std::map<MapPair, std::vector<float> >::const_iterator it = colorsMapLoad.begin(); it != colorsMapLoad.end(); ++it) {
		logDebug("Map elemLoad %ld\n", it->second.size());
	}

#pragma omp parallel for
	for (int color = 0; color <= maxValue; color++) {
		float c = color / (float)maxValue;
		mapColorsLoad[color] = f(c);
	}

	ptrLoad = mapColorsLoad.data();
	lenLoad = static_cast<int>(mapColorsLoad.size());
}

void genSaveMap(int maxValue, float(*f)(float)) {
	logDebug("maxValue %d %d\n", maxValue, (1 << (sizeof(short)* 8)) - 1);
	check(maxValue <= (1 << (sizeof(short)* 8)) - 1);

	std::vector<unsigned short> &mapColorsSave = colorsMapSave[MapPair(maxValue, f)];
	const int arraySize = 50000 + 1;

	if (mapColorsSave.size() != 0) {
		logDebug("get mapColorsSave\n");
		check(mapColorsSave.size() == arraySize);
		return;
	} else {
		logDebug("Initialize mapColorsSave %d %ld\n", maxValue, f);
	}

	mapColorsSave.resize(arraySize);

	for (std::map<MapPair, std::vector<unsigned short> >::const_iterator it = colorsMapSave.begin(); it != colorsMapSave.end(); ++it) {
		logDebug("Map elemSave %ld\n", it->second.size());
	}

#pragma omp parallel for
	for (int color = 0; color < mapColorsSave.size(); color++) {
		float c = static_cast<float>(color) / (mapColorsSave.size() - 1);
		int result = static_cast<int>(std::round(f(c) * maxValue));
		mapColorsSave[color] = std::max(0, std::min(maxValue, result));
	}

	// ������� ������������ ��������.
	int maxInt = 0;
	int sumRazn = 0;
	for (int color = 0; color < mapColorsSave.size(); color++) {
		float c1 = static_cast<float>(color) / (mapColorsSave.size() - 1);
		int result1 = static_cast<int>(f(c1) * maxValue);
		int result11 = std::max(0, std::min(maxValue, result1));

		float c2 = static_cast<float>(color + 1) / (mapColorsSave.size() - 1);
		int result2 = static_cast<int>(f(c2) * maxValue);
		int result22 = std::max(0, std::min(maxValue, result2));

		int result = mapColorsSave[color];

		int max1 = std::max(std::abs(result - result22), std::abs(result - result11));
		maxInt = std::max(maxInt, max1);
		sumRazn += max1;
	}

	logDebug("Max razn %d %d\n", maxInt, sumRazn);

	ptrSave = mapColorsSave.data();
	lenSave = static_cast<int>(mapColorsSave.size());
}

float sRGB2Lin(int color) {
	return ptrLoad[color];
};

int Lin2sRGBSave(float c) {
	clamp(c);
	return ptrSave[static_cast<int>(c * (lenSave - 1))];
};

loadPtr colorConversionInitLoad(int maxValue, float(*fLoad)(float)) {
	genLoadMap(maxValue, fLoad);
	return sRGB2Lin;
}

savePtr colorConversionInitSave(int maxValue, float(*fSave)(float)) {
	genSaveMap(maxValue, fSave);
	return Lin2sRGBSave;
}

float lin2sRGBAdobeRGB(float c) {
	return pow(c, 1.0f / 2.19921875f);
};

float lin2sRGB(float c) {
	if (c <= 0.0031308f) {
		return 12.92f*c;
	} else {
		return 1.055f*pow(c, 1.0f / 2.4f) - 0.055f;
	}
};
