#pragma once

#include "Image.h"
#include "common.h"
#include "Conversion.h"

void unsharpMask(int width, int height, float *arr, float sharpIndex);

const std::string bilinearStr("Bilinear");
const std::string bicubickStr("Bicubic");
const std::string monteKarloStr("C3C algorithm (Monte - Carlo)");
const std::string SazerlHodjmenStr("C3C algorithm(Sutherland�Hodgman)");
const std::string sincStr("Lanczos");

namespace bilinearInterpolation {
Image transform(const Image &image1, const LinearConversion &sc);
}

namespace bicubicInterpolation {
Image transform(const Image &image1, const LinearConversion &sc);
}

namespace monteKarlo {
Image transform(const Image &image1, const LinearConversion &sc);
}

namespace MKNormal1 {
Image transform(const Image &image1, const LinearConversion &sc);
}
namespace MKNormal2 {
Image transform(const Image &image1, const LinearConversion &sc);
}
namespace MKNormal3 {
Image transform(const Image &image1, const LinearConversion &sc);
}
namespace MKNormal4 {
Image transform(const Image &image1, const LinearConversion &sc);
}

namespace SH {
Image transform(const Image &image1, const LinearConversion &sc);
}

namespace sinc {
Image transform(const Image &image1, const LinearConversion &sc);
}
