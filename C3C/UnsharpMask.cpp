#include "Image.h"

#include <omp.h>

void unsharpMask(int width, int height, float *arr, float sharpIndex) {
	if (sharpIndex > 0 && width > 2 && height > 2) {
		float SQRT = sqrt(sqrt(2.0f));
		float alpha = pow(SQRT, sharpIndex);

		float a = alpha + (1.f - alpha) / 3.f;
		float b = (1.f - alpha) / 3.f;

#pragma omp parallel for
		for (int i = 0; i < height; i++) {
			float x = arr[i * width + 0];
			float y = arr[i * width + 1];
			for (int j = 2; j < width; j++) {
				float z = arr[i * width + j];
				y = a*y + b*(x + z);
				if (y < 0.f) {
					y = 0.f;
				} else if (y > 1.f) {
					y = 1.f;
				}
				arr[i* width + j - 2] = x;
				x = y;
				y = z;
			}
			arr[i * width + width - 2] = x;
		}

#pragma omp parallel for
		for (int j = 0; j < width; j++) {
			float x = arr[0 * width + j];
			float y = arr[1 * width + j];
			for (int i = 2; i < height; i++) {
				float z = arr[i * width + j];
				y = a*y + b*(x + z);
				if (y < 0.f) {
					y = 0.f;
				} else if (y > 1.f){
					y = 1.f;
				}
				arr[(i - 2) * width + j] = x;
				x = y;
				y = z;
			}
			arr[(height - 2) * width + j] = x;
		}
	}
}
