Image transform(const Image &image1, const LinearConversion &sc) {
	Image imageOut(image1.getWidth(), image1.getHeight(), sc.getObr());

	beginTransform(sc);

	//imageOut.memset0();

	std::string err = "";

	#pragma omp parallel for
	for (int j = 0; j < imageOut.height; j++) {
		try {
			for (int i = 0; i < imageOut.width; i++) {
				float resR = 0;
				float resG = 0;
				float resB = 0;
				work(static_cast<float>(i), static_cast<float>(j), sc, image1, resR, resG, resB);

				clamp(resR);
				clamp(resG);
				clamp(resB);
				
				imageOut.r[j * imageOut.width + i] = resR;
				imageOut.g[j * imageOut.width + i] = resG;
				imageOut.b[j * imageOut.width + i] = resB;
			}
		} catch (std::string &e) {
#pragma omp critical (saveThrow)
			{
				err = e + "!";
			}
		} catch (...) {
#pragma omp critical (saveThrow)
			{
				err = "Unknown error";
			}
		}
	}

	if (err != "") {
		throw err;
	}

	return imageOut;
}
