/* cmdline.h */

/* File autogenerated by gengetopt version 2.20  */

#ifndef CMDLINE_H
#define CMDLINE_H

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef CMDLINE_PARSER_PACKAGE
#define CMDLINE_PARSER_PACKAGE "app"
#endif

#ifndef CMDLINE_PARSER_VERSION
#define CMDLINE_PARSER_VERSION "1.0.0"
#endif

struct gengetopt_args_info
{
  const char *help_help; /* Print help and exit help description.  */
  const char *version_help; /* Print version and exit help description.  */
  char * filenameIn_arg;	/* ������� ����.  */
  char * filenameIn_orig;	/* ������� ���� original value given at command line.  */
  const char *filenameIn_help; /* ������� ���� help description.  */
  char * folderOut_arg;	/* �������� �������.  */
  char * folderOut_orig;	/* �������� ������� original value given at command line.  */
  const char *folderOut_help; /* �������� ������� help description.  */
  int algorithm_arg;	/* ��������.  */
  char * algorithm_orig;	/* �������� original value given at command line.  */
  const char *algorithm_help; /* �������� help description.  */
  char * mode_arg;	/* ����� ������� ����� (test, transform360)).  */
  char * mode_orig;	/* ����� ������� ����� (test, transform360)) original value given at command line.  */
  const char *mode_help; /* ����� ������� ����� (test, transform360)) help description.  */
  int resize_arg;	/* ���������� ��� �������� (default='1').  */
  char * resize_orig;	/* ���������� ��� �������� original value given at command line.  */
  const char *resize_help; /* ���������� ��� �������� help description.  */
  char * unsharp_arg;	/* Unsharp �����(none, all, one) (default='none').  */
  char * unsharp_orig;	/* Unsharp �����(none, all, one) original value given at command line.  */
  const char *unsharp_help; /* Unsharp �����(none, all, one) help description.  */
  float unsharpCoeff_arg;	/* Unsharp ����������� (default='0.0').  */
  char * unsharpCoeff_orig;	/* Unsharp ����������� original value given at command line.  */
  const char *unsharpCoeff_help; /* Unsharp ����������� help description.  */
  int angle_arg;	/* ���� �������� � ��������. ���������� ����� ������� ������� � 360 (default='1').  */
  char * angle_orig;	/* ���� �������� � ��������. ���������� ����� ������� ������� � 360 original value given at command line.  */
  const char *angle_help; /* ���� �������� � ��������. ���������� ����� ������� ������� � 360 help description.  */
  int notLinearizing_arg;	/* ��� ������������ � �� ������� ������������� ����������� � int (default='0').  */
  char * notLinearizing_orig;	/* ��� ������������ � �� ������� ������������� ����������� � int original value given at command line.  */
  const char *notLinearizing_help; /* ��� ������������ � �� ������� ������������� ����������� � int help description.  */
  int saveAll_arg;	/* ��������� ��� ������������� ����������.  */
  char * saveAll_orig;	/* ��������� ��� ������������� ���������� original value given at command line.  */
  const char *saveAll_help; /* ��������� ��� ������������� ���������� help description.  */
  
  int help_given ;	/* Whether help was given.  */
  int version_given ;	/* Whether version was given.  */
  int filenameIn_given ;	/* Whether filenameIn was given.  */
  int folderOut_given ;	/* Whether folderOut was given.  */
  int algorithm_given ;	/* Whether algorithm was given.  */
  int mode_given ;	/* Whether mode was given.  */
  int resize_given ;	/* Whether resize was given.  */
  int unsharp_given ;	/* Whether unsharp was given.  */
  int unsharpCoeff_given ;	/* Whether unsharpCoeff was given.  */
  int angle_given ;	/* Whether angle was given.  */
  int notLinearizing_given ;	/* Whether notLinearizing was given.  */
  int saveAll_given ;	/* Whether saveAll was given.  */

} ;

extern const char *gengetopt_args_info_purpose;
extern const char *gengetopt_args_info_usage;
extern const char *gengetopt_args_info_help[];

int cmdline_parser (int argc, char * const *argv,
  struct gengetopt_args_info *args_info);
int cmdline_parser2 (int argc, char * const *argv,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);
int cmdline_parser_file_save(const char *filename,
  struct gengetopt_args_info *args_info);

void cmdline_parser_print_help(void);
void cmdline_parser_print_version(void);

void cmdline_parser_init (struct gengetopt_args_info *args_info);
void cmdline_parser_free (struct gengetopt_args_info *args_info);

int cmdline_parser_required (struct gengetopt_args_info *args_info,
  const char *prog_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* CMDLINE_H */
