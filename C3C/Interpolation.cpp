#include "Algorithms.h"

#include <map>
#include <time.h>
#include <cmath>
#include <vector>
#include <stdio.h>
#include <iostream>

#include <omp.h>

namespace bilinearInterpolation {

inline float biLinearInterpolate(const float *a, float u, float t, int width) {
	if (0.f <= u && u <= 1.f && 0.f <= t && t <= 1.f) {
		/* ������������ */
		const float d1 = (1.f - t) * (1.f - u);
		const float d2 = t * (1.f - u);
		const float d3 = t * u;
		const float d4 = (1.f - t) * u;

		/* ��������� �������: a[i][j] */
		const float p1 = a[0 + 0 * width];
		const float p2 = a[1 + 0 * width];
		const float p3 = a[1 + 1 * width];
		const float p4 = a[0 + 1 * width];

		/* ���������� */
		float result = p1 * d1 + p2 * d4 + p3 * d3 + p4 *d2;
		if (result < 0.f) {
			result = 0.f;
		}
		if (result > 1.f) {
			result = 1.f;
		}
		return result;
	} else {
		return 0;
	}
}

inline float biLinearInterpolation(float x, float y, int width, int height, const float* a) {
	int xInt = (int)std::floor(x);
	if (xInt < 0) {
		xInt = 0;
	} else {
		if (xInt >= width - 1) {
			xInt = width - 2;
		}
	}
	const float u = x - xInt;

	int yInt = (int)std::floor(y);
	if (yInt < 0) {
		yInt = 0;
	} else {
		if (yInt >= height - 1) {
			yInt = height - 2;
		}
	}
	const float t = y - yInt;

	return biLinearInterpolate(&a[xInt + yInt * width], u, t, width);
}

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start transform bilinearInterpolation\n";
}

inline void work(float x0, float y0, const LinearConversion &sc, const Image &image, float &resR, float &resG, float &resB) {
	const PointMy pointNew = sc.getConversion(x0, y0);
	const float xNew = pointNew.x;
	const float yNew = pointNew.y;

	resR = biLinearInterpolation(xNew, yNew, image.width, image.height, image.r);
	resG = biLinearInterpolation(xNew, yNew, image.width, image.height, image.g);
	resB = biLinearInterpolation(xNew, yNew, image.width, image.height, image.b);
}

#include "transform.inc"

}


namespace bicubicInterpolation {

inline float cubicInterpolate(const float *p, float x) {
	return p[1] + (-0.5f * p[0] + 0.5f * p[2]) * x
		+ (p[0] - 2.5f * p[1] + 2.0f * p[2] - 0.5f * p[3]) * x * x
		+ (-0.5f * p[0] + 1.5f * p[1] - 1.5f * p[2] + 0.5f * p[3]) * x * x * x;
}

inline float bicubicInterpolate(const float *p, float x, float y, int width) {
	float tmp[4];
	tmp[0] = cubicInterpolate(&p[width * 0], x);
	tmp[1] = cubicInterpolate(&p[width * 1], x);
	tmp[2] = cubicInterpolate(&p[width * 2], x);
	tmp[3] = cubicInterpolate(&p[width * 3], x);
	return cubicInterpolate(tmp, y);
}

inline float bicubickInterpolation(float x, float y, int width, int height, const float* a) {
	int xInt = (int)std::floor(x);
	if (xInt < 1) {
		xInt = 1;
	} else {
		if (xInt >= width - 2) {
			xInt = width - 3;
		}
	}
	const float u = x - xInt;

	int yInt = (int)std::floor(y);
	if (yInt < 1) {
		yInt = 1;
	} else {
		if (yInt >= height - 2) {
			yInt = height - 3;
		}
	}
	const float t = y - yInt;

	if (0.f <= u && u <= 1.f && 0.f <= t && t <= 1.f) {
		float result = bicubicInterpolate(&a[(xInt - 1) + (yInt - 1) * width], u, t, width);
		if (result < 0.f) {
			result = 0.f;
		}
		if (result > 1.f) {
			result = 1.f;
		}
		return result;
	} else {
		return bilinearInterpolation::biLinearInterpolation(x, y, width, height, a);
	}
}

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start transform bicubickInterpolation\n";
}

inline void work(float x0, float y0, const LinearConversion &sc, const Image &image, float &resR, float &resG, float &resB) {
	const PointMy pointNew = sc.getConversion(x0, y0);
	const float xNew = pointNew.x;
	const float yNew = pointNew.y;

	resR = bicubickInterpolation(xNew, yNew, image.width, image.height, image.r);
	resG = bicubickInterpolation(xNew, yNew, image.width, image.height, image.g);
	resB = bicubickInterpolation(xNew, yNew, image.width, image.height, image.b);
}

#include "transform.inc"

}
