#ifndef COMMON_H_
#define COMMON_H_

#include <iostream>
#include <math.h>
#include <algorithm>
#include <string>
#include <sstream>
#include <iomanip>

/// ������������ ������ �����������, ��� ����, ����� �� �������� ����.
const long long MAX_IMAGE_SIZE = (long long)200 * 1000 * 1000;

void createFile(const std::string &folder, const std::string &file);

void logDebug(const char *format, ...);

void throwError();

#define check(v) { \
if (!(v)) { \
	char buf[256]; \
	sprintf_s(buf, "Error %s %d %s\n", __FILE__, __LINE__, #v); \
	logDebug("%s", buf); \
	std::string sErr(buf); \
	throw sErr; \
} \
}

#define check2(v,s) { \
if (!(v)) { \
	logDebug("Error %s %s in file %s line %d\n", #v, s, __FILE__, __LINE__); \
	throw std::string(s); \
} \
}

const float PI = 3.141592653589793238463f;

inline float nextRandom() {
	return static_cast<float>(rand()) / RAND_MAX;
}

inline std::string intToStr(int i) {
	std::stringstream s;
	s << i;
	return s.str();
}

inline std::string floatToStr(float f) {
	setlocale(LC_ALL, "C");
	std::stringstream s;
	s.setf(std::ios::fixed);
	s << std::setprecision(1) << f;
	return s.str();
}

inline bool isEqualsRelative(float a, float b, float absoluteEpsilon) {
	if (std::abs(a) <= absoluteEpsilon && std::abs(b) <= absoluteEpsilon) {
		return true;
	}
	const float delta = std::max(std::abs(a) * absoluteEpsilon, std::abs(b) * absoluteEpsilon);
	return std::abs(a - b) <= delta;
}

inline void clamp(float &f) {
	if (f < 0.f) {
		f = 0.f;
	} else if (f > 1.f) {
		f = 1.f;
	}
}

#endif // COMMON_H_
