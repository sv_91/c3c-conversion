#include "Algorithms.h"

#include <map>
#include <time.h>
#include <cmath>
#include <vector>
#include <stdio.h>
#include <iostream>

#include <omp.h>

namespace SH {

class Polygon {
private:

	static const int MAX_POINTS = 10;

	PointMy points[MAX_POINTS];

	int size_;

public:

	Polygon()
		: size_(0)
	{}

	inline void push_back(const PointMy &p) {
		check(0 <= size_ && size_ < MAX_POINTS);
		points[size_] = p;
		size_++;
	}

	PointMy& operator[] (int index) {
		check(0 <= index && index < size_);
		return points[index];
	}

	const PointMy operator[] (int index) const {
		check(0 <= index && index < size_);
		return points[index];
	}

	int size() const {
		return size_;
	}

};

inline float sqalar(const PointMy &p1, const PointMy &p2) {
	return p1.x * p2.x + p1.y * p2.y;
}

Polygon otsech(const Polygon &sq, const PointMy &point0, const PointMy &point1, const PointMy &n) {
	const float EPSILON = 1e-4f;
	PointMy vectLine(point1 - point0);
	check(std::abs(sqalar(vectLine, n)) < EPSILON); // ��� - �������.

	Polygon result;
	if (sq.size() == 0) {
		return result;
	}

	const PointMy vecPointLine(sq[0] - point1);
	float sqalarVecPointLine = sqalar(vecPointLine, n);
	const int sqSize = sq.size();
	for (int i = 0; i < sqSize; i++) {
		// ������ �� ����� ������ �� ������.
		if (sqalarVecPointLine >= 0) {
			// ����� ����������� �������������.
			result.push_back(sq[i]);
		}

		// ���� ����� ����������� 2-� ������
		const PointMy &p0 = sq[i];
		const PointMy &p1 = sq[i + 1 == sqSize ? 0 : i + 1];
		PointMy vec2PointLine(p1 - point1);
		const float sqalarVec2PointLine = sqalar(vec2PointLine, n);
		if (sqalarVec2PointLine * sqalarVecPointLine < 0.f) {
			const float a1 = p0.y - p1.y;
			const float b1 = -(p0.x - p1.x);
			const float c1 = p0.y * -b1 - p0.x * a1;

			const float a2 = point0.y - point1.y;
			const float b2 = -(point0.x - point1.x);
			const float c2 = point0.y * -b2 - point0.x * a2;

			const float znam = a1 * b2 - a2 * b1;

			const float x = -(c1 * b2 - c2 * b1) / znam;
			const float y = -(a1 * c2 - a2 * c1) / znam;

			result.push_back(PointMy(x, y));
		}
		sqalarVecPointLine = sqalarVec2PointLine;
	}

	return result;
}

inline float calcSquareTriangle(const PointMy &v1, const PointMy &v2) {
	return std::abs(v1.x * v2.y - v2.x * v1.y) / 2.f;
}

float calkSquare(const Polygon &sq) {
	float result = 0;
	if (sq.size() >= 3) {
		const PointMy &v0 = sq[0];
		PointMy vect1 = sq[1] - v0;
		for (int i = 2; i < sq.size(); i++) {
			const PointMy vect2 = sq[i] - v0;
			result += calcSquareTriangle(vect2, vect1);
			vect1 = vect2;
		}
	}

	return result;
}

float getSquare(const Polygon &sq1, const Polygon &sq2) {
	check(sq1.size() == 4);
	check(sq2.size() == 4);
	// ���������, ��� ������ ������ - ������������� � �������, ������������� ����� ����.
	if (sq2[0].x == sq2[1].x) {
		check(sq2[0].x == sq2[1].x);
		check(sq2[1].y == sq2[2].y);
		check(sq2[2].x == sq2[3].x);
		check(sq2[3].y == sq2[0].y);
	} else {
		check(sq2[0].y == sq2[1].y);
		check(sq2[1].x == sq2[2].x);
		check(sq2[2].y == sq2[3].y);
		check(sq2[3].x == sq2[0].x);
	}
	Polygon res1 = otsech(sq1, sq2[0], sq2[1], sq2[2] - sq2[1]);
	Polygon res2 = otsech(res1, sq2[1], sq2[2], sq2[3] - sq2[2]);
	Polygon res3 = otsech(res2, sq2[2], sq2[3], sq2[0] - sq2[3]);
	Polygon res4 = otsech(res3, sq2[3], sq2[0], sq2[1] - sq2[0]);

	const float result = calkSquare(res4);
	return result;
}

inline Polygon getNewFigure(const float xMin_, const float yMin_, const float xMax_, const float yMax_, const LinearConversion &sc) {
	const PointMy p1 = sc.getConversion(xMin_, yMin_);
	const PointMy p2 = sc.getConversion(xMin_, yMax_);
	const PointMy p3 = sc.getConversion(xMax_, yMax_);
	const PointMy p4 = sc.getConversion(xMax_, yMin_);

	Polygon sq1;
	sq1.push_back(p1);
	sq1.push_back(p2);
	sq1.push_back(p3);
	sq1.push_back(p4);

	return sq1;
}

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start transform SH\n";
}

inline void work(float x0, float y0, const LinearConversion &sc, const Image &image, float &resR, float &resG, float &resB) {
	const float x1 = x0 + 1.f;
	const float y1 = y0 + 1.f;

	Polygon sq1 = getNewFigure(x0, y0, x1, y1, sc);

	const float x1Min = std::fminf(sq1[0].x, std::fminf(sq1[1].x, std::fminf(sq1[2].x, sq1[3].x)));
	const float x1Max = std::fmaxf(sq1[0].x, std::fmaxf(sq1[1].x, std::fmaxf(sq1[2].x, sq1[3].x)));
	const float y1Min = std::fminf(sq1[0].y, std::fminf(sq1[1].y, std::fminf(sq1[2].y, sq1[3].y)));
	const float y1Max = std::fmaxf(sq1[0].y, std::fmaxf(sq1[1].y, std::fmaxf(sq1[2].y, sq1[3].y)));

	const int xMin = static_cast<int>(std::fmaxf(0.f, x1Min));
	const int xMax = static_cast<int>(std::fminf(x1Max + 1.f, static_cast<float>(image.width)));
	const int yMin = static_cast<int>(std::fmaxf(0.f, y1Min));
	const int yMax = static_cast<int>(std::fminf(y1Max + 1.f, static_cast<float>(image.height)));
	// �� �������� ����� ���������� +1 !!!

	const float square0 = calkSquare(sq1);

	resR = 0;
	resG = 0;
	resB = 0;

	float rSquare = 0;
	for (int m = xMin; m < xMax; m++) {
		for (int n = yMin; n < yMax; n++) {
			const float xIn0 = static_cast<float>(m);
			const float yIn0 = static_cast<float>(n);
			const float xIn1 = xIn0 + 1.f;
			const float yIn1 = yIn0 + 1.f;

			Polygon sq2;
			sq2.push_back(PointMy(xIn0, yIn0));
			sq2.push_back(PointMy(xIn0, yIn1));
			sq2.push_back(PointMy(xIn1, yIn1));
			sq2.push_back(PointMy(xIn1, yIn0));

			const float square = getSquare(sq1, sq2);
			rSquare += square;

			resR += square * image.r[n * image.width + m];
			resG += square * image.g[n * image.width + m];
			resB += square * image.b[n * image.width + m];
		}
	}

	const float square0Tmp = 1.f / square0;

	resR *= square0Tmp;
	resG *= square0Tmp;
	resB *= square0Tmp;

	rSquare *= square0Tmp;
	// �� ����, rSquare ������ ���� ����� 1, �� � ���� ������������ ������������ ������ �� ������ ���� ��������.
	// � ����� ���� ������ 1, �� ��� ���������.
	if (rSquare > 1.f) {
		resR /= rSquare;
		resG /= rSquare;
		resB /= rSquare;
	}
}

#include "transform.inc"

}
