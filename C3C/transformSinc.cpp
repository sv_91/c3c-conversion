#include "Algorithms.h"

#include <map>
#include <time.h>
#include <cmath>
#include <vector>
#include <stdio.h>
#include <iostream>

#include <omp.h>

namespace sinc {

float sinc(float x) {
	if (x == 0.f) {
		return 1.f;
	} else {
		return std::sin(PI * x) / (PI * x);
	}
}

float sinc(float x, float y, float i, float j) {
	return sinc(x - i) * sinc(y - j);
}

float lanczos(float x, float a) {
	if (std::abs(x) < a) {
		return sinc(x) * sinc(x / a);
	} else {
		return 0.f;
	}
}

float lanczos(float x, float y, float i, float j, float a) {
	return lanczos(x - i, a) * lanczos(y - j, a);
}

float j1(float x) {
	const float saveX = x;
	x = std::abs(x);
	if (x < 3.f / 4.f) {
		return saveX / 2.f;
	} else {
		return std::sqrt(2 / (PI * x)) * std::cos(x - 3.f * PI / 4.f);
	}
}

float jinc(float x) {
	//x = std::abs(x);
	//return j1(PI * x) / (2.f * x);
	return (std::fabs(PI * x) < 1e-8) ? 0.5f : j1(PI * x) / (2.f * x);
	//return j1(x) / (x);
}

float jinc(float x, float y, float i, float j) {
	return jinc(std::sqrt((x - i) * (x - i) + (y - j) * (y - j)));
}

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start transform sinc\n";
}

inline void work(float x0, float y0, const LinearConversion &sc, const Image &image, float &resR, float &resG, float &resB) {
	const PointMy p = sc.getConversion(x0, y0);
	const float x = p.x;
	const float y = p.y;

	resR = 0;
	resG = 0;
	resB = 0;

	const int radius = 3;
	const float radiusF = static_cast<float>(radius);

	std::vector<float> xLancoz(2 * radius + 1);
	for (int m = -radius; m <= radius; m++) {
		const float x1F = std::floor(x + m);
		xLancoz[m + radius] = lanczos(x - x1F, radiusF);
	}

	for (int m = -radius; m <= radius; m++) {
		const float y1F = y + m;
		if (0.f <= y1F && y1F < image.height) {
			const int y1 = static_cast<int>(y1F);

			float resR2 = 0;
			float resG2 = 0;
			float resB2 = 0;
			for (int n = -radius; n <= radius; n++) {
				const float x1F = x + n;
				if (0.f <= x1F && x1F < image.width) {
					const int x1 = static_cast<int>(x1F);

					const float funcR = xLancoz[n + radius];
					resR2 += funcR * image.r[x1 + y1 * image.width];
					resG2 += funcR * image.g[x1 + y1 * image.width];
					resB2 += funcR * image.b[x1 + y1 * image.width];
				}
			}

			const float funcR2 = lanczos(y - std::floor(y1F), radiusF);
			resR += resR2 * funcR2;
			resG += resG2 * funcR2;
			resB += resB2 * funcR2;
		}
	}
}

#include "transform.inc"

}
