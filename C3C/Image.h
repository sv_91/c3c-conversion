#pragma once

#include "common.h"
#include "colorConversion.h"
#include "Conversion.h"

#include <omp.h>

unsigned char* alloc(const int sizeBytes);

inline int* allocInt(const int sizeElements) {
	return reinterpret_cast<int*>(alloc(sizeElements * sizeof(int)));
}

inline float* allocFloat(const int sizeElements) {
	return reinterpret_cast<float*>(alloc(sizeElements * sizeof(float)));
}

void memcpyArray(unsigned char* dst, unsigned char *src, int sizeBytes);

inline void memcpyFloats(float* dst, float* src, int sizeElements) {
	memcpyArray(reinterpret_cast<unsigned char*>(dst), reinterpret_cast<unsigned char*>(src), sizeElements * sizeof(float));
}

void freeMemory(void* memory);

class Image {
private:

	int getLinearAddress(int x, int y) const {
		check(0 <= x && x < width);
		check(0 <= y && y < height);
		return y * width + x;
	}

public:

	float *r;
	float *g;
	float *b;

	int width;
	int height;

	float xMin;
	float yMin;

	Image(int width_, int height_)
		: width(width_)
		, height(height_)
		, xMin(-width_ / 2.f)
		, yMin(-height_ / 2.f)
	{
		std::cout << width << " " << height << "\n";
		check((long long)width * height <= MAX_IMAGE_SIZE);
		r = allocFloat(width * height);
		g = allocFloat(width * height);
		b = allocFloat(width * height);
	}

	Image(int width_, int height_, const LinearConversion &sc)
		: width(getWidthConversion(width_, height_, sc))
		, height(getHeightConversion(width_, height_, sc))
	{
		xMin = getFourConversion(0.f, 0.f, static_cast<float>(width_), static_cast<float>(height_), sc).xMin;
		yMin = getFourConversion(0.f, 0.f, static_cast<float>(width_), static_cast<float>(height_), sc).yMin;

		logDebug("Image sizes %d %d %f %f %d\n", this->width, this->height, xMin, yMin, (long long)width * height - MAX_IMAGE_SIZE);

		check((long long)width * height <= MAX_IMAGE_SIZE);

		r = allocFloat(this->width * this->height);
		g = allocFloat(this->width * this->height);
		b = allocFloat(this->width * this->height);
	}

	void memset0() {
		memset(r, 0, width * height * sizeof(float));
		memset(g, 0, width * height * sizeof(float));
		memset(b, 0, width * height * sizeof(float));
	}

	~Image() {
		freeMemory(r);
		freeMemory(g);
		freeMemory(b);
	}

	void setColors(int x, int y, float colorRed, float colorGreen, float colorBlue) {
		const int address = getLinearAddress(x, y);
		r[address] = colorRed;
		g[address] = colorGreen;
		b[address] = colorBlue;
	}

	void setColorsInvertTo0Or1(int x, int y) {
		const int address = getLinearAddress(x, y);
		const float avg = (r[address] + g[address] + b[address]) / 3;
		float color;
		if (avg >= 0.5) {
			color = 0.f;
		} else {
			color = 1.f;
		}
		r[address] = color;
		g[address] = color;
		b[address] = color;
	}

	void setColorR(int x, int y, float color) {
		r[getLinearAddress(x, y)] = color;
	}

	void setColorG(int x, int y, float color) {
		g[getLinearAddress(x, y)] = color;
	}

	void setColorB(int x, int y, float color) {
		b[getLinearAddress(x, y)] = color;
	}

	int getWidth() const {
		return width;
	}

	int getHeight() const {
		return height;
	}

	float getRed(int x, int y) const {
		return r[getLinearAddress(x, y)];
	}

	float getGreen(int x, int y) const {
		return g[getLinearAddress(x, y)];
	}

	float getBlue(int x, int y) const {
		return b[getLinearAddress(x, y)];
	}

	void operator= (const Image& second) {
		std::cout << "Copy" << "\n";
		freeMemory(r);
		freeMemory(g);
		freeMemory(b);

		this->width = second.width;
		this->height = second.height;
		this->xMin = second.xMin;
		this->yMin = second.yMin;

		this->r = allocFloat(width * height);
		memcpyFloats(this->r, second.r, width * height);
		this->g = allocFloat(width * height);
		memcpyFloats(this->g, second.g, width * height);
		this->b = allocFloat(width * height);
		memcpyFloats(this->b, second.b, width * height);
	}

	Image(const Image& second)
		: r(allocFloat(second.width * second.height))
		, g(allocFloat(second.width * second.height))
		, b(allocFloat(second.width * second.height))
		, width(second.width)
		, height(second.height)
		, xMin(second.xMin)
		, yMin(second.yMin)
	{
		std::cout << "Create" << "\n";
		memcpyFloats(this->r, second.r, width * height);
		memcpyFloats(this->g, second.g, width * height);
		memcpyFloats(this->b, second.b, width * height);
	}

	void crop() {
		const float EPS = 1e-6f;

		int xMin = width;
		int yMin = height;
		int xMax = 0;
		int yMax = 0;

		//#pragma omp parallel for
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (r[getLinearAddress(x, y)] > EPS || g[getLinearAddress(x, y)] > EPS || b[getLinearAddress(x, y)] > EPS) {
					xMin = std::min(xMin, x);
					yMin = std::min(yMin, y);
					xMax = std::max(xMax, x);
					yMax = std::max(yMax, y);
				}
			}
		}

		const int widthNew = xMax - xMin + 1;
		const int heightNew = yMax - yMin + 1;

		check(0 <= widthNew && widthNew <= width);
		check(0 <= heightNew && heightNew <= height);

		float *rNew = allocFloat(widthNew * heightNew);
		float *gNew = allocFloat(widthNew * heightNew);
		float *bNew = allocFloat(widthNew * heightNew);

		//#pragma omp parallel for
		for (int y = yMin; y <= yMax; y++) {
			memcpyFloats(&rNew[(y - yMin) * widthNew], &r[y * width + xMin], widthNew);
			memcpyFloats(&gNew[(y - yMin) * widthNew], &g[y * width + xMin], widthNew);
			memcpyFloats(&bNew[(y - yMin) * widthNew], &b[y * width + xMin], widthNew);
		}

		freeMemory(r);
		freeMemory(g);
		freeMemory(b);

		r = rNew;
		g = gNew;
		b = bNew;

		width = widthNew;
		height = heightNew;

		xMin = -width / 2;
		yMin = -height / 2;
	}

	void increraseSize(int count) {
		int newWidth = width * count;
		int newHeight = height * count;
		float newXMin = xMin * count;
		float newYMin = yMin * count;

		float *newR = allocFloat(newWidth * newHeight);
		float *newG = allocFloat(newWidth * newHeight);
		float *newB = allocFloat(newWidth * newHeight);

		//#pragma omp parallel for
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int m = 0; m < count; m++) {
					for (int n = 0; n < count; n++) {
						newR[(i * count + m) + (j * count + n) * newWidth] = r[i + j * width];
						newG[(i * count + m) + (j * count + n) * newWidth] = g[i + j * width];
						newB[(i * count + m) + (j * count + n) * newWidth] = b[i + j * width];
					}
				}
			}
		}

		freeMemory(r);
		freeMemory(g);
		freeMemory(b);

		r = newR;
		g = newG;
		b = newB;

		width = newWidth;
		height = newHeight;
		xMin = newXMin;
		yMin = newYMin;
	}

	void roundArr(int maxValue) {
		std::cout << "round\n";
		loadPtr sRGB2Lin = colorConversionInitLoad(maxValue, lin2Lin);
		savePtr Lin2sRGBSave = colorConversionInitSave(maxValue, lin2Lin);

		//#pragma omp parallel for
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				const int address = j * width + i;
				r[address] = sRGB2Lin(Lin2sRGBSave(r[address]));
				g[address] = sRGB2Lin(Lin2sRGBSave(g[address]));
				b[address] = sRGB2Lin(Lin2sRGBSave(b[address]));
			}
		}
	}

};
