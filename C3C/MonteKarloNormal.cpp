#include "Algorithms.h"

#include <map>
#include <time.h>
#include <cmath>
#include <vector>
#include <iostream>

#include <omp.h>

PointMy getNormal01() {
	const float u = nextRandom() + 1e-25f;
	const float v = nextRandom() + 1e-25f;

	const float PI = 3.141592653589793238463f;

	const float r = std::sqrt(-2 * std::log(u));
	const float fi = 2 * PI * v;

	return PointMy(r * std::cos(fi), r * std::sin(fi));
}

PointMy getNormal(float maton, float disp) {
	PointMy p = getNormal01();

	p.x *= std::sqrt(disp);
	p.y *= std::sqrt(disp);

	p.x += maton;
	p.y += maton;

	return p;
}

inline void getVectorNormal(const int count, std::vector<float> &randoms) {
	unsigned int randInitiate = static_cast<unsigned int>(time(NULL));
	std::cout << "rand initiate " << randInitiate << "\n";
	srand(randInitiate);

	for (int i = 0; i < count; i++) {
		const PointMy p = getNormal(.5f, .41f*.41f);

		const float x = p.x;
		const float y = p.y;

		randoms.push_back(x);
		randoms.push_back(y);
	}

	/*FILE *f = fopen("this.txt", "w");
	for (int i = 0; i < randoms.size(); i += 2) {
	fprintf(f, "%.6f ", randoms[i]);
	}
	fprintf(f, "\n");
	for (int i = 1; i < randoms.size(); i += 2) {
	fprintf(f, "%.6f ", randoms[i]);
	}
	fclose(f);*/

	check(randoms.size() / 2 == count);
}

namespace MKNormal1 {

const int COUNT_ITERATION = 1000;

std::vector<float> randoms;

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start transform monteKarloNormal1\n";

	getVectorNormal(COUNT_ITERATION, randoms);
}

inline void work(float x0, float y0, const LinearConversion &sc, const Image &image, float &resR, float &resG, float &resB) {
	resR = 0;
	resG = 0;
	resB = 0;
	int count = 0;
	for (int k = 0; k < COUNT_ITERATION; k++) {
		const float xIn = randoms[2 * k];
		const float yIn = randoms[2 * k + 1];

		if (0.f <= xIn && xIn < 1.f && 0.f <= yIn && yIn < 1.f) {
			const float x = x0 + xIn;
			const float y = y0 + yIn;

			const PointMy pointResF = sc.getConversion(x, y);
			float xResF = pointResF.x;
			float yResF = pointResF.y;

			if (0.f <= xResF && xResF < image.width && 0.f <= yResF && yResF < image.height) {
				// ��� ������������� ����� ���������� (int) �������� ��� floor.
				const int xRes = (int)xResF;
				const int yRes = (int)yResF;

				resR += image.r[xRes + yRes * image.width];
				resG += image.g[xRes + yRes * image.width];
				resB += image.b[xRes + yRes * image.width];

				count++;
			} else {
				// ���� ������� �� �������, �� ������ ������ �� ���������.
			}
		} else {
			// ������� �� ������� ��� �����.
		}
	}

	if (count != 0) {
		resR /= count;
		resG /= count;
		resB /= count;
	}
}

#include "transform.inc"

}

namespace MKNormal2 {

const int COUNT_ITERATION = 1000;

std::vector<float> randoms;

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start transform monteKarloNormal2\n";

	getVectorNormal(COUNT_ITERATION, randoms);
}

inline void work(float x0, float y0, const LinearConversion &sc, const Image &image, float &resR, float &resG, float &resB) {
	resR = 0.f;
	resG = 0.f;
	resB = 0.f;
	int sumResult = 0;
	for (int k = 0; k < COUNT_ITERATION; k++) {
		const float xIn = randoms[2 * k];
		const float yIn = randoms[2 * k + 1];

		const float x = x0 + xIn;
		const float y = y0 + yIn;

		const PointMy pointResF = sc.getConversion(x, y);
		float xResF = pointResF.x;
		float yResF = pointResF.y;

		if (0.f <= xResF && xResF < image.width && 0.f <= yResF && yResF < image.height) {
			// ��� ������������� ����� ���������� (int) �������� ��� floor.
			const int xRes = (int)xResF;
			const int yRes = (int)yResF;

			resR += image.r[xRes + yRes * image.width];
			resG += image.g[xRes + yRes * image.width];
			resB += image.b[xRes + yRes * image.width];
			sumResult++;
		} else {
			// ���� ������� �� �������, �� ������ ������ �� ���������.
		}
	}

	if (sumResult != 0.f) {
		resR = resR / sumResult;
		resG = resG / sumResult;
		resB = resB / sumResult;
	} 
}

#include "transform.inc"

}

float getGauss(float x, float maton, float disp) {
	return std::exp(-(x - maton) * (x - maton) / (2 * disp)) / std::sqrt(2 * PI * disp);
}

float getGauss(float x, float y, float maton1, float maton2, float disp) {
	return getGauss(x, maton1, disp) * getGauss(y, maton2, disp);
}

namespace MKNormal3 {

const int COUNT_ITERATION = 1000;

std::vector<float> randoms;

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start transform monteKarloNormal3\n";

	getVectorNormal(COUNT_ITERATION, randoms);
}

inline void work(float x0, float y0, const LinearConversion &sc, const Image &image, float &resR, float &resG, float &resB) {
	resR = 0.f;
	resG = 0.f;
	resB = 0.f;
	float sumResult = 0.f;
	for (int k = 0; k < COUNT_ITERATION; k++) {
		const float xIn = randoms[2 * k];
		const float yIn = randoms[2 * k + 1];

		const float x = x0 + xIn;
		const float y = y0 + yIn;

		const PointMy pointResF = sc.getConversion(x, y);
		float xResF = pointResF.x;
		float yResF = pointResF.y;

		if (0.f <= xResF && xResF < image.width && 0.f <= yResF && yResF < image.height) {
			// ��� ������������� ����� ���������� (int) �������� ��� floor.
			const int xRes = (int)xResF;
			const int yRes = (int)yResF;

			const float xRazn = xResF - xRes;
			const float yRazn = yResF - yRes;

			const float gaussCoeff = getGauss(xRazn, yRazn, 0.5f, 0.5f, 0.41f*0.41f);

			resR += image.r[xRes + yRes * image.width] * gaussCoeff;
			resG += image.g[xRes + yRes * image.width] * gaussCoeff;
			resB += image.b[xRes + yRes * image.width] * gaussCoeff;
			sumResult += gaussCoeff;
		} else {
			// ���� ������� �� �������, �� ������ ������ �� ���������.
		}
	}

	if (sumResult != 0.f) {
		resR = resR / sumResult;
		resG = resG / sumResult;
		resB = resB / sumResult;
	} else {
		resR = 0.f;
		resG = 0.f;
		resB = 0.f;
	}
}

#include "transform.inc"

}

namespace MKNormal4 {

const int COUNT_ITERATION = 1000;

std::vector<float> randoms;

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start transform monteKarloNormal4\n";

	getVectorNormal(COUNT_ITERATION, randoms);
}

inline void work(float x0, float y0, const LinearConversion &sc, const Image &image, float &resR, float &resG, float &resB) {
	resR = 0.f;
	resG = 0.f;
	resB = 0.f;
	float sumResult = 0.f;
	for (int k = 0; k < COUNT_ITERATION; k++) {
		const float xIn = randoms[2 * k];
		const float yIn = randoms[2 * k + 1];

		if (0.f <= xIn && xIn < 1.f && 0.f <= yIn && yIn < 1.f) {
			const float x = x0 + xIn;
			const float y = y0 + yIn;

			const PointMy pointResF = sc.getConversion(x, y);
			float xResF = pointResF.x;
			float yResF = pointResF.y;

			if (0.f <= xResF && xResF < image.width && 0.f <= yResF && yResF < image.height) {
				// ��� ������������� ����� ���������� (int) �������� ��� floor.
				const int xRes = (int)xResF;
				const int yRes = (int)yResF;

				const float xRazn = xResF - xRes;
				const float yRazn = yResF - yRes;

				const float gaussCoeff = getGauss(xRazn, yRazn, 0.5f, 0.5f, 0.41f*0.41f);

				resR += image.r[xRes + yRes * image.width] * gaussCoeff;
				resG += image.g[xRes + yRes * image.width] * gaussCoeff;
				resB += image.b[xRes + yRes * image.width] * gaussCoeff;
				sumResult += gaussCoeff;
			} else {
				// ���� ������� �� �������, �� ������ ������ �� ���������.
			}
		} else {
			// ������� �� ������� ��� �����
		}
	}

	if (sumResult != 0.f) {
		resR = resR / sumResult;
		resG = resG / sumResult;
		resB = resB / sumResult;
	} else {
		resR = 0.f;
		resG = 0.f;
		resB = 0.f;
	}
}

#include "transform.inc"

}
