// ADOBE SYSTEMS INCORPORATED
// Copyright  2007 Adobe Systems Incorporated
// All Rights Reserved
//
// NOTICE:  Adobe permits you to use, modify, and distribute this 
// file in accordance with the terms of the Adobe license agreement
// accompanying it.  If you have received this file from a source
// other than Adobe, then your use, modification, or distribution
// of it requires the prior written permission of Adobe.
//-------------------------------------------------------------------
//
//	File:
//		PI3D.h
//
//	Copyright 2007, Adobe Systems Incorporated.
//	All Rights Reserved.
//
//	Distribution:
//		PUBLIC
//
//	Description:
//		This file describes version 1 of Photoshop's plug-in 3d interface.
//
//	Use:
//		Include in all plug-ins using 3D.
//		
//
#ifndef __AGF__

#ifndef __PI3D__
#define __PI3D__

#if PRAGMA_ONCE
#pragma once
#endif

#include "PIActions.h"
#include "PIGeneral.h"
#include "SPBasic.h"
#include "SPFiles.h"


/* Operation selectors */
#define formatSelectorPrepareRenderer		100
#define formatSelectorRender				101
#define formatSelectorFinishRenderer		102
#define formatSelectorSetObjectPosition		103
#define formatSelectorGoToCamera			104
#define formatSelectorSetLightMode			105
#define formatSelectorSetRenderMode			106
#define formatSelectorSetCrossSection		107
#define formatSelectorUpdateTexture			108
#define formatSelectorSetObjectScale		109
#define formatSelectorPreprocessU3D			110
#define formatSelectorGetDataFromEngine		111
#define formatSelectorReloadScene			112
#define formatSelectorGetTexturePointer		113
#define formatSelectorUpdateTextureInMem	114
#define formatSelectorSaveTexture			115
#define formatSelectorReleaseResources		116
#define formatSelectorSetTransferFunction	117

/*	We keep various pieces of information about the file format in the PiMI resource.
 Here is the structure of that resource. */

/******************************************************************************/
#define PIFmtSupports3DProperty				'fm3d'		/* 'fm3d' plug-in supports 3D */

#define key3DCurrentEngine			"key3DCurrentEngine"

#define k3DXAxis 0
#define k3DYAxis 1
#define k3DZAxis 2

#define kMax3DTextures				4096
#define kMax3DInterfaceStringLength 512
#define kMax3DURLStringLength		2048

#define kRenderQualityHigh 0
#define kRenderQualityLow 1

#define k3DToolOrbit 0
#define k3DToolRotate 1
#define k3DToolPan 2
#define k3DToolWalk 3
#define k3DToolZoom 4
#define k3DToolCrossSection 5

#define k3DDataTypeU3D 0
#define k3DDataType3DS 1
#define k3DDataTypeDAE 2
#define k3DDataTypeKMZ 3

#define k3DMapTypeTexture 0x0001
#define k3DMapTypeReflection 0x0002
#define k3DMapTypeBump 0x0004
#define k3DMapTypeSpecular 0x0008
#define k3DMapTypeOpacity 0x0010
#define k3DMapTypeShininess 0x0020
#define k3DMapTypeSelfIllumination 0x0040


/*	Keys in the 3D Scene Descriptor */

/******************************************************************************/
#define keyZ				'Z   '

#define keyGlobalAmbientRed  'GamR'
#define keyGlobalAmbientGreen	'GamG'
#define keyGlobalAmbientBlue	'GamB'

#define keyGlobalUnits		'Gunt'
#define keyGlobalAnimStart	'Gast'
#define keyGlobalFrames		'Gfrm'
#define keyGlobalFPS		'Gfps'
#define keyGlobalRangeName	'GRNm'

#define keyLightList		'lite'
#define keyLightClass		'litc'
#define keyCameraList		'caml'
#define keyCameraClass		'camc'
#define keyMeshList			'mshl'
#define keyMeshClass		'msho'
#define keyMaterialList		'mtll'
#define keyMaterialClass	'mtlo'
#define keyMeshIsVolume		'misv'
#define keyRerenderPaint	'rrwp'
#define keyRerenderOnUp		'rrmu'

#define keyVertexCount		'verc'
#define keyPolyCount		'plyc'

#define keyMultiple			'mult'
#define keyTargetX			'tarx'
#define keyTargetY			'tary'
#define keyTargetZ			'tarz'
#define keyHotspot			'hots'
#define keyShadow			'shdw'
#define keyAttenuation		'attn'
#define keyAttenType		'attt'
#define keyAttenA			'atta'
#define keyAttenB			'attb'
#define keyAttenC			'attc'
#define keyOuterRadius		'orad'
#define keyInnerRadius		'irad'
#define keyBank				'bank'
#define keyOrtho			'orth'
#define keyApsect			'aspr'
#define keyZoomFactor		'zmfc'
#define keyFlags			'flag'
#define keySmoothing		'smth'
#define key3DIndexList		'indl'
#define key3DIndex			'indx'
#define keyHidden			'hidn'
#define keyHasMatrix		'hmat'
#define keyHasSmoothing		'hsmt'
#define keyAmbientRed		'ared'
#define keyAmbientGreen		'agrn'
#define keyAmbientBlue		'ablu'
#define keyDiffuseRed		'dred'
#define keyDiffuseGreen		'dgrn'
#define keyDiffuseBlue		'dblu'
#define keySpecularRed		'sred'
#define keySpecularGreen	'sgrn'
#define keySpecularBlue		'sblu'
#define keyEmissiveRed		'ered'
#define keyEmissiveGreen	'egrn'
#define keyEmissiveBlue		'eblu'
#define keyShininess		'shin'
#define keyShininess2		'shi2'
#define keyReflection		'refl'
#define keySelfIllumination	'self'
#define keyShading			'shad'
#define keyTwoSide			'twos'
#define keyWireframe		'wire'
#define keyDecal			'decl'
#define key3DSStyle			'msty'
#define keyWireframeSize	'wfsz'
#define keyUScale			'uscl'
#define keyVScale			'vscl'
#define keyUOffset			'uoff'
#define keyVOffset			'voff'
#define keyFullMapName		'fMnM'
#define keyVertexList		'verl'
#define keyNormalList		'nrml'
#define keyUVList			'uvl '
#define keylineColorList	'lncl'
#define keylineMaterialList 'lnmt'
#define keyVertexColorList	'vnvl'
#define keyLineList			'lnfl'
#define keyFaceList			'facl'
#define keyFaceIndexes		'faci'
#define keyMapList			'mapl'
#define keyMapClass			'mapo'
#define keyMaterialIndList	'matl'
#define keyMaterialIndObj	'mato'
#define keyRefraction		'RfAc'

#define keyKeys				'KeyS'
#define keyFrameNumber		'FrNm'
#define keyFlagsAnim		'FlAn'
#define keyNameAnim			'FlNn'
#define keyTension			'KtEn'
#define keyBias				'KBia'
#define keyEasto			'Keto'
#define keyEasefrom			'Kfro'

#define keyRotationRad		  'RoRd'
#define keyAnimVectorX        'vctX'
#define keyAnimVectorY        'vctY'
#define keyAnimVectorZ        'vctZ'
#define keyAnimVectorObject   'vctO'
#define keyAnimationDataClass 'AniC'
#define keyAnimActionClass    'AnAc'
#define keyInstanceName		  'InsN'
#define keyFlags1			  'flgO'
#define keyFlags2			  'flgT'
#define keyU3DNodeID		  'NoID'
#define keyU3DParentID        'PrID'
#define keyU3DType			  'tYpE'
#define keyPivotX			  'PvtX'
#define keyPivotY			  'PvtY'
#define keyPivotZ			  'PvtZ'
#define keyPivotObject        'PvtO'
#define keyPivotList		  'PvLs'
#define keyHasPivot			  'PvPr'
#define keyHasRange			  'RgBl'
#define keyHasLocalMatrix	  'LcMt'
#define keyRangeName		  'RgNm'
#define keyframePositionList  'PosL'
#define keyframeScaleList     'SclL'
#define keyframeRotationList  'RotL'
#define keyframeFOVList		  'FOVl'
#define keyframeRollList	  'RolL'
#define keyKeyFrameList		  'KeFL'
#define keyKeyFrameClass      'KeCS'
#define keyAnimationList	  'AnLs'
#define keyframePositionActionList 'acPs'
#define keyframeScaleActionList    'acSc'
#define keyframeRotationActionList 'acRt'
#define keyframeFOVActionList      'acFV'
#define keyframeRollActionList     'acRl'
#define keyframeHotActionList		'acHt'
#define keyframeHotList				'HotL'
#define keyframeFalloffActionList	'acFo'
#define keyframeFalloffList			'FalL'
#define keyframeHideActionList		'acHd'
#define keyframeHideList			'HidL'
#define keyframeMorphActionList		'acMp'
#define keyframeMorphList			'MorL'
#define keyframeColorActionList		'acCl'
#define keyframeColorList			'ColL'

#define keyAnimFrameNumber			'KAfn'
#define keyAnimFrameFlags			'KAff'
#define keyAnimFrameTension			'KAft'
#define keyAnimFrameContinuity		'KAfc'
#define keyAnimFrameBias			'KAfb'
#define keyAnimFrameEaseTo			'KAet'
#define keyAnimFrameEaseFrom		'KAef'
#define keyAnimFrameRotation		'KAro'
#define keyAnimFrameValue			'KAvl'
#define keyAnimFrameVectorX			'KAvx'
#define keyAnimFrameVectorY			'KAvy'
#define keyAnimFrameVectorZ			'KAvz'
#define keyAnimFrameUseQuat			'KAuq'
#define keyAnimFrameQuatW			'KAqw'
#define keyAnimFrameQuatX			'KAqx'
#define keyAnimFrameQuatY			'KAqy'
#define keyAnimFrameQuatZ			'KAqz'
#define keyAnimFrameClass			'KAFC'
#define keyAnimFrameList			'KAFL'

#define keventToolStartStr "keventToolStart"
#define keventToolStopStr "keventToolStop"
#define krenderStateStr "krenderState"
#define krenderFunctionStr "krenderFunction"
#define ktool3DStr "tool3D"

#define kFrameReaderClass						"FrameReader"
#define kFrameReaderTypeKey						"frameReaderType"

#define kDescVersionKey							"descVersion"
#define kDocumentSizeKey						"documentSize"
#define key3DSceneKey							"key3DScene"
#define key3DDataKey							"key3DData" 
#define key3DFileNameKey						"key3DFileName"
#define key3DFileListKey						"fileList"
#define key3DMeshTexturePathKey					"key3DMeshTexturePath"
#define key3DTexturesExternalKey				"key3DTexturesExternal" 
#define key3DTexturesVisibleKey					"key3DTexturesVisible" 
#define key3DTextureListKey						"key3DTextureList"
#define key3DTextureObjectKey					"key3DTextureObject"
#define key3DTextureNameKey						"key3DTextureName"
#define key3DTexturePathKey						"key3DTexturePath"
#define key3DTextureDataKey						"key3DTextureData"
#define key3DTextureVisibleKey					"key3DTextureVisible"
#define key3DTextureTypeKey						"key3DTextureType"
#define key3DDurationKey						"key3DDuration"
#define key3DScriptKey							"key3DScript"
#define key3DState								"key3DState"
#define key3DPositionKey						"key3DPosition"
#define key3DStateListKey						"key3DStateList"
#define key3DStateNameKey						"key3DStateName"
#define key3DXPosKey							"key3DXPos"
#define key3DYPosKey							"key3DYPos"
#define key3DZPosKey							"key3DZPos"
#define key3DXAngleKey							"key3DXAngle"
#define key3DYAngleKey							"key3DYAngle"
#define key3DZAngleKey							"key3DZAngle"
#define key3DFOVKey								"key3DFOV"
#define key3DSpeedKey							"key3DSpeed"
#define key3DCameraDistanceKey					"key3DCameraDistance"
#define key3DCurrentCameraPositionKey			"key3DCurrentCameraPosition" 
#define key3DCurrentFOVKey						"key3DCurrentFOV" 
#define key3DCurrentPositionKey					"key3DCurrentPosition" 
#define key3DCurrentOrthographicKey				"key3DOrthographic" 
#define key3DCurrentOrthographicScaleKey		"key3DOrthographicScale" 
#define key3DCurrentRenderModeKey				"key3DRenderMode"
#define key3DCurrentLightModeKey				"key3DLightMode"
#define key3DCurrentTimeKey						"key3DTime"
#define key3DCurrentCrossSectionKey				"key3DCrossSection"
#define key3DCrossSectionPlaneColorKey			"key3DCrossSectionPlaneColor"
#define key3DCrossSectionIntersectionColorKey	"key3DCrossSectionIntersectionColor"
#define key3DCrossSectionOffsetKey				"key3DCrossSectionOffset"
#define key3DCrossSectionPlaneTilt1Key			"key3DCrossSectionPlaneTilt1"
#define key3DCrossSectionPlaneTilt2Key			"key3DCrossSectionPlaneTilt2"
#define key3DCrossSectionPlaneOpacityKey		"key3DCrossSectionPlaneOpacity"
#define key3DCrossSectionAlignmentKey			"key3DCrossSectionAlignment"
#define key3DCrossSectionEnabledKey				"key3DCrossSectionEnabled"
#define key3DCrossSectionPlaneFlipKey			"key3DCrossSectionPlaneFlip"
#define key3DCrossSectionPlaneVisibleKey		"key3DCrossSectionPlaneVisible"
#define key3DCrossSectionIntersectionVisibleKey	"key3DCrossSectionIntersectionVisible"
#define key3DCurrentObjectXScaleKey				"key3DCurrentObjectXScale"
#define key3DCurrentObjectYScaleKey				"key3DCurrentObjectYScale"
#define key3DCurrentObjectZScaleKey				"key3DCurrentObjectZScale"
#define key3DAuxilaryColorKey					"key3DAuxilaryColor"
#define key3DFaceColorKey						"key3DFaceColor"
#define key3DOpacityKey							"key3DOpacity"
#define key3DLineWidthKey						"key3DLineWidth"
#define key3DCreaseValueKey						"key3DCreaseValue"
#define key3DViewIndexKey						"key3DViewIndex"
#define key3DEngineIndexKey						"key3DEngineIndex"
#define key3DViewNameKey						"key3DViewName"
#define key3DPaintTypeKey						"key3DPaintType"
#define key3DStateTypeKey						"key3DStateType"
#define key3DTextureFunctionPtr					"key3DTextureFunction"
#define key3DTextureSizeFunctionPtr				"key3DTextureSizeFunction"

#define key3DKeepLayersSeparateKey				"key3DKeepLayersSeparate"

#define keyPolyCount							'plyc'

#define key3DCurrentRenderSettingsKey			"key3DRenderSettings"
#define key3DSecondaryRenderSettingsKey			"key3DRenderSettings2"
#define key3DLineColorKey						"key3DLineColor"
#define key3DVertexColorKey						"key3DVertexColor"
#define key3DRenderFacesKey						"key3DRenderFaces"
#define key3DRenderEdgesKey						"key3DRenderEdges"
#define key3DRenderVerticesKey					"key3DRenderVertices"
#define key3DRenderFaceStyleKey					"key3DRenderFaceStyle"
#define key3DRenderEdgeStyleKey					"key3DRenderEdgeStyle"
#define key3DRenderVerticesStyleKey				"key3DRenderVerticesStyle"
#define key3DRenderAntiAliasKey					"key3DRenderAntiAlias"
#define key3DRenderRayDepthKey					"key3DRenderRayDepth"
#define key3DRenderReflectionsKey				"key3DRenderReflections"
#define key3DRenderRefractionsKey				"key3DRenderRefractions"
#define key3DRenderShadowsKey					"key3DRenderShadows"
#define key3DRenderRemoveBackfacesKey			"key3DRenderRemoveBackfaces"
#define key3DRenderRemoveBackfaceLinesKey		"key3DRenderRemoveBackfaceLines"
#define key3DRenderRemoveBackfaceVerticesKey	"key3DRenderRemoveBackfaceVertices"
#define key3DRenderVolumesKey					"key3DRenderVolume"
#define key3DRenderSeteroKey					"key3DRenderStereo"
#define key3DRenderVolumeStyleKey				"key3DRenderVolumeStyle"
#define key3DRenderStereoStyleKey				"key3DRenderStereoStyle"
#define key3DRenderGradientEnhancedKey			"key3DRenderGradientEnhanced"
#define key3DStereoOffsetKey					"key3DStereoOffset"
#define key3DStereoSpacingKey					"key3DStereoLenticularSpacing"
#define key3DStereoFocalPlaneKey				"key3DStereoFocalPlane"
#define key3DVertexRadiusKey					"key3DVertexRadius"
#define key3DRenderAdvancedIllumKey				"key3DRenderAdvancedIllumVideo"
#define key3DRenderRemoveHiddenLinesKey			"key3DRenderRemoveHiddenLines"
#define key3DRenderRemoveHiddenVerticesKey		"key3DRenderRemoveHiddenVertices"
#define key3DRenderOpacityScaleKey				"key3DRenderOpacityScale"
#define key3DShowGroundPlaneKey					"key3DShowGroundPlane"
#define key3DShowLightsKey						"key3DShowLights"

#define toolBadMode -31000	/* the tool is not read for external commands */


#if WIN32
#pragma pack(push,4)
#endif

#if PRAGMA_STRUCT_ALIGN
#pragma options align=mac68k
#endif


//  Callback from device plugin to ask 3D tool to update
//  idleProc is a RenderDeviceIdleFunction - but one has to be defined first
#define k3DRenderPluginUpdateTypeSetIdleFunc	0
#define k3DRenderPluginUpdateTypeRemoveIdleFunc	1
#define k3DRenderPluginUpdateTypeUpdateState	2
typedef MACPASCAL OSErr (*RenderUpdate) (int32 updateType, PIActionDescriptor stateDescriptor, void *idleProc);

/// Callback from app to plugin to give it time
//Plugins fill in the doInteraction flag to say if they need to be active
//We will sit in a loop until this is false
//Plugins should call RenderUpdate when idled to get the state changed
#define k3DRenderAppUpdateTypeCheckInteraction		0
#define k3DRenderAppUpdateTypeSetState				1
#define k3DRenderAppUpdateTypeDoInteraction			2

typedef MACPASCAL OSErr (*RenderDeviceIdleFunction) (int32 updateType, Boolean *doInteraction, PIActionDescriptor stateDescriptor, RenderUpdate updateProc);

typedef int16			U3DAlignment;
typedef int16			RenderQuality;
typedef int16			ToolID;
typedef uint16			U3DInterfaceString[512];

#define PI3D			3.1415926535897932384626433832795
#define D2R				( PI3D / 180 )
#define R2D				( 180 / PI3D )

typedef nativeFloat		PI3DVector[3];
typedef nativeFloat		PI3DPoint[2];
typedef nativeFloat		PI3DMatrix[4][4];

#define kCurrenr3DMajorVersion			1
#define kCurrenr3DMinorVersion			0

#define kNumSupportedTextureTypes		9
#define PI3DTextureMap					0
#define PI3DSphereEnvironmentMap		1
#define PI3DBumpMap						2
#define PI3DSpecularMap					3
#define PI3DOpacityMap					4
#define PI3DShininessMap				5
#define PI3DSelfIlluminationMap			6
#define PI3DDepthMap					7
#define PI3DReflectionMap				8


typedef enum {
	PI3DLinearAttenuation=0,
	PI3DABCAttenuation
} PI3DAttenuationEnum;

typedef struct
	{
		nativeFloat a;
		nativeFloat b;
		nativeFloat c;
	} PI3DABCAtt;

typedef struct
	{
		nativeFloat w;
		nativeFloat x;
		nativeFloat y;
		nativeFloat z;
	} PI3DQuat;

typedef struct 
	{
		PI3DVector axis; // origin, direction and angle
		nativeFloat angle;
	} PI3DAngleAxis;

typedef enum {
	PI3DWireFrame=0x0001,
	PI3DFlat,
	PI3DGouraud,
	PI3DPhong,
	PI3DMetal
} PI3DShadingEnum; // shading type, NOT USED EXCEPT FOR WIREFRAME RIGHT NOW

typedef enum {
	PI3DSpotLight=0x0001,
	PI3DPointLight,
	PI3DInfiniteLight, 
	PI3DGlobalAmbientLight // This type of light is only found in U3D files so far
	// It will be added to the global ambient (see T3DCurrentScene::PI3DParseDescriptorIntoScene())
	// and not used as separate light source
} PI3DLightEnum;

typedef enum {
	PI3DClampX = 0x0001,
	PI3DClampY = 0x0002,
	PI3DStyle_3DS = 0x0004, //3D Studio style map
	PI3DModulate = 0x0008,
	PI3DColorMap = 0x0010,
	PI3DBumpNormal= 0x0010,
	PI3DInvert = 0x0020
} PI3DTextureMapFlags;

typedef enum {
	PI3DAmbientNode,	
	PI3DObjectNode,
	PI3DCameraNode,
	PI3DTargetNode,
	PI3DLightNode,
	PI3DLtargetNode,
	PI3DSpotlightNode,
	PI3DGroupNode //used for PI3D and other formats that have parent nodes which are not meshes or some other object
} PI3DNodeTypes;

typedef struct {
    char				name[80];
    void				*next;
} PI3DList;

typedef struct {
    nativeFloat			red;
	nativeFloat			green;
	nativeFloat			blue;
} PI3DColor;

typedef struct {
	int64				start;
	int64				end;
	int64				length;
	uint16				tag;
} PI3DChunk;

typedef struct {
	uint8				red;
	uint8				green;
	uint8				blue;
} PI3DColor24;

/* Face flags */
//Edge 1														0x001
//Edge 2														0x002
//Edge 3														0x004
//Edges															0x007
//Double sided													0x010
//Selected triangle												0x20
//Triangle visible												0x40
//Triangle visible as edge										0x80
//Next face is hole, and additional space for points allocated  0x100
//This face is hole, in previous face							0x200
//This is triangle strip										0x400
//Subdivision patch.											0x800

typedef struct {
	int32				numPoints;		/* number of points in the face */
    int32				*points; 
	int32				flags;			/* Face flags - see above */
	int32				smoothing;		/* smoothing value */
	int32				material;		/* material index */
	int32				*normals;		/* normals if you have them */
	int32				*colors;		/* colors if you have them */
	int32				*textures; 
		
	uint32				faceID;
} PI3DFace;


typedef struct {
    //Standard list fields
	char				name[80];
	void				*next;
	
	PI3DLightEnum		type;				/* light type */
    PI3DVector			pos;				/* light position */
    PI3DVector			target;				/* light target location */
    PI3DColor			col;				/* light colour */
    nativeFloat			hotspot;			/* Hotspot angle (degrees) */
    nativeFloat			falloff;			/* Falloff angle (degrees) */
    int32				shadowFlag;			/* Shadow flag (not used) */
	Boolean				attenuation;		/* Use attenuation */
	PI3DAttenuationEnum	attenuationType;	/* linear or ABC */
	PI3DABCAtt			attenuationAbc;		/* use if you have ABC attenuation */
	nativeFloat			outerRadius;		/* outer range */
	nativeFloat			innerRadius;		/* inner range */
	nativeFloat			multiple;			/* multiplier */
	
	Boolean				cameraBound;		/* true if light shall be bound to camera instead of world */
	PI3DMatrix			rendermatrix;				/* Render transformation matrix - filled in by photoshop at render time */
	
	char				padding[383];
} PI3DLight;


/* Camera */
typedef struct {
    //Standard list fields
	char				name[80];
	void				*next;
	
    PI3DVector			pos;				/* Camera location */
    PI3DVector			target;				/* Camera target */
    nativeFloat			bank;				/* Banking angle (degrees) */
    nativeFloat			lens;				/* Camera lens size (mm) */
	Boolean				ortho;				/* use orthographic */
	nativeFloat			aspectratio;		/* Aspect Ratio */
	nativeFloat			zoomfactor;			/* zoom factor */
	
	PI3DMatrix			rendermatrix;				/* Render transformation matrix - filled in by photoshop at render time */
	
	char				padding[384];
} PI3DCamera;


typedef struct {
	char					map[2048];
    nativeFloat				strength;		/* intensity of the map */
	nativeFloat				uscale;  
	nativeFloat				vscale;
	nativeFloat				uoffset;
	nativeFloat				voffset;
	nativeFloat				angle;
	PI3DTextureMapFlags		flags;
	Boolean					mapIs3DSStyle;	/* Needed only by the 3ds importer */
	
	int32					photoshopTextureID; /* filled in by Photoshop on load */
	char					padding[508];
} PI3DTextureMatProp;

typedef struct {
    //Standard list fields
	char				name[80];
	void				*next;
	
    PI3DColor			ambient;
    PI3DColor			diffuse;
    PI3DColor			specular;
	PI3DColor			emissive;
    nativeFloat			shininess;
	nativeFloat			glossiness;			/* specular level */
    nativeFloat			transparency;
    nativeFloat			reflection;
    int32				selfIllumination;	/* unused */
	PI3DShadingEnum		shading;
	PI3DTextureMatProp	maps[kNumSupportedTextureTypes];
	Boolean				twoSided;
	Boolean				wireframe;
	Boolean				decal;
	nativeFloat			wireframesize;
	uint32				importdata;			/* reserved, do not use */
	
	char				padding[512];
} PI3DMaterial;

typedef struct U3DBoundingBox
	{
		nativeFloat				minX;	
		nativeFloat				maxX;	
		nativeFloat				centerX;	
		
		nativeFloat				minY;	
		nativeFloat				maxY;	
		nativeFloat				centerY;	
		
		nativeFloat				minZ;	
		nativeFloat				maxZ;	
		nativeFloat				centerZ;	
		
	}
	U3DBoundingBox, *U3DBoundingBoxPtr;

typedef struct 
	{
		//Standard list fields
		void		*next;
		
		//Data field: index of the connected vertex
		int32		connectedVertex;
		int32		edgeIndex;
	} 
	PI3DVertexConnection;

typedef struct 
	{
		int32				eVertex[2];		/* Connecting vertices indexes */
		int32				eNumFaces;		/* Number of shared faces. E.g. for close solids, these should always be 2 */
		int32				*eFaces;		/* List of shared faces */
		nativeFloat			faceNormalDotProduct; /* Product between two faces(if there is 2) */
	} 
	PI3DEdge;	

typedef struct {
    //Standard list fields
	char				name[80];
	void				*next;
	
    int32				vertices;					/* Number of vertices */
    PI3DVector			*vertex;					/* List of object vertices */
    int32				textures;					/* Number of textures */
    PI3DPoint			*texture;					/* List of object textures */
    int32				faces;						/* Number of faces */
    PI3DFace			*face;						/* List of object faces */
	int32				lines;						/* number of lines */
	PI3DVector			*linecolors;				/* per line color - rgb*/
	PI3DVector			*line;						/* line pts */
	int32				*linematerial;				/* line material index */
	int32				flags;						/* NOT USED */
    int32				hidden;						/* Hidden flag */
    int32				shadow;						/* Shadow flag */
	PI3DMatrix			matrix;						/* Mesh transformation matrix */
	Boolean				matrixPresent;				/* is there a transformation matrix */
	Boolean				smoothingGroupPresent;		/* is there a smoothing group */
    int32				normals;					/* Number of normals */
    PI3DVector			*normal;					/* List of object normals */
	int32				colors;						/* Number of colors */
	PI3DVector			*color;						/* List of vertex colors */
	
	U3DBoundingBox		bbox;						/* Per-mesh bounding box */
	PI3DEdge			*edge;
	int32				edges;
	PI3DVertexConnection	**vconnect;				
	
	Boolean				isVolume;					/* Is this mesh a volume */
	PI3DMatrix			rendermatrix;				/* Render transformation matrix - filled in by photoshop at render time */
	
	char				padding[291]; //432
} PI3DMesh;

/* Frame vector flags */
//Start cyclic							0x010
//End cyclic							0x10
//Linear								0x00000
//Bezier								0x10000 NOT USED RIGHT NOW
//TCB(Use the tension continuity...)	0x20000 

typedef struct {
	nativeFloat			framenumber;
	int32				flags;			/* Frame vector flags - see above*/
	nativeFloat			tension;
	nativeFloat			continuity;
	nativeFloat			bias;
	nativeFloat			easeto;
	nativeFloat			easefrom;
	nativeFloat			rotation;		//radians
	nativeFloat			value;			//FOV, HOT, FALL, ROLL - in degrees
	PI3DVector			v;				//POS, SCL, ROT(angleaxis value)
	Boolean				usequat;		//if true using q if not using v
	PI3DQuat			q;				//ROT(quaternion)
	
	char				padding[512];
} PI3DFrameVector;

typedef struct {
	//Standard list fields
	char				name[80];				/* name of animation segment */
	void				*next;
	
	int32				flags;
	int32				keys;					/* number of keyframes */
	PI3DFrameVector*	aFrameData;
	
	char				padding[512];
} PI3DAnimationData;

/* A key frame object */
typedef struct {
    //Standard list fields
	char				name[80];
	void				*next;
	
	char				instancename[80];		/* instance name for this object */
	int32				nodeID;					/* node id */
	int32				parentNodeID;			/* parent node id - 65535 means scene is parent */
	int32				flags1;
	int32				flags2;
	
	PI3DVector			pivot;
	Boolean				usepivot;				/* if not using pivot, local matrix will be set for keyframes that do not have animation data */
	Boolean				matrixPresent;
	PI3DMatrix			localmatrix;
	
	Boolean				userange;
	PI3DNodeTypes		nodeType;
	
	PI3DVector			bboxmin;				/* bounding box */
	PI3DVector			bboxmax;
	
    PI3DAnimationData	*positionData;
    PI3DAnimationData	*rotationData;
    PI3DAnimationData	*scaleData;
	PI3DAnimationData	*colData;
	PI3DAnimationData	*fOVData;
	PI3DAnimationData	*hotData;				/* not in use */
	PI3DAnimationData	*fallData;				/* not in use */
	PI3DAnimationData	*rollData;
	PI3DAnimationData	*hideData;				/* not in use */
	PI3DAnimationData	*morphData;				/* not in use */
	
	char				padding[512];
} PI3DKeyFrame;


typedef struct {
	PI3DLight			*lightList;
	PI3DCamera			*cameraList;
	PI3DMesh			*meshList;
	PI3DMaterial		*matPropList;
	PI3DKeyFrame		*keyFrameList;
	PI3DColor			globalAmbientColor;
	nativeFloat			globalUnits;					/* measurement units in inches */
	char				globalRange[80];				/* name of animation segment to activate */
	int32				globalAnimationStartTime;
	int32				globalNumFrames;
	nativeFloat			globalFPS;						/* frames per second */
	
	PI3DColor			lightingSchemeGlobalAmbientColor; /* We set this color in our lighting scheme */
	/* e.g. white lights, hard lights, etc.     */
	char				name[80];
	char				padding[408];
} PI3DScene;



typedef struct U3DPosition
	{
		nativeFloat				x;								// x location
		nativeFloat				y;								// y location
		nativeFloat				z;								// z location
		nativeFloat				xAngle;							// x angle
		nativeFloat				yAngle;							// y angle
		nativeFloat				zAngle;							// z angle	
		
		/* Reserved for future expansion */
		char					reserved [128]; 				// Set to zero 
	}
	U3DPosition, *U3DPositionPtr;

typedef struct U3DCrossSection
	{
		nativeFloat				planeColor[3];
		nativeFloat				intersectionColor[3];
		nativeFloat				offset;
		nativeFloat				planeTilt1;
		nativeFloat				planeTilt2;
		nativeFloat				planeOpacity;
		U3DAlignment			alignmentMode;
		Boolean					crossSectionEnabled;
		Boolean					planeFlip;
		Boolean					planeVisible;
		Boolean					intersectionVisible;
		
		/* Reserved for future expansion */
		char					reserved [512]; 				// Set to zero 
	}
	U3DCrossSection, *U3DCrossSectionPtr;

#define kMaxTransferFunctionValue	32767
#define kTransferFunctionSize		4097
typedef uint16			U3DTransferFunction[4][kTransferFunctionSize];

typedef struct 
	{
		//Faces
		Boolean				renderFaces;				//Flag for controlling whether or not to render faces
		uint32				faceStyle;					//Constant, Flat, Smooth, Bounding Box, Ray Traced, Unlit Texture, Face IDs, Depth, Normals
		nativeFloat			faceColor[3];				//Face colors in illustration modes
		uint32				textureType;				//When rendering diffuse only, what kind of texture do we display
		uint32				antiAliasQuality;			//1, 2, 4
		uint32				rayDepth;					//# of bounces
		Boolean				showReflections;			//Flag for controlling whether or not to render reflections
		Boolean				showRefractions;			//Flag for controlling whether or not to render refractions
		Boolean				showShadows;				//Flag for controlling whether or not to render shadows
		Boolean				useAdvancedIllumination;	//Flag for controlling whether or not to render using advanced illumination
		Boolean				removeBackfaces;			//Flag for controlling whether or not to remove back facing polygons
		
		
		//Edges
		Boolean				renderEdges;				//Flag for controlling whether or not to render edges
		uint32				lineStyle;					//Constant, Flat, Smooth, Bounding Box, Illustrative
		nativeFloat			lineColor[3];				//Line colors in illustration modes
		nativeFloat			lineWidth;					//Width of wireframe lines
		nativeFloat			creaseValue;				//Threshold angle for illustration angle
		Boolean				removeBackfaceLines;		//Flag for controlling whether or not to remove back facing lines
		Boolean				removeHiddenLines;			//Flag for controlling visibility of lines thatw oudl not be seen in full face rendering
		
		//Vertices
		Boolean				renderVertices;				//Flag for controlling whether or not to render vertices
		uint32				vertexStyle;				//Constant, Flat, Smooth, Bounding Box
		nativeFloat			vertexColor[3];				//Vertex colors in illustration modes	
		nativeFloat			vertexRadius;				//Radius of drawn vertices
		Boolean				removeBackfaceVertices;		//Flag for controlling whether or not to remove back facing vertices
		Boolean				removeHiddenVertices;		//Flag for controlling visibility of vertices thatw oudl not be seen in full face rendering
		
		//Volumes	
		Boolean				renderVolume;				//Flag for controlling whether or not to render volumes
		uint32				volumeStyle;				//MIP, Alpha
		U3DTransferFunction	transferFunction;			//Volume lookup
		Boolean				gradientEnhancement;		//Enhance Gradients	
		nativeFloat			opacityScale;				//Coefficient for scaling the opacity of each voxel
		
		//Stereo	
		Boolean				renderStereo;				//Flag for controlling whether or not to render in stereo
		uint32				stereoStyle;				//Red-Blue, Vertical Interlaced, Horizontal Interlaced
		nativeFloat			stereoOffset;				//Eye separation in bbox coordinates
		nativeFloat			stereoLineSpacing;			//Spacing to use for interlacing
		nativeFloat			focalPlane;					//Plane that the stereo camera focus on. Zero is the cenetr of the bounding box, -100 is the back and 100 is the front.
		
		//For Photoshop use only
		int16				projection;					//Rendering into textures
		Boolean				blendUnlitWithBaseValue;	//Flag to determine how unlit texture mode gets drawn
		Boolean				drawGroundPlane;			//Include ground plane in rendering
		Boolean				drawLights;					//Include light widgets in rendering
		Boolean				videoAtHighestQuality;		//Turn on ray tracing with advanced illumination when rendering video 
	}
U3DRenderSettings;

typedef struct 
	{
		uint32					length;							// number of textures
		uint16					**textureNames;					// texture names
		uint16					**texturePaths;					// texture paths
		uint32					*textureType;					// flags to indicate the type of texture
		/* Reserved for future expansion */
		char					reserved [508]; 				// Set to zero 
	} 
	U3DTextureArray;

typedef struct RenderDrawBuffer
	{
		void				*pixels;
		int32				rowBytes;
		VPoint				sceneSize;
		VRect				tileRect;
		PlaneMap			planeMap;							/* Maps plug-in plane numbers to host plane
		 numbers.  The host initializes this is a linear
		 mapping.  The plug-in may change this mapping if
		 it sees the data in a different order. */
		/* Reserved for future expansion */
		int32				bitsPerPixel;
		char				reserved [508]; 					// Set to zero 
		
	} 
	RenderDrawBuffer, *RenderDrawBufferPtr;


/// Callback from plugin to get texture data
typedef MACPASCAL OSErr (*GetTextureSizeFunction) (int32 index, RenderDrawBuffer *buffer);
typedef MACPASCAL OSErr (*GetTextureFunction) (int32 index, RenderDrawBuffer *buffer);
typedef MACPASCAL void  *(*Malloc3D) (uint32 size);
typedef MACPASCAL void (*Free3D) (void *mem);
typedef uint32			U3DStateType;

typedef struct 
	{
		//State Data - set to defaults by plugin and saved in PSD
		U3DPosition			currentCameraPosition;				//Current camera position
		nativeFloat			currentFieldOfView;					//Field of view of camera (degrees)
		nativeFloat			currentObjectXScale;				//Scale applied to the object by the user
		nativeFloat			currentObjectYScale;				//Scale applied to the object by the user
		nativeFloat			currentObjectZScale;				//Scale applied to the object by the user
		U3DPosition			currentObjectPosition;				//Current object position
		Boolean				currentOrthographic;				//Is the camera currently in orthographic mode
		uint32				currentRenderMode;					//Current render mode
		uint32				currentLightMode;					//Current light mode
		nativeFloat			currentTime;						//Current time of the play head in seconds
		U3DCrossSection		currentCrossSection;				//Data to define the current cross section	
		nativeFloat			currentAuxilaryColor[3];			//Line colors in illustration modes
		nativeFloat			currentFaceColor[3];				//Face colors in illustration modes
		nativeFloat			currentOpacity;						//Model opacity
		nativeFloat			currentLineWidth;					//Width of wireframe lines
		nativeFloat			currentCreaseValue;					//Threshold angle for illustration angle
		nativeFloat			currentOrthoScale;					//Scale for orthorgraphic view 1/x
		//added for CS4
		U3DRenderSettings	currentRenderSettings;				//Data to define the current render settings
		PI3DColor			currentGlobalAmbient;				//Global ambient color
		U3DStateType		stateType;
		nativeFloat			documentResolution;					// The document's resolution- used for lenticulars

		//Interaction Data - set by Photoshop per render
		RenderQuality		quality;							//Quality level of final output
		uint32				currentTexture;						//Set before calling update texture
		uint16				currentTexturePath[2048];			//Set before calling update texture
		
		U3DBoundingBox		currentBoundingBox;					//Set by Plugin on SetObjectPosition
		
		
		RenderDrawBuffer	drawBuffer;							//Used for passing texture updates
		
		void				*deviceIdler;						//Unused
		int32						currentToolMode;			//Kept current by PS.  Set by controller plugin when requesting tool mode change.
		int32						currentToolID;				//Kept current by PS.  Set by controller plugin when requesting tool id change.
		
		
		GetTextureSizeFunction		getTextureSizeFunction;
		GetTextureFunction			getTextureFunction;
		
		int32				faceCount;
		uint32				*faceList;
		
		uint16				paintingTextureTarget;				//Identifies to the engine, which texture is currently painted on
		//E.g. PI3DTextureMap, PI3DBumpMap
		//App data
		void				*appData;							//Used in callbacks to PS
		/* Reserved for future expansion */
		char				reserved [478]; 					// Set to zero 
	}
	RenderState, *RenderStatePtr;


typedef struct 
	{
		uint32					length;							// number of cameras
		uint16					**positionNames;				// position names
		RenderState				*viewStates;					// states of current view
		
		/* Reserved for future expansion */
		char					reserved [512]; 				// Set to zero 
	} 
	U3DStateArray;

typedef struct 
	{
		void				*engine;							//Pointer to a 3D engine instance
		//This should be passed back from PrepareU3DRenderer
		//It will be provided in all render calls
		Boolean				supportsOrthographic;				//Can this renderer do orthographic?
		Boolean				supportsCrossSection;				//Can this renderer render a cross section?
		uint16				numRenderModes;
		uint16				**renderModeList;					//List of known render modes
		uint16				numLightModes;
		uint16				**lightModeList;					//List of known light modes
		Boolean				useHardware;						//Flag to determine whether or not to use hardware acceleration
		
		PI3DLight			*lightList;							//Actual lights that map to the light mode names
		Boolean				supportsPainting;					//Can this engine support painting
		
		
		/* Reserved for future expansion */
		char				reserved [507]; 					// Set to zero 
		
	}
	RendererInfo, *RendererInfoPtr;


typedef struct RenderRecord
	{
		//File Data	- retrieved when the file is loaded and saved in PSD
		SPPlatformFileSpecificationW	*fileSpec2;							//Location of 3D data on disk
		PIDescriptorHandle				sceneDescriptor;					//This will contain the scene data
		SPBasicSuite					*sSPBasic;							//SuitePea basic suite
		uint8							*data;								//Pointer to File Data
		//This should be filled in on ReadContinue and will be provided on PrepareU3DRenderer and on all writing calls
		Boolean							mustAllocateData;					//If the host allocated the data, we need to allocate a copy of our own
		uint32							dataSize;							//Size of the U3D data
		uint16							dataType;							//Type of data
		nativeFloat						duration;							//Duration of animation in the file in seconds
		U3DStateArray					stateList;							//List of known states
		U3DTextureArray					textureList;						//List of textures in the file
		Boolean							texturesExternal;					//Set to true if the textures were not contained inside the original 3D file
		uint16							*savedScript;						//For File Loading: Any script associated with the file
		
		RenderDrawBuffer				drawBuffer;
		//State Data - set to defaults by plugin and saved in PSD	
		RenderState						renderState;
		
		//Engine Data - not needed for file and not saved per instance - sent to PS in prepareRenderer
		RendererInfo					rendererInfo;
		
		//Initial Bounding Box of Scene	
		U3DBoundingBox					initialBoundingBox;					//Bounding box on initial load
		
		uint16							*meshTexturePath;					//Path to texture to use for the mesh
		
		//Memory allocation that needs to be turned over to the app	
		Malloc3D						mallocFunc;
		Free3D							freeFunc;
		
		
		/* Reserved for future expansion */
		char							reserved [504]; 					// Set to zero 
	}
	RenderRecord, *RenderRecordPtr;


#if WIN32
#pragma pack(pop)
#endif

#if PRAGMA_STRUCT_ALIGN
#pragma options align=reset
#endif

#endif  // __PI3D_h__

#endif //__AGF__
