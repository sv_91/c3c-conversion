#ifndef ABOUT_H_
#define ABOUT_H_

#define USING_MFC

#include <Stdafx.h>
#include "afxcmn.h"
#include "resource.h"
#include "afxwin.h"

#undef min
#undef max

class About : public CDialogEx {
	DECLARE_DYNAMIC(About)
public:

	About(CWnd* pParent = NULL);   // standard constructor
	virtual ~About();

	enum { IDD = IDD_DIALOG1 };

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	BOOL About::OnInitDialog();

	DECLARE_MESSAGE_MAP()

public:

	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	CEdit textAbout;

	CEdit textAbout2;

	afx_msg void OnBnClickedButton1();
};

#endif // ABOUT_H_
