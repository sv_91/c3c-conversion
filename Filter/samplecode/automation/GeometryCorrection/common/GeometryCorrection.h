#ifndef __GeometryCorrection_H__
#define __GeometryCorrection_H__

#include "SPHost.h"
#include "PIActionsPlugIn.h"
#include "PIUtilities.h"
#include "PITerminology.h"

#include "commonFilter.h"

class GeometryCorrectionData {
public:

	/// Run the plug-in from this routine
	void DoIt(PSActionsPlugInMessage * message);

	/// Functions needed by GeometryCorrectionDialog
	void Execute(void);
};

#endif
