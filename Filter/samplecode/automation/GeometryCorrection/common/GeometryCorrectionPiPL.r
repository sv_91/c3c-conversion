#include "PIDefines.h"

#define plugInName "Geometry Correction"
#define plugInDescription \
	"Geometry correction automate filter"

#ifdef __PIMac__
	#include "Types.r"
	#include "SysTypes.r"
	#include "PIGeneral.r"
	#include "PIUtilities.r"
#elif defined(__PIWin__)
	#define Rez
	#include "PIGeneral.h"
	#include "PIUtilities.r"
#endif

#include "PITerminology.h"
#include "PIActions.h"

resource 'PiPL' ( 16000, "Geometry Correction", purgeable)
	{
		{
		Kind { Actions },
		Name { plugInName"..." },
		Category { "Geometry Correction" },
		Version { (latestActionsPlugInVersion << 16) | latestActionsPlugInSubVersion },

		#ifdef __PIMac__
			#if (defined(__i386__))
				CodeMacIntel32 { "AutoPluginMain" },
			#endif
			#if (defined(__ppc__))
				CodeMachOPowerPC { 0, 0, "AutoPluginMain" },
			#endif
		#else
			#if defined(_WIN64)
				CodeWin64X86 { "AutoPluginMain" },
			#else
				CodeWin32X86 { "AutoPluginMain" },
			#endif
		#endif

        EnableInfo { "in (PSHOP_ImageMode, GrayScaleMode, RGBMode, RGB48Mode) ||"
                           "PSHOP_ImageDepth == 16"},		
		HasTerminology
			{ 
			'AuFi', 
			'AuFi', 
			16000, 
			"adc931a0-cfe2-11d5-98bf-00b0d0204936"
			},
		}
	};

//-------------------------------------------------------------------------------
//	Dictionary (scripting) resource
//-------------------------------------------------------------------------------

resource 'aete' (16000, "Geometry Correction dictionary", purgeable)
	{
	1, 0, english, roman,					/* aete version and language specifiers */
		{
		"Testing",							/* vendor suite name */
		"Geometry Correction",			    /* optional description */
		'AuFi',								/* suite ID */
		1,									/* suite code, must be 1 */
		1,									/* suite level, must be 1 */
			{								/* structure for automation */
			"Automation Filter",						/* name */
			"No comment",					/* optional description */
			'AuFi',							/* class ID, must be unique or Suite ID */
			'AuFi',							/* event ID, must be unique */

			NO_REPLY,						/* never a reply */
			IMAGE_DIRECT_PARAMETER,			/* direct parameter, used by Photoshop */
				{							// filter or selection class here:
				}
			},
			{},	/* non-filter/automation plug-in class here */
			{}, /* comparison ops (not supported) */
			{ // Enumerations go here:
			}	/* end of any enumerations */
		}
	};
// end GeometryCorrectionPiPL.r
