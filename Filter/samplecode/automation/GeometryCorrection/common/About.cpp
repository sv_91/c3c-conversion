#include "stdafx.h"
#include "About.h"
#include "afxdialogex.h"

#include "commonFilter.h"

IMPLEMENT_DYNAMIC(About, CDialogEx)

About::About(CWnd* pParent /*=NULL*/)
	: CDialogEx(About::IDD, pParent)
{}

About::~About() {
}

void About::DoDataExchange(CDataExchange* pDX) {
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, textAbout);
	DDX_Control(pDX, IDC_EDIT2, textAbout2);
}

BOOL About::OnInitDialog() {
	CDialogEx::OnInitDialog();

	logDebug("Ya tuta22011\n");

	std::string text = std::string("Suvorov Ivan\r\n")
					 + std::string("sv_91@inbox.ru");

	textAbout2.SetWindowTextA(text.c_str());

	return true;
}

BEGIN_MESSAGE_MAP(About, CDialogEx)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON1, &About::OnBnClickedButton1)
END_MESSAGE_MAP()

// About message handlers
void About::OnLButtonDown(UINT nFlags, CPoint point) {
	logDebug("Filter ya tuta\n");
	OnCancel();

	CDialogEx::OnLButtonDown(nFlags, point);
}

void About::OnBnClickedButton1() {
	OnCancel();
}
