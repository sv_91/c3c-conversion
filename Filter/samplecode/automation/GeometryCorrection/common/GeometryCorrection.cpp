#include "About.h"

#include "GeometryCorrection.h"
#include "GeometryCorrectionGUIUniqueString.h"
#include "GeometryCorrectionFilterUniqueString.h"
#include "FilterKeys.h"

#include "version.h"

#include <ctime>

#undef min
#undef max

SPErr gError = kSPNoError;
SPBasicSuite *sSPBasic;

SPErr Execute(void *parameters);

void drawAbout() {
	logDebug("Draw about\n");
	HWND hParent = GetActiveWindow();
	About dial(CWnd::FromHandle(hParent));

	INT_PTR result = dial.DoModal();
	logDebug("Close about\n");
}

//-------------------------------------------------------------------------------
//
//	AutoPluginMain
//
//	All calls to the plug-in module come through this routine.
//	It must be placed first in the resource.  To achieve this,
//	most development systems require this be the first routine
//	in the source.
//
//	The entrypoint will be "pascal void" for Macintosh,
//	"void" for Windows.
//
//-------------------------------------------------------------------------------
DLLExport SPAPI SPErr AutoPluginMain(
	const char* caller,	// who is calling
	const char* selector, // what do they want
	void* message	// what is the message
) {
	createFile("C:/� ����� D/����������/1 �������/������/", "outAutomate.txt");
	logDebug("Version %s\n", VERSION);

	gError = kSPNoError;

	try {
		//all messages contain a SPMessageData*
		SPMessageData* basicMessage;
		basicMessage = (SPMessageData*)message;
		sSPBasic = basicMessage->basic;

		// check for SP interface callers
		if (sSPBasic->IsEqual(caller, kSPInterfaceCaller)) {
			// pop an about box
			if (sSPBasic->IsEqual(selector, kSPInterfaceAboutSelector))	{
				drawAbout();
			}
		}

		// Photoshop is calling us
		if (sSPBasic->IsEqual(caller, kPSPhotoshopCaller)) {
			// the one and only message 
			if (sSPBasic->IsEqual(selector, kPSDoIt)) {
				GeometryCorrectionData afData;
				//now that we know more we can cast the message to a PSActionsPlugInMessage*
				PSActionsPlugInMessage* actionsMessage = (PSActionsPlugInMessage*)message;

				afData.DoIt(actionsMessage);
			}
		}
	} catch (std::string err) {
		logDebug("Poimal error %s\n", err.c_str());
		gError = kSPBadParameterError;
	} catch (SPErr err) {
		logDebug("Poimal error %d\n", err);
		gError = kSPBadParameterError;
	} catch(...) {
		logDebug("Poimal error2 %d\n", gError);
		gError = kSPBadParameterError;
	}

	return gError;
}

void PlayeventCanvasSize(int width, int height) {
	logDebug("Canvas size call %d %d\n", width, height);

	Auto_Desc imageResizeDesc;
	Auto_Desc result;

	checkSPErr(sPSActionDescriptor->PutUnitFloat(imageResizeDesc.get(), keyWidth, unitPixels, width));
	checkSPErr(sPSActionDescriptor->PutUnitFloat(imageResizeDesc.get(), keyHeight, unitPixels, height));

	checkSPErr(sPSActionDescriptor->PutEnumerated(imageResizeDesc.get(), keyHorizontal, typeHorizontalLocation, enumLeft));
	checkSPErr(sPSActionDescriptor->PutEnumerated(imageResizeDesc.get(), keyVertical, typeVerticalLocation, enumTop));

	checkSPErr(sPSActionControl->Play(&result, eventCanvasSize, imageResizeDesc.get(), plugInDialogSilent));

	logDebug("Canvas size call: ok\n");
}

void PlayeventFilterGeometryCorrectionFilter(
	Auto_Desc &inParameters
) {
	logDebug("GeometryCorrectionFilter call\n");

	DescriptorTypeID GeometryCorrectionFilterEventID;
	checkSPErr(sPSActionControl->StringIDToTypeID(GeometryCorrectionFilterUniqueString,	&GeometryCorrectionFilterEventID));

	Auto_Desc result = NULL;
	checkSPErr(sPSActionControl->Play(&result, GeometryCorrectionFilterEventID, inParameters.get(), plugInDialogSilent));

	logDebug("GeometryCorrectionFilter call: ok\n");
}

void PlayeventDrawDialogFilter(
	Auto_Desc &result,
	bool &isCancel, 
	int32 &width, int32 &height,
	int32 &widthNew, int32 &heightNew
) {
	logDebug("Draw dialog call\n");

	Auto_Desc descriptor;

	DescriptorTypeID GeometryCorrectionGUIEventID;
	checkSPErr(sPSActionControl->StringIDToTypeID(GeometryCorrectionGUIUniqueString,	&GeometryCorrectionGUIEventID));

	// TODO �� ����, ��� ������ ������ �����-�� playinfo
	checkSPErr(sPSActionControl->Play(&result, GeometryCorrectionGUIEventID, descriptor.get(), plugInDialogSilent));

	Boolean isCancelTmp;
	checkSPErr(sPSActionDescriptor->GetBoolean(result.get(), keyIsCancel, &isCancelTmp));
	isCancel = isCancelTmp != 0;
	if (isCancel) {
		return;
	}

	checkSPErr(sPSActionDescriptor->GetInteger(result.get(), keyWidthImage, &width));
	checkSPErr(sPSActionDescriptor->GetInteger(result.get(), keyHeightImage, &height));

	checkSPErr(sPSActionDescriptor->GetInteger(result.get(), keyWidthNew, &widthNew));
	checkSPErr(sPSActionDescriptor->GetInteger(result.get(), keyHeightNew, &heightNew));

	logDebug("Draw dialog call: ok\n");
}

void GeometryCorrectionData::DoIt(PSActionsPlugInMessage* message) {
	check(message != NULL);

	Auto_Ref reference;
	PIActionParameters* actionParams = message->actionParameters;

	ASZString nameAsZString = NULL;
	sPSUIHooks->GetPluginName(message->d.self, &nameAsZString);

	checkSPErr(sPSActionReference->PutEnumerated(reference.get(), classDocument, typeOrdinal, enumTarget));

	// If your execute routine error's you will not get the
	// error value from SuspendHistory for version 5.5 of Photoshop.
	// This was fixed for Photoshop 6.0. I am going to use a global error value
	// to work around this problem.
	checkSPErr(sPSActionControl->SuspendHistory(reference.get(), ::Execute,	actionParams, nameAsZString));

	checkSPErr(sASZString->Release(nameAsZString));
}


//-------------------------------------------------------------------------------
//
//	Execute
//	
//	A helper routine. Get back inside the class and Execute from inside there.
//
//-------------------------------------------------------------------------------
SPErr Execute(void *parameters) {
	// try/catch this also because there is a layer between you and the 
	// SuspendHistory() call. In Photoshop 5.5 the error return value is not
	// passed back to SuspendHistory() show use a global value instead.
	try {
		GeometryCorrectionData *autoData = static_cast<GeometryCorrectionData*> (parameters);
		check(autoData != NULL);
		autoData->Execute();
	} catch (SPErr thrownError)	{
		logDebug("Poimal error4 %d\n", thrownError);
		gError = thrownError;
	} catch (...)	{
		logDebug("Poimal error5 %d\n", gError);
		gError = kSPLogicError;
	}

	return gError;
}

//-------------------------------------------------------------------------------
//
//	Execute
//	
//	A helper routine. Get back inside the class and Execute from inside there.
//
//-------------------------------------------------------------------------------
void GeometryCorrectionData::Execute(void) {
	logDebug("execute\n");

	// width � height ���������� �� �������, ��� ��� ����������� ������ �������� �����.
	bool isCancel;

	Auto_Desc result;

	int32 widthNew = 0;
	int32 heightNew = 0;
	int32 width = 0;
	int32 height = 0;
	PlayeventDrawDialogFilter(
		result,
		isCancel, 
		width, height,
		widthNew, heightNew
	);

	if (isCancel) {
		logDebug("Cancel\n");
		return;
	}

	logDebug("new sizes %d %d\n", widthNew, heightNew);

	check(widthNew * heightNew <= MAX_IMAGE_SIZE);

	double tt1 = clock();
	PlayeventCanvasSize(std::max((int)widthNew, (int)width), std::max((int)heightNew, (int)height));
	double tt2 = clock();
	logDebug("Time canvas size %.5lf\n", tt2 - tt1);

	PlayeventFilterGeometryCorrectionFilter(result);
	double tt3 = clock();
	logDebug("Time GeometryCorrectionFilter %.5lf\n", tt3 - tt2);

	if (widthNew < width || heightNew < height) {
		// after resize
		PlayeventCanvasSize(widthNew, heightNew);
		double tt4 = clock();
		logDebug("Time canvas size2 %.5lf\n", tt4 - tt3);
	}
}
