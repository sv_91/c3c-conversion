#include "FilterBigDocument.h"

VPoint GetImageSize(void)
{
	VPoint returnPoint = { 0, 0};

	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		returnPoint.h = param->bigDocumentData->imageSize32.h;
		returnPoint.v = param->bigDocumentData->imageSize32.v;
	}
	else
	{
		returnPoint.h = param->imageSize.h;
		returnPoint.v = param->imageSize.v;
	}

	return returnPoint;
}

VRect GetFilterRect(void)
{
	VRect returnRect = { 0, 0, 0, 0};
	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		returnRect.right = param->bigDocumentData->filterRect32.right;
		returnRect.top = param->bigDocumentData->filterRect32.top;
		returnRect.left = param->bigDocumentData->filterRect32.left;
		returnRect.bottom = param->bigDocumentData->filterRect32.bottom;
	}
	else
	{
		returnRect.right = param->filterRect.right;
		returnRect.top = param->filterRect.top;
		returnRect.left = param->filterRect.left;
		returnRect.bottom = param->filterRect.bottom;
	}
	
	return returnRect;
}

VRect GetInRect(void)
{
	VRect returnRect = { 0, 0, 0, 0};
	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		returnRect.right = param->bigDocumentData->inRect32.right;
		returnRect.top = param->bigDocumentData->inRect32.top;
		returnRect.left = param->bigDocumentData->inRect32.left;
		returnRect.bottom = param->bigDocumentData->inRect32.bottom;
	}
	else
	{
		returnRect.right = param->inRect.right;
		returnRect.top = param->inRect.top;
		returnRect.left = param->inRect.left;
		returnRect.bottom = param->inRect.bottom;
	}
	
	return returnRect;
}

VRect GetOutRect(void)
{
	VRect returnRect = { 0, 0, 0, 0};
	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		returnRect.right = param->bigDocumentData->outRect32.right;
		returnRect.top = param->bigDocumentData->outRect32.top;
		returnRect.left = param->bigDocumentData->outRect32.left;
		returnRect.bottom = param->bigDocumentData->outRect32.bottom;
	}
	else
	{
		returnRect.right = param->outRect.right;
		returnRect.top = param->outRect.top;
		returnRect.left = param->outRect.left;
		returnRect.bottom = param->outRect.bottom;
	}
	
	return returnRect;
}

VRect GetMaskRect(void)
{
	VRect returnRect = { 0, 0, 0, 0};
	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		returnRect.right = param->bigDocumentData->maskRect32.right;
		returnRect.top = param->bigDocumentData->maskRect32.top;
		returnRect.left = param->bigDocumentData->maskRect32.left;
		returnRect.bottom = param->bigDocumentData->maskRect32.bottom;
	}
	else
	{
		returnRect.right = param->maskRect.right;
		returnRect.top = param->maskRect.top;
		returnRect.left = param->maskRect.left;
		returnRect.bottom = param->maskRect.bottom;
	}
	
	return returnRect;
}

VPoint GetFloatCoord(void)
{
	VPoint returnPoint = { 0, 0};

	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		returnPoint.h = param->bigDocumentData->floatCoord32.h;
		returnPoint.v = param->bigDocumentData->floatCoord32.v;
	}
	else
	{
		returnPoint.h = param->floatCoord.h;
		returnPoint.v = param->floatCoord.v;
	}

	return returnPoint;
}

VPoint GetWholeSize(void)
{
	VPoint returnPoint = { 0, 0};

	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		returnPoint.h = param->bigDocumentData->wholeSize32.h;
		returnPoint.v = param->bigDocumentData->wholeSize32.v;
	}
	else
	{
		returnPoint.h = param->wholeSize.h;
		returnPoint.v = param->wholeSize.v;
	}

	return returnPoint;
}

void SetInRect(VRect inRect)
{
	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		param->bigDocumentData->inRect32.right = inRect.right;
		param->bigDocumentData->inRect32.top = inRect.top;
		param->bigDocumentData->inRect32.left = inRect.left;
		param->bigDocumentData->inRect32.bottom = inRect.bottom;
	}
	else
	{
		param->inRect.right = (int16)inRect.right;
		param->inRect.top = (int16)inRect.top;
		param->inRect.left = (int16)inRect.left;
		param->inRect.bottom = (int16)inRect.bottom;
	}
}

void SetOutRect(VRect inRect)
{
	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		param->bigDocumentData->outRect32.right = inRect.right;
		param->bigDocumentData->outRect32.top = inRect.top;
		param->bigDocumentData->outRect32.left = inRect.left;
		param->bigDocumentData->outRect32.bottom = inRect.bottom;
	}
	else
	{
		param->outRect.right = (int16)inRect.right;
		param->outRect.top = (int16)inRect.top;
		param->outRect.left = (int16)inRect.left;
		param->outRect.bottom = (int16)inRect.bottom;
	}
}

void SetMaskRect(VRect inRect)
{
	if (param->bigDocumentData != NULL && 
		param->bigDocumentData->PluginUsing32BitCoordinates)
	{
		param->bigDocumentData->maskRect32.right = inRect.right;
		param->bigDocumentData->maskRect32.top = inRect.top;
		param->bigDocumentData->maskRect32.left = inRect.left;
		param->bigDocumentData->maskRect32.bottom = inRect.bottom;
	}
	else
	{
		param->maskRect.right = (int16)inRect.right;
		param->maskRect.top = (int16)inRect.top;
		param->maskRect.left = (int16)inRect.left;
		param->maskRect.bottom = (int16)inRect.bottom;
	}
}
// end FilterBigDocument.cpp
