#include "commonFilter.h"

#include <stdarg.h>

#include <windows.h>

bool dirExists(const std::string& dirName_in) {
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES) {
		return false;  //something is wrong with your path!
	}

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY) {
		return true;   // this is a directory!
	}

	return false;    // this is not a directory!
}

static FILE *f;

#ifndef LOG_DEBUG_NO
void createFile(const std::string &folder, const std::string &file) {
	if (dirExists(folder)) {
		f = fopen((folder + file).c_str(), "a");
	} else {
		if (dirExists(folder + "/")) {
			f = fopen((folder + "/" + file).c_str(), "a");
		} else {
			f = NULL;
		}
	}
}
#else
void createFile(const std::string &folder, const std::string &file) {
}
#endif

#ifndef LOG_DEBUG_NO
void logDebug(const char *format, ...) {
	if (f != NULL) {
		va_list vl;
		va_start(vl, format);
		vfprintf(f, format, vl);
		va_end(vl);
		fflush(f);
	}
}
#else 
void logDebug(const char *format, ...) {}
#endif
