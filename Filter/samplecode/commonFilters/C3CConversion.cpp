#include "C3CConversion.h"
#include "global.h"
//#include "..\DataUnit.h"



ColorServicesInfo RGB2Lab_info;

Color8 src, dst;

void InitConversion(void) 
{
   RGB2Lab_info.infoSize = sizeof(RGB2Lab_info);
   RGB2Lab_info.selector = plugIncolorServicesConvertColor;
   RGB2Lab_info.reservedSourceSpaceInfo = nil;
   RGB2Lab_info.reservedResultSpaceInfo = nil;
   RGB2Lab_info.colorComponents[3] = 0;
   RGB2Lab_info.resultGamutInfoValid = true;
   RGB2Lab_info.reserved = nil;
   RGB2Lab_info.sourceSpace = plugIncolorServicesRGBSpace;
   RGB2Lab_info.resultSpace = plugIncolorServicesLabSpace;
   
   src[0] = 0;
}

void GetLab(int16 red, int16 green, int16 blue, int16 &L, int16 &a, int16&b)
{
	RGB2Lab_info.colorComponents[0] = red;
	RGB2Lab_info.colorComponents[1] = green;
	RGB2Lab_info.colorComponents[2] = blue;

	param->colorServices(&RGB2Lab_info);

	L = RGB2Lab_info.colorComponents[0];
	a = RGB2Lab_info.colorComponents[1];
	b = RGB2Lab_info.colorComponents[2];
}

void GetMonitorRGB(uint8 r, uint8 g, uint8 b, uint8 &red, uint8 &green, uint8 &blue)
{
	src[1] = r;
	src[2] = g;
	src[3] = b;
	ColorSpaceSuite->ConvertToMonitorRGB(plugIncolorServicesRGBSpace, &src, &dst, 1);
	red = dst[1];
	green = dst[2];
	blue = dst[3];
}

bool testMonitorRGB(void)
{
   uint8 r, g, b;

   GetMonitorRGB(255, 255, 255, r, g, b);

   return (r > 127 && g > 127 && b > 127);

}

