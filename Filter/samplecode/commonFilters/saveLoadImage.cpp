#include "SaveLoadImage.h"

Image loadImage(FilterRecord *param, int width, int height, float(*funcLinearize)(float)) {
	logDebug("sizes %d %d\n", width, height);

	// TODO ���������, ��� �������� ������ ����������� �� ������ ����������
	const long long sizeImageG = width * height;
	const int imageWidth = param->inRowBytes;

	const int mode = param->imageMode;
	int MAX_VALUE;
	if (mode == plugInModeRGBColor || mode == plugInModeGrayScale) {
		MAX_VALUE = 256 - 1;
	} else if (mode == plugInModeRGB48 || mode == plugInModeGray16) {
		MAX_VALUE = 256 * 128; // Photoshop
	} else {
		check(false);
	}

	loadPtr sRGB2Lin = colorConversionInitLoad(MAX_VALUE, funcLinearize);

	Image image(width, height);
	if (mode == plugInModeRGBColor) {
		check(param->planes == 3);
		uint8 *inData = reinterpret_cast<uint8*>(param->inData);

		std::string ompErr = "";
#pragma omp parallel for
		for (int i = 0; i < width; i++){
			try {
				for (int j = 0; j < height; j++){
					image.setColors(
						i, j,
						sRGB2Lin(inData[0 + i * 3 + j * imageWidth]),
						sRGB2Lin(inData[1 + i * 3 + j * imageWidth]),
						sRGB2Lin(inData[2 + i * 3 + j * imageWidth])
						);
				}
			} catch (std::string &e) {
				ompErr = e;
			} catch (...) {
				ompErr = "UnknownError";
			}
		}

		if (ompErr != "") {
			throw ompErr;
		}
	} else if (mode == plugInModeGrayScale) {
		check(param->planes == 1);
		uint8 *inData = reinterpret_cast<uint8*>(param->inData);

		std::string ompErr = "";
#pragma omp parallel for
		for (int i = 0; i < width; i++){
			try {
				for (int j = 0; j < height; j++){
					image.setColors(
						i, j,
						sRGB2Lin(inData[i + j * imageWidth]),
						sRGB2Lin(inData[i + j * imageWidth]),
						sRGB2Lin(inData[i + j * imageWidth])
						);
				}
			} catch (std::string &e) {
				ompErr = e;
			} catch (...) {
				ompErr = "UnknownError";
			}
		}

		if (ompErr != "") {
			throw ompErr;
		}
	} else if (mode == plugInModeRGB48) {
		check(param->planes == 3);
		uint16 *inData = reinterpret_cast<uint16*>(param->inData);

		std::string ompErr = "";
#pragma omp parallel for
		for (int i = 0; i < width; i++){
			try {
				for (int j = 0; j < height; j++){
					image.setColors(
						i, j,
						sRGB2Lin(inData[0 + i * 3 + j * imageWidth / 2]),
						sRGB2Lin(inData[1 + i * 3 + j * imageWidth / 2]),
						sRGB2Lin(inData[2 + i * 3 + j * imageWidth / 2])
						);
				}
			} catch (std::string &e) {
				ompErr = e;
			} catch (...) {
				ompErr = "UnknownError";
			}
		}

		if (ompErr != "") {
			throw ompErr;
		}
	} else if (mode == plugInModeGray16) {
		check(param->planes == 1);
		uint16 *inData = reinterpret_cast<uint16*>(param->inData);

		std::string ompErr = "";
#pragma omp parallel for
		for (int i = 0; i < width; i++){
			try {
				for (int j = 0; j < height; j++){
					image.setColors(
						i, j,
						sRGB2Lin(inData[i + j * imageWidth / 2]),
						sRGB2Lin(inData[i + j * imageWidth / 2]),
						sRGB2Lin(inData[i + j * imageWidth / 2])
						);
				}
			} catch (std::string &e) {
				ompErr = e;
			} catch (...) {
				ompErr = "UnknownError";
			}
		}

		if (ompErr != "") {
			throw ompErr;
		}
	} else {
		check(false);
	}

	return image;
}

void saveImage(const Image &image, int width, int height, int mode, void* outDataP, int outRowBytes, float(*funcLinearize)(float)) {
	int MAX_VALUE;
	if (mode == plugInModeRGBColor || mode == plugInModeGrayScale) {
		MAX_VALUE = 256 - 1;
	} else if (mode == plugInModeRGB48 || mode == plugInModeGray16) {
		MAX_VALUE = 256 * 128; // Photoshop
	} else {
		check(false);
	}

	savePtr Lin2sRGBSave = colorConversionInitSave(MAX_VALUE, funcLinearize);

	if (mode == plugInModeRGBColor) {
		uint8 *outData = reinterpret_cast<uint8*>(outDataP);

		std::string ompErr = "";
#pragma omp parallel for
		for (int i = 0; i < width; i++){
			try {
				for (int j = 0; j < height; j++){
					outData[0 + i * 3 + j * outRowBytes] = (uint8)Lin2sRGBSave(image.getRed(i, j));
					outData[1 + i * 3 + j * outRowBytes] = (uint8)Lin2sRGBSave(image.getGreen(i, j));
					outData[2 + i * 3 + j * outRowBytes] = (uint8)Lin2sRGBSave(image.getBlue(i, j));
				}
			} catch (std::string &e) {
				ompErr = e;
			} catch (...) {
				ompErr = "UnknownError";
			}
		}

		if (ompErr != "") {
			throw ompErr;
		}
	} else if (mode == plugInModeGrayScale) {
		uint8 *outData = reinterpret_cast<uint8*>(outDataP);

		std::string ompErr = "";
#pragma omp parallel for
		for (int i = 0; i < width; i++){
			try {
				for (int j = 0; j < height; j++){
					outData[i + j * outRowBytes] = Lin2sRGBSave(image.getRed(i, j));
				}
			} catch (std::string &e) {
				ompErr = e;
			} catch (...) {
				ompErr = "UnknownError";
			}
		}

		if (ompErr != "") {
			throw ompErr;
		}
	} else if (mode == plugInModeRGB48) {
		uint16 *outData = reinterpret_cast<uint16*>(outDataP);

		std::string ompErr = "";
#pragma omp parallel for
		for (int i = 0; i < width; i++){
			try {
				for (int j = 0; j < height; j++){
					outData[0 + i * 3 + j * outRowBytes / 2] = Lin2sRGBSave(image.getRed(i, j));
					outData[1 + i * 3 + j * outRowBytes / 2] = Lin2sRGBSave(image.getGreen(i, j));
					outData[2 + i * 3 + j * outRowBytes / 2] = Lin2sRGBSave(image.getBlue(i, j));
				}
			} catch (std::string &e) {
				ompErr = e;
			} catch (...) {
				ompErr = "UnknownError";
			}
		}

		if (ompErr != "") {
			throw ompErr;
		}
	} else if (mode == plugInModeGray16) {
		uint16 *outData = reinterpret_cast<uint16*>(outDataP);

		std::string ompErr = "";
#pragma omp parallel for
		for (int i = 0; i < width; i++){
			try {
				for (int j = 0; j < height; j++){
					outData[i + j * outRowBytes / 2] = Lin2sRGBSave(image.getRed(i, j));
				}
			} catch (std::string &e) {
				ompErr = e;
			} catch (...) {
				ompErr = "UnknownError";
			}
		}

		if (ompErr != "") {
			throw ompErr;
		}
	} else {
		check(false);
	}
}
