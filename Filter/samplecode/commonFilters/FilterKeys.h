#ifndef FILTER_KEYS_H_
#define FILTER_KEYS_H_

#include <string>

#define keyWidthImage         'wdth'
#define keyHeightImage        'hght'
#define keyWidthNew         'wdtN'
#define keyHeightNew        'hghN'
#define keyAlgorithmNumber 'algN'
#define keyAlgorithmTransformNumber 'alTN'
#define keyUnsharpCoeff   'unsh'
#define keyNotLinearizing 'nLin'
#define keyXMin 'xmin'
#define keyYMin 'ymin'
#define keyIsCancel 'canc'
#define keyZAngle 'zAng'
#define keyXAngle 'xAng'
#define keyYAngle 'yAng'
#define keyFocalLength 'zCon'

#endif // FILTER_KEYS_H_
