#ifndef _C3CConversion_H
#define _C3CConversion_H

#include "PITypes.h"

void InitConversion(void);
void GetLab(int16 red, int16 green, int16 blue, int16 &L, int16 &a, int16&b);
void GetMonitorRGB(uint8 r, uint8 g, uint8 b, uint8 &red, uint8 &green, uint8 &blue);
bool testMonitorRGB(void);

#endif