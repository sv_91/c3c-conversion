#ifndef COMMON_FILTER_H_
#define COMMON_FILTER_H_

#include <cstdio>
#include "SPTypes.h"
#include <string>

#include <PIFilter.h>

#undef min
#undef max

#include "common.h"

extern SPErr gError;

#define checkSPErr(v) { \
if (v != kSPNoError) { \
	char buf[256]; \
	sprintf_s(buf, "Error filter %s %d %d\n", __FILE__, __LINE__, v); \
	logDebug("%s", buf); \
	gError = v; \
	throw gError; \
} \
}

#endif // COMMON_FILTER_H_
