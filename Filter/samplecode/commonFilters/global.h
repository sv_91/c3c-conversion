#ifndef GLOBAL_H
#define GLOBAL_H

#include "PITypes.h"
#include "PIFilter.h"
#include "PIColorSpaceSuite.h"

//#include "C3CImageSizeScripting.h"

#define ORGANIZATION "Egor&Gleb Software"

#define MAXSIZE 32768

#define   MAX8 255
#define   MAX16 32768
#define   SIZE8 (MAX8+1)
#define   SIZE16 (MAX16+1)

struct tRGB16 {
   uint16 R, G, B;
};

struct tRGB {
   uint8 R, G, B;
};

struct tRGBfloat {
   float R, G, B/*, X*/;
};

//extern int16 resultCode;
extern FilterRecordPtr param;
extern PSColorSpaceSuite1* ColorSpaceSuite;

extern uint8 *inData, *outData;
extern uint16 *inData16, *outData16;
extern struct tRGB *inDataRGB, *outDataRGB;
extern struct tRGB16 *inDataRGB16, *outDataRGB16;

//extern C3CResizeParameters pluginParams;

/*
extern int glWidth, glHeight;
extern int glPlanes;
extern int glSize;
extern int glMode;
extern int glSpace;
*/

extern int newWidth, newHeight;
extern int glMethod;
extern int glSharpness;

extern int glMAX;

extern float sRGB2LinCurveP[SIZE8];
extern int Lin2sRGBCurveP[SIZE16];

extern float sRGB2LinCurve[SIZE16];
extern int Lin2sRGBCurve[SIZE16];

extern float RowFactor[MAXSIZE];
extern float LineFactor[MAXSIZE];

extern float ScaleCurve[SIZE16];

extern int C1628[SIZE16];
extern int C8216[SIZE8];

void InitData(FilterRecordPtr param);

#endif
