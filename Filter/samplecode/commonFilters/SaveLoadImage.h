#ifndef SAVE_LOAD_IMAGE_H
#define SAVE_LOAD_IMAGE_H

#include <PIFilter.h>

#undef min
#undef max

#include "common.h"
#include "Image.h"

Image loadImage(FilterRecord *param, int width, int height, float(*funcLinearize)(float));
void saveImage(const Image &image, int width, int height, int mode, void* outDataP, int outRowBytes, float(*funcLinearize)(float));

#endif // SAVE_LOAD_IMAGE_H
