#ifndef _GeometryCorrectionFilterSCRIPTING_H
#define _GeometryCorrectionFilterSCRIPTING_H

#include "PIDefines.h"
#include "PIActions.h"
#include "PITerminology.h"
#include "GeometryCorrectionFilterUniqueString.h"

#ifndef Rez
#include "GeometryCorrectionFilter.h"
#endif

#define vendorName			"AdobeSDK"
#define plugInName			"GeometryCorrectionFilter"
#define plugInAETEComment	"GeometryCorrectionFilter filter plug-in"
#define plugInSuiteID		'sdk1'
#define plugInClassID		plugInSuiteID
#define plugInEventID		plugInClassID

#define plugInDescription \
	"An example plug-in filter module for Adobe Photoshop�."

#define keyDisposition 		'disP'
#define keyIgnoreSelection	'ignS'
#define typeMood			'mooD'
#define dispositionClear	'moD0'
#define dispositionCool		'moD1'
#define dispositionHot		'moD2'
#define dispositionSick		'moD3'

#ifndef Rez
OSErr ReadScriptParameters(Boolean* displayDialog);
OSErr WriteScriptParameters(void);
int16 ScriptToDialog(int32 script);
int32 DialogToScript(int16 dialog);
#endif
#endif