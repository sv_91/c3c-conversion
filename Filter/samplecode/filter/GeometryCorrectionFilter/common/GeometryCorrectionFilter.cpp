#include "GeometryCorrectionFilter.h"
#include "FilterBigDocument.h"
#include "PIColorSpaceSuite.h"

#include <time.h>
#include <string>

#undef min
#undef max

#include "Algorithms.h"
#include "C3CConversion.h"
#include "global.h"
#include "colorConversion.h"
#include "Image.h"

#include "commonFilter.h"
#include "SaveLoadImage.h"

#include "version.h"

FilterRecord * param = NULL;
SPBasicSuite * sSPBasic = NULL;

SPErr gError = 0;

float(*funcLinearize1)(float);
float(*funcLinearize2)(float);

PSColorSpaceSuite1* ColorSpaceSuite = NULL;

void DoPrepare(void);
void DoStart(void);
void DoContinue(void);
void DoFinish(void);

void setErrorString(const std::string &error, FilterRecordPtr &filterRecord) {
	char *pErrorString = (char*)filterRecord->errorString;
	if (pErrorString != NULL && error.size() < 256) {
		*pErrorString = (char)error.size();
		memcpy(pErrorString + 1, error.data(), (unsigned char)(*pErrorString));
	}
}

//-------------------------------------------------------------------------------
//
//	PluginMain
//	
//	All calls to the plug in module come through this routine.
//
//	Inputs:
//		const int16 selector		Host provides selector indicating what
//									command to do.
//
//	Inputs and Outputs:
//		FilterRecord *filterRecord	Host provides a pointer to parameter block
//									containing pertinent data and callbacks.
//									See PIFilter.h
//
//		intptr_t *data				Use this to store a handle or pointer to our global
//									data structure, which is maintained by the
//									host between calls to the plug in.
//
//	Outputs:
//		int16 *result				Returns error result. Some errors are handled
//									by the host, some are silent, and some you
//									must handle. See PIGeneral.h.
//
//-------------------------------------------------------------------------------
DLLExport MACPASCAL void PluginMain(const int16 selector,
								    FilterRecordPtr filterRecord,
								    intptr_t * data,
								    int16 * result)
{
	createFile("C:/� ����� D/����������/1 �������/������/", "outGeometryCorrectionFilter.txt");
	logDebug("version %s\n", VERSION);

	// update our global parameters
	param = filterRecord;

	try {
		if (selector == filterSelectorAbout) {
			sSPBasic = ((AboutRecord*)param)->sSPBasic;
		} else {
			sSPBasic = param->sSPBasic;

			if (param->bigDocumentData != NULL) {
				param->bigDocumentData->PluginUsing32BitCoordinates = true;
			}
		}

		// do the command according to the selector
		switch (selector) {
		case filterSelectorAbout:
			logDebug("filterSelectorAbout\n");
			break;
		case filterSelectorParameters:
			logDebug("filterSelectorParameters\n");
			break;
		case filterSelectorPrepare:
			logDebug("filterSelectorPrepare\n");
			DoPrepare();
			break;
		case filterSelectorStart:
			logDebug("filterSelectorStart\n");
			DoStart();
			break;
		case filterSelectorContinue:
			logDebug("filterSelectorContinue\n");
			DoContinue();
			break;
		case filterSelectorFinish:
			logDebug("filterSelectorFinish\n");
			DoFinish();
			break;
		default:
			break;
		}
	} catch (std::string err) {
		logDebug("error %s\n", err.c_str());
		gError = errReportString;
		setErrorString(err, filterRecord);
	} catch (SPErr err) {
		gError = errReportString;
		setErrorString("SPError", filterRecord);
	} catch (...) {
		gError = errReportString;
		setErrorString("Unknown error", filterRecord);
	}

	*result = gError;

	logDebug("End filter %d\n", gError);
}

//-------------------------------------------------------------------------------
//
// DoPrepare
//
// Almost identical to DoParameters. Make sure we have valid Data and Parameters
// handle(s) and lock and initialize as necessary. Sets the bufferSpace and 
// maxSpace variables in the gFilterRecord so memory is used at an optimum.
//
// NOTE:
// The fields in the gFilterRecord are not all valid at this stage. We will take a
// guess at the actual tile size information.
// 
//-------------------------------------------------------------------------------
void DoPrepare(void) {
	check(
		param->imageMode == plugInModeRGBColor ||
		param->imageMode == plugInModeRGB48 ||
		param->imageMode == plugInModeGrayScale ||
		param->imageMode == plugInModeGray16
	);

	check(param->planes == 1 || param->planes == 3);

	const void* suite;

	// �������� ColorSpaceSuite
	checkSPErr(param->sSPBasic->AcquireSuite(kPSColorSpaceSuite, kPSColorSpaceSuiteVersion1, &suite));
	ColorSpaceSuite = (PSColorSpaceSuite1*)suite;

	int16 rL, ra, rb;
	int16 gL, ga, gb;
	int16 bL, ba, bb;

	InitConversion();

	GetLab(255, 0, 0, rL, ra, rb);
	GetLab(0, 255, 0, gL, ga, gb);
	GetLab(0, 0, 255, bL, ba, bb);

	if (rL == 160 && ra == 218 && rb == 206 &&
		gL == 212 && ga == 0 && gb == 215 &&
		bL == 77 && ba == 197 && bb == 14
	) {
		funcLinearize1 = adobeRGB2Lin;
		funcLinearize2 = lin2sRGBAdobeRGB;
	} else if (rL == 138 && ra == 209 && rb == 198 &&
		gL == 224 && ga == 49 && gb == 209 &&
		bL == 75 && ba == 196 && bb == 16
	) {
		funcLinearize1 = sRGB2Lin;
		funcLinearize2 = lin2sRGB;
	} else {
		funcLinearize1 = lin2Lin;
		funcLinearize2 = lin2Lin;
	}

	param->bufferSpace = 0;
	param->maxSpace = 0;

	logDebug("DoPrepare: ok\n");
}

inline Image transform(const Image &image1, const LinearConversion &sc, const std::string &algorithmNumber) {
	if (algorithmNumber == monteKarloStr) {
		return monteKarlo::transform(image1, sc);
	} else if (algorithmNumber == SazerlHodjmenStr) {
		return SH::transform(image1, sc);
	} else if (algorithmNumber == bilinearStr) {
		return bilinearInterpolation::transform(image1, sc);
	} else if (algorithmNumber == bicubickStr) {
		return bicubicInterpolation::transform(image1, sc);
	} else if (algorithmNumber == sincStr) {
		return sinc::transform(image1, sc);
	} else {
		check(false);
	}
}

Image test(
	Image &image, 
	float zAngle, float xAngle, float yAngle, 
	float focalLengthInPixels, 
	const std::string &algorithmNumber, float unsharpFactor, bool notLinearizing,
	const std::string &algorithmTransformeNumber
) {
	const LinearConversion transformConversion = LinearConversion::make(
		zAngle, xAngle, yAngle, 
		focalLengthInPixels,
		image.xMin, image.yMin,
		image.width, image.height,
		algorithmTransformeNumber
	);

	double tt1 = clock();
	Image imageOut = transform(image, transformConversion, algorithmNumber);
	double tt2 = clock();
	logDebug("Time filter %lf\n", tt2 - tt1);

	if (unsharpFactor != 0.f) {
		unsharpMask(imageOut.width, imageOut.height, imageOut.r, unsharpFactor);
		unsharpMask(imageOut.width, imageOut.height, imageOut.g, unsharpFactor);
		unsharpMask(imageOut.width, imageOut.height, imageOut.b, unsharpFactor);
	}

	return imageOut;
}

void solve() {
	double tt1 = clock();

	PIDescriptorHandle descHandle = param->descriptorParameters->descriptor;
	Auto_Desc actionDescriptor;

	checkSPErr(sPSActionDescriptor->HandleToDescriptor(descHandle, &actionDescriptor));

	double xMin = 0;
	checkSPErr(sPSActionDescriptor->GetFloat(actionDescriptor.get(), keyXMin, &xMin));
	double yMin = 0;
	checkSPErr(sPSActionDescriptor->GetFloat(actionDescriptor.get(), keyYMin, &yMin));

	int32 width = 0;
	checkSPErr(sPSActionDescriptor->GetInteger(actionDescriptor.get(), keyWidthImage, &width));
	int32 height = 0;
	checkSPErr(sPSActionDescriptor->GetInteger(actionDescriptor.get(), keyHeightImage, &height));

	double zAngle = 0;
	checkSPErr(sPSActionDescriptor->GetFloat(actionDescriptor.get(), keyZAngle, &zAngle));
	double xAngle = 0;
	checkSPErr(sPSActionDescriptor->GetFloat(actionDescriptor.get(), keyXAngle, &xAngle));
	double yAngle = 0;
	checkSPErr(sPSActionDescriptor->GetFloat(actionDescriptor.get(), keyYAngle, &yAngle));

	double focalLengthInPixels = 0;
	checkSPErr(sPSActionDescriptor->GetFloat(actionDescriptor.get(), keyFocalLength, &focalLengthInPixels));

	char tmpAlgNumber[100];
	checkSPErr(sPSActionDescriptor->GetString(actionDescriptor.get(), keyAlgorithmNumber, tmpAlgNumber, 100));
	logDebug("Algorithm name %s\n", tmpAlgNumber);
	std::string algorithmNumber(tmpAlgNumber);

	char tmpAlgNumber2[100];
	checkSPErr(sPSActionDescriptor->GetString(actionDescriptor.get(), keyAlgorithmTransformNumber, tmpAlgNumber2, 100));
	logDebug("Algorithm transforme name %s\n", tmpAlgNumber2);
	std::string algorithmTransformeNumber = std::string(tmpAlgNumber2);

	double unsharpFactorTmp = 0.f;
	checkSPErr(sPSActionDescriptor->GetFloat(actionDescriptor.get(), keyUnsharpCoeff, &unsharpFactorTmp));
	float unsharpFactor = static_cast<float>(unsharpFactorTmp);

	Boolean nLin = false;
	checkSPErr(sPSActionDescriptor->GetBoolean(actionDescriptor.get(), keyNotLinearizing, &nLin));
	bool notLinearizing = nLin != 0;

	logDebug("Params width, height %d %d %lf %lf\n", width, height, xMin, yMin);

	double tt2 = clock();
	logDebug("Time parse params %lf\n", tt2 - tt1);

	float(*funcLinearize11)(float) = funcLinearize1;
	float(*funcLinearize22)(float) = funcLinearize2;
	if (notLinearizing) {
		funcLinearize11 = lin2Lin;
		funcLinearize22 = lin2Lin;
	}

	Image image = loadImage(param, width, height, funcLinearize11);
	image.xMin = static_cast<float>(xMin);
	image.yMin = static_cast<float>(yMin);

	double tt3 = clock();
	logDebug("Time load image %lf\n", tt3 - tt2);

	// ��������������
	image = test(
		image,
		static_cast<float>(zAngle),
		static_cast<float>(xAngle),
		static_cast<float>(yAngle),
		static_cast<float>(focalLengthInPixels),
		algorithmNumber,
		unsharpFactor,
		notLinearizing,
		algorithmTransformeNumber
	);

	// ���������� �����������.
	double tt4 = clock();
	{
		const int widthOut = std::min((int)param->imageSize.h, image.width);
		const int heightOut = std::min((int)param->imageSize.v, image.height);
		const int mode = param->imageMode;
		saveImage(image, widthOut, heightOut, mode, param->outData, param->outRowBytes, funcLinearize22);
	}
	double tt5 = clock();
	logDebug("Time save image %lf\n", tt5 - tt4);
}

//-------------------------------------------------------------------------------
//
// DoStart
//
// The main filtering routine for this plug in. See if we have any registry
// parameters from the last time we ran. Determine if the UI needs to be
// displayed by reading the script parameters. Save the last dialog parameters
// in case something goes wrong or the user cancels.
//
//-------------------------------------------------------------------------------
void DoStart(void) {
	const int maxThreads = omp_get_max_threads();
	logDebug("Max threads %d\n", maxThreads);
	omp_set_num_threads(maxThreads);
	omp_set_dynamic(maxThreads); // ��� ����������� ���������� ������� � ��������.

	VRect rect;
	rect.top = 0;
	rect.bottom = param->imageSize.v;
	rect.left = 0;
	rect.right = param->imageSize.h;
	SetInRect(rect);
	SetOutRect(rect);
	param->inLoPlane = 0;
	param->inHiPlane = param->planes - 1;
	param->outLoPlane = 0;
	param->outHiPlane = param->planes - 1;

	checkSPErr(param->advanceState());

	if (param->imageMode != plugInModeRGBColor &&
		param->imageMode != plugInModeRGB48 &&
		param->imageMode != plugInModeGrayScale &&
		param->imageMode != plugInModeGray16) 
	{
		check(false);
	} else {
		solve();
	}

	rect.top = 0;
	rect.bottom = 0;
	rect.left = 0;
	rect.right = 0;
	SetInRect(rect);
	SetOutRect(rect);
	param->outRect = param->inRect;

	logDebug("DoStart ok\n");
}



//-------------------------------------------------------------------------------
//
// DoContinue
//
// If we get here we probably did something wrong. This selector was needed
// before advanceState() was in the FilterRecord*. Now that we use advanceState()
// there is nothing for us to do but set all the rectangles to 0 and return.
//
//-------------------------------------------------------------------------------
void DoContinue(void) {
	VRect zeroRect = { 0, 0, 0, 0 };

	SetInRect(zeroRect);
	SetOutRect(zeroRect);
	SetMaskRect(zeroRect);
}



//-------------------------------------------------------------------------------
//
// DoFinish
//
// Everything went as planned and the pixels have been modified. Now record
// scripting parameters and put our information in the Photoshop Registry for the
// next time we get called. The Registry saves us from keeping a preferences file.
//
//-------------------------------------------------------------------------------
void DoFinish(void) {
}
