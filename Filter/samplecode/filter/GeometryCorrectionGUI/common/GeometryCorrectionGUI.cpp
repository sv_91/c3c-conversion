#include <Stdafx.h>
#include "MyDialog2.h"

#include "FilterBigDocument.h"
#include "PITerminology.h"
#include <PropertyUtils.h>
#include <PIProperties.h>

#include <exif.hpp>

#include <time.h>

#undef min
#undef max

#include <string>

#include "commonFilter.h"

#include "version.h"

FilterRecord * param = NULL;
SPBasicSuite * sSPBasic = NULL;

SPErr gError = 0;

void DoPrepare(void);
void DoStart(void);
void DoContinue(void);
void DoFinish(void);

void setErrorString(const std::string &error, FilterRecordPtr &filterRecord) {
	char *pErrorString = (char*)filterRecord->errorString;
	if (pErrorString != NULL && error.size() < 256) {
		*pErrorString = (char)error.size();
		memcpy(pErrorString + 1, error.data(), (unsigned char)(*pErrorString));
	}
}

//-------------------------------------------------------------------------------
//
//	PluginMain
//	
//	All calls to the plug in module come through this routine.
//
//	Inputs:
//		const int16 selector		Host provides selector indicating what
//									command to do.
//
//	Inputs and Outputs:
//		FilterRecord *filterRecord	Host provides a pointer to parameter block
//									containing pertinent data and callbacks.
//									See PIFilter.h
//
//		intptr_t *data				Use this to store a handle or pointer to our global
//									data structure, which is maintained by the
//									host between calls to the plug in.
//
//	Outputs:
//		int16 *result				Returns error result. Some errors are handled
//									by the host, some are silent, and some you
//									must handle. See PIGeneral.h.
//
//-------------------------------------------------------------------------------
DLLExport MACPASCAL void PluginMain(const int16 selector,
								    FilterRecordPtr filterRecord,
								    intptr_t *data,
								    int16 *result)
{
	createFile("C:/� ����� D/����������/1 �������/������/", "outDraw.txt");
	logDebug("version %s\n", VERSION);

	gError = 0;

	param = filterRecord;

	try {
		if (selector == filterSelectorAbout) {
			sSPBasic = ((AboutRecord*)param)->sSPBasic;
		} else {
			sSPBasic = param->sSPBasic;

			if (param->bigDocumentData != NULL) {
				param->bigDocumentData->PluginUsing32BitCoordinates = true;
			}
		}

		// do the command according to the selector
		switch (selector) {
		case filterSelectorAbout:
			logDebug("call filterSelectorAbout\n");
			break;
		case filterSelectorParameters:
			logDebug("call filterSelectorParameters\n");
			break;
		case filterSelectorPrepare:
			logDebug("call filterSelectorPrepare\n");
			DoPrepare();
			break;
		case filterSelectorStart:
			logDebug("call filterSelectorStart\n");
			DoStart();
			break;
		case filterSelectorContinue:
			logDebug("call filterSelectorContinue\n");
			DoContinue();
			break;
		case filterSelectorFinish:
			logDebug("call filterSelectorFinish\n");
			DoFinish();
			break;
		default:
			break;
		}
	} catch (std::string err) {
		logDebug("error %s\n", err.c_str());
		gError = errReportString;
		setErrorString(err, filterRecord);
	} catch (SPErr err) {
		gError = errReportString;
		setErrorString("SPError", filterRecord);
	} catch (...) {
		gError = errReportString;
		setErrorString("Unknown error", filterRecord);
	}

	*result = static_cast<int16>(gError);

	logDebug("End filter %d\n", gError);
}

void DoPrepare(void) {
	check(
		param->imageMode == plugInModeRGBColor ||
		param->imageMode == plugInModeRGB48 ||
		param->imageMode == plugInModeGrayScale ||
		param->imageMode == plugInModeGray16
	);

	check(param->planes == 1 || param->planes == 3);

	logDebug("Ya tuta3\n");
}

//-------------------------------------------------------------------------------
//
// DoStart
//
// The main filtering routine for this plug in. See if we have any registry
// parameters from the last time we ran. Determine if the UI needs to be
// displayed by reading the script parameters. Save the last dialog parameters
// in case something goes wrong or the user cancels.
//
//-------------------------------------------------------------------------------
void DoStart(void) {
	logDebug("DoStart\n");

	const int maxThreads = omp_get_max_threads();
	logDebug("Max threads %d\n", maxThreads);
	omp_set_num_threads(maxThreads);
	omp_set_dynamic(maxThreads); // ��� ����������� ���������� ������� � ��������

	VRect rect;
	rect.top = 0;
	rect.bottom = param->imageSize.v;
	rect.left = 0;
	rect.right = param->imageSize.h;

	SetInRect(rect);
	SetOutRect(rect);

	param->inLoPlane = 0;
	param->inHiPlane = param->planes - 1;
	param->outLoPlane = 0;
	param->outHiPlane = param->planes - 1;

	GeometryCorrectionGUIData afData;
	afData.DoIt(param);

	logDebug("Ya tuta9\n");
}



//-------------------------------------------------------------------------------
//
// DoContinue
//
// If we get here we probably did something wrong. This selector was needed
// before advanceState() was in the FilterRecord*. Now that we use advanceState()
// there is nothing for us to do but set all the rectangles to 0 and return.
//
//-------------------------------------------------------------------------------
void DoContinue(void) {
	VRect zeroRect = { 0, 0, 0, 0 };

	SetInRect(zeroRect);
	SetOutRect(zeroRect);
	SetMaskRect(zeroRect);
}



//-------------------------------------------------------------------------------
//
// DoFinish
//
// Everything went as planned and the pixels have been modified. Now record
// scripting parameters and put our information in the Photoshop Registry for the
// next time we get called. The Registry saves us from keeping a preferences file.
//
//-------------------------------------------------------------------------------
void DoFinish(void) {
}

float getFocusMmInExif() {
	Handle handle;
	checkSPErr(PIGetEXIFData(handle));
	std::string ss;
	if (HandleToString(handle, ss) != kSPNoError) {
		return 0.f;
	}
	logDebug("Exif handle size %d\n", (int)ss.size());

	Exiv2::ExifData exifData;
	Exiv2::ExifParser::decode(exifData, reinterpret_cast<const Exiv2::byte*>(ss.data()), static_cast<uint32_t>(ss.size()));
	for (Exiv2::ExifData::const_iterator i = exifData.begin(); i != exifData.end(); ++i) {
		logDebug("Exif field %s %f\n", i->key().c_str(), i->value().toFloat());
	}

	if (exifData.findKey(Exiv2::ExifKey("Exif.Photo.FocalLengthIn35mmFilm")) != exifData.end()) {
		return exifData["Exif.Photo.FocalLengthIn35mmFilm"].toFloat(0);
	} else {
		const Exiv2::ExifKey xResolutionKey("Exif.Photo.FocalPlaneXResolution");
		const Exiv2::ExifKey yResolutionKey("Exif.Photo.FocalPlaneYResolution");
		const Exiv2::ExifKey resolutionUnitKey("Exif.Photo.FocalPlaneResolutionUnit");
		const Exiv2::ExifKey focalLengthKey("Exif.Photo.FocalLength");

		if (
			exifData.findKey(xResolutionKey) == exifData.end()
			|| exifData.findKey(yResolutionKey) == exifData.end()
			|| exifData.findKey(resolutionUnitKey) == exifData.end()
			|| exifData.findKey(focalLengthKey) == exifData.end()
		) {
			return 0.f;
		} else {
			const float xResolution = exifData[xResolutionKey.key()].toFloat(0);
			const float yResolution = exifData[yResolutionKey.key()].toFloat(0);
			const float resolutionUnit = exifData[resolutionUnitKey.key()].toFloat(0);
			const float focalLength = exifData[focalLengthKey.key()].toFloat(0);

			logDebug("My exif data %f %f %f %f\n", xResolution, yResolution, resolutionUnit, focalLength);

			const int width = param->imageSize.h;
			const int height = param->imageSize.v;

			float toMM;
			if (resolutionUnit == 2.f) {
				toMM = 25.4f;
			} else if (resolutionUnit == 3.f) {
				toMM = 10.f;
			} else if (resolutionUnit == 4.f) {
				toMM = 1.f;
			} else if (resolutionUnit == 5.f) {
				toMM = 1e-3f;
			} else {
				return 0.f;
			}

			const float xMm = width / (xResolution / toMM);
			const float yMm = height / (yResolution / toMM);

			const float diagMm = std::sqrt(xMm * xMm + yMm * yMm);
			const float idealDiagMm = std::sqrt(24 * 24 + 36 * 36);

			const float cropFactor = idealDiagMm / diagMm;

			logDebug("Crop factor %f\n", cropFactor);

			return focalLength * cropFactor;
		}
	}
}

void GeometryCorrectionGUIData::DoIt(FilterRecord* message) {
	const float focusInMm = getFocusMmInExif();
	logDebug("Data exif %f\n", focusInMm);

	HWND hParent = GetActiveWindow();
	MyDialog2 dial(this, CWnd::FromHandle(hParent), focusInMm, focusInMm != 0.f);
	logDebug("End create dialog\n");

	INT_PTR result = dial.DoModal();
	logDebug("End dialog\n");

	if (result == IDOK) {
		logDebug("Ok\n");
	} else if (result == IDCANCEL) {
		logDebug("Cancel\n");
	}
}

void GeometryCorrectionGUIData::ExecuteCancel(void) {
	logDebug("Execute cancel\n");
	Auto_Desc filterDesc;

	checkSPErr(sPSActionDescriptor->PutBoolean(filterDesc.get(), keyIsCancel, true));

	checkSPErr(sPSActionDescriptor->AsHandle(filterDesc.get(), &param->descriptorParameters->descriptor));
}

void putString(Auto_Desc &filterDesc, DescriptorKeyID key, const std::string &str) {
	char *tmpStr = new char[str.size() + 1];
	tmpStr[str.size()] = 0;
	str.copy(tmpStr, str.size());
	checkSPErr(sPSActionDescriptor->PutString(filterDesc.get(), key, tmpStr));
	delete[] tmpStr;
}

void GeometryCorrectionGUIData::ExecuteFilter(
	float xMin, float yMin,
	float zAngle, float xAngle, float yAngle,
	float focalLengthInPixels,
	const std::string &algorithmName,
	float unsharpCoeff, bool notLinearizing,
	int widthReal, int heightReal,
	int widthNew, int heightNew,
	const std::string &algorithmTransforme
) {
	logDebug("execute filter: %d %d %d %d %f %f %f %d %s\n", widthReal, heightReal, widthNew, heightNew, xMin, yMin, unsharpCoeff, static_cast<int>(notLinearizing), algorithmTransforme.c_str());
	logDebug("execute filter angles: %f %f %f %f\n", zAngle, xAngle, yAngle, focalLengthInPixels);

	Auto_Desc filterDesc;

	checkSPErr(sPSActionDescriptor->PutBoolean(filterDesc.get(), keyIsCancel, false));

	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyWidthImage, unitNone, widthReal));
	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyHeightImage, unitNone, heightReal));

	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyWidthNew, unitNone, widthNew));
	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyHeightNew, unitNone, heightNew));

	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyXMin, unitNone, xMin));
	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyYMin, unitNone, yMin));
	
	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyZAngle, unitNone, zAngle));
	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyXAngle, unitNone, xAngle));
	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyYAngle, unitNone, yAngle));

	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyFocalLength, unitNone, focalLengthInPixels));

	putString(filterDesc, keyAlgorithmNumber, algorithmName);

	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyUnsharpCoeff, unitNone, unsharpCoeff));

	checkSPErr(sPSActionDescriptor->PutUnitFloat(filterDesc.get(), keyNotLinearizing, unitNone, notLinearizing));

	putString(filterDesc, keyAlgorithmTransformNumber, algorithmTransforme);

	checkSPErr(sPSActionDescriptor->AsHandle(filterDesc.get(), &param->descriptorParameters->descriptor));
}
