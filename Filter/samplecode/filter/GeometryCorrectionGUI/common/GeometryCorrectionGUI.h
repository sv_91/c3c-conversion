#ifndef _GeometryCorrectionFilter_H
#define _GeometryCorrectionFilter_H

#include "PIFilter.h"
#include "PIUtilities.h"
#include "FilterKeys.h"

extern FilterRecord* param;
extern SPBasicSuite * sSPBasic;

#include <vector>
#include <string>

class GeometryCorrectionGUIData {
public:

	void DoIt(FilterRecord * message);

	void ExecuteFilter(
		float xMin, float yMin, 
		float zAngle, float xAngle, float yAngle,
		float zConst,
		const std::string &algorithmName, 
		float unsharpCoeff, bool notLinearizing, 
		int widthReal, int heightReal,
		int widthNew, int heightNew,
		const std::string &algorithmTransforme
	);
	
	void ExecuteCancel(void);

};

#endif
