#ifndef _GeometryCorrectionGUICRIPTING_H
#define _GeometryCorrectionGUICRIPTING_H

#include "PIDefines.h"
#include "PIActions.h"
#include "PITerminology.h"

#include "GeometryCorrectionGUIUniqueString.h"

#ifndef Rez
#include "PIDefines.h"
#include "PITypes.h"
#include "PIAbout.h"
#include "PIFilter.h"
#include "PIUtilities.h"
#include "FilterKeys.h"
#endif

#define vendorName			"AdobeSDK"
#define plugInName			"GeometryCorrectionGUI"
#define plugInAETEComment	"GeometryCorrectionGUI filter plug-in"
#define plugInSuiteID		'sdk1'
#define plugInClassID		plugInSuiteID
#define plugInEventID		plugInClassID

#define plugInDescription \
	"An example plug-in filter module for Adobe Photoshop�."

#define keyDisposition 		'disP'
#define keyIgnoreSelection	'ignS'
#define typeMood			'mooD'
#define dispositionClear	'moD0'
#define dispositionCool		'moD1'
#define dispositionHot		'moD2'
#define dispositionSick		'moD3'

#ifndef Rez
OSErr ReadScriptParameters(Boolean* displayDialog);
OSErr WriteScriptParameters(void);
int16 ScriptToDialog(int32 script);
int32 DialogToScript(int16 dialog);
#endif
#endif
