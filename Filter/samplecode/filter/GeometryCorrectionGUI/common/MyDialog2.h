#pragma once

#define USING_MFC

#include <Stdafx.h>
#include "afxcmn.h"
#undef min
#undef max

#include "GeometryCorrectionGUI.h"
#include "resource.h"

#include "Image.h"

class CEditMy : public CEdit {

	bool isRealDigit(char character) {
		logDebug("character %d\n", character);
		if (character == 8 /*backspace*/ || character == '-' || character == '.' || '0' <= character && character <= '9') {
			return true;
		} else {
			return false;
		}
	}

	BOOL PreTranslateMessage(MSG* pMsg)	{
		int nTextLength = this->GetWindowTextLength();
		if (pMsg->message == WM_CHAR) {
			if (!isRealDigit(pMsg->wParam)) {
				return true;
			}
		}
		return CEdit::PreTranslateMessage(pMsg);
	}
};

struct Point2 {
	float x;
	float y;
};

class MyDialog2 : public CDialog {
private:

	DECLARE_DYNAMIC(MyDialog2)

	GeometryCorrectionGUIData *_instance;

	void saveImage1(const Image &image, FilterRecord *param, PSPixelMap &pixels);

	void getSizesOutImage(float zAngleRotation, float xAngleRotation, float yAngleRotation, int &widthNew, int &heightNew);

	bool isInitialized;

	Image imageIn;

	VRect pixelsBounds;
	Point2 scales;

	CEditMy zAngleRotationEdit;
	CSliderCtrl zAngleRotationSlider;
	float zAngleRotationSave;

	CEditMy xAngleRotationEdit;
	CSliderCtrl xAngleRotationSlider;
	float xAngleRotationSave;

	CEditMy yAngleRotationEdit;
	CSliderCtrl yAngleRotationSlider;
	float yAngleRotationSave;

	float getFloatOfCEdit(const CEdit &pole);

	void bindSpinToCEdit(int spinValue, CEdit &edit);
	void bindSpinToCEditInt(int spinValue, CEdit &edit);

	void bindEditToSlider(const CEdit &edit, CSliderCtrl &slider, float &saveValue);
	void bindSliderToEdit(const CSliderCtrl &slider, CEdit &edit, float &saveValue);

	CComboBox algorithmName;

	CStatic picture;

	CStatic sizesImage;

	// �������� ����������
	float focalLengthInMm;
	CEdit focalLengthEdit;
	CComboBox focalLengthComboBox;
	bool isFocalLengthExists;
	void MyDialog2::bindFocalEditToCombo(CComboBox &focalCombo, const CEdit &focalEdit);
	void MyDialog2::bindFocalComboToEdit(const CComboBox &focalCombo, CEdit &focalEdit);
	CStatic focalLengthWarn;
	void MyDialog2::showHideWarn();

	int stepGrid;
	CEdit gridEdit;

	int widthHH;
	int heightHH;

	long m_ScaleFactor;
	void SetupFilterRecordForProxy();
	int CalcProxyScaleFactor();

	CToolTipCtrl toolTip;

	afx_msg BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnNMCustomdrawSlider2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnEnChangeEdit4();
	afx_msg void OnNMCustomdrawSlider3(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeEdit3();
	afx_msg void OnNMCustomdrawSlider4(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeEdit5();
	afx_msg void OnEnChangeEdit6();
	afx_msg void OnCbnSelchangeCombo3();

	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

public:
	MyDialog2(GeometryCorrectionGUIData *instance, CWnd* pParentWnd, float focusMm_, bool focusIsEst_);
	virtual ~MyDialog2() {};

	// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnDeltaposSpin1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpin2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpin3(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpin4(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpin5(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
