#include <Stdafx.h>
#include "MyDialog2.h"
#include "afxdialogex.h"

#undef min
#undef max

#include "FilterBigDocument.h"

#include <sstream>

#include "common.h"
#include "Algorithms.h"
#include "commonFilter.h"
#include "SaveLoadImage.h"

IMPLEMENT_DYNAMIC(MyDialog2, CDialog)

BEGIN_MESSAGE_MAP(MyDialog2, CDialog)
	ON_BN_CLICKED(IDCANCEL, &MyDialog2::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &MyDialog2::OnBnClickedOk)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_CREATE()
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER2, &MyDialog2::OnNMCustomdrawSlider2)
	ON_EN_CHANGE(IDC_EDIT1, &MyDialog2::OnEnChangeEdit1)
	ON_EN_CHANGE(IDC_EDIT4, &MyDialog2::OnEnChangeEdit4)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER3, &MyDialog2::OnNMCustomdrawSlider3)
	ON_EN_CHANGE(IDC_EDIT3, &MyDialog2::OnEnChangeEdit3)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER4, &MyDialog2::OnNMCustomdrawSlider4)
	ON_EN_CHANGE(IDC_EDIT5, &MyDialog2::OnEnChangeEdit5)
	ON_EN_CHANGE(IDC_EDIT6, &MyDialog2::OnEnChangeEdit6)
	ON_CBN_SELCHANGE(IDC_COMBO3, &MyDialog2::OnCbnSelchangeCombo3)
	ON_BN_CLICKED(IDC_BUTTON1, &MyDialog2::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &MyDialog2::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &MyDialog2::OnBnClickedButton3)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN1, &MyDialog2::OnDeltaposSpin1)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN2, &MyDialog2::OnDeltaposSpin2)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN3, &MyDialog2::OnDeltaposSpin3)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN4, &MyDialog2::OnDeltaposSpin4)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN5, &MyDialog2::OnDeltaposSpin5)
END_MESSAGE_MAP()

const int ULTRA_WIDE_ANGLE_LENS_BEGIN = 12;
const int WIDE_ANGLE_LENS_BEGIN = 24;
const int NORMAL_LENS_BEGIN = 35;
const int LONG_FOCUS_LENS_BEGIN = 70;

const std::string ULTRA_WIDE_ANGLE_LENS("Ultra wide angle lens (12-24mm)");
const std::string WIDE_ANGLE_LENS("Wide angle lens (24-35mm)");
const std::string NORMAL_LENS("Normal lens (35-70mm)");
const std::string LONG_FOCUS_LENS("Long-focus lens (>70mm)");
const std::string OTHER_LENS("Other");

const char* ZERO = "0.0";

MyDialog2::MyDialog2(GeometryCorrectionGUIData *instance, CWnd* pParentWnd, float focusMm_, bool focusIsEst_)
	: CDialog(MyDialog2::IDD, pParentWnd)
	, isInitialized(false)
	, _instance(instance)
	, imageIn(1, 1)
	, focalLengthInMm(focusIsEst_ ? focusMm_ : 50.f)
	, stepGrid(30)
	, isFocalLengthExists(focusIsEst_)
{}

float getAngleOnSlider(float slider) {
	return std::tan((slider) / 401.f);
}

float getFocalLengthInPixelIsFromFocalLengthInMm(float focalLengthMm, int width, int height) {
	return static_cast<float>(focalLengthMm / std::sqrt(24 * 24 + 36 * 36) * std::sqrt(width * width + height * height));
}

float MyDialog2::getFloatOfCEdit(const CEdit &edit) {
	CString text;
	edit.GetWindowText(text);
	std::stringstream ss(text.GetString());
	float value;
	ss >> value;
	if (text.IsEmpty() || text.Compare("-") == 0) {
		value = 0.f;
	}
	logDebug("value of CEdit %s %d \n", text.GetString(), value);

	return value;
}

void MyDialog2::showHideWarn() {
	if (isFocalLengthExists || (focalLengthEdit.GetStyle() & ES_READONLY)) {
		focalLengthWarn.ShowWindow(SW_HIDE);
	} else {
		focalLengthWarn.ShowWindow(SW_SHOW);
	}
}

void MyDialog2::bindFocalEditToCombo(CComboBox &focalCombo, const CEdit &focalEdit) {
	const int focalLength = static_cast<int>(getFloatOfCEdit(focalEdit));
	if (focalLength == 0) {
		focalCombo.SetCurSel(4);
		focalCombo.EnableWindow(FALSE);
	} else {
		focalCombo.EnableWindow(TRUE);
		if (ULTRA_WIDE_ANGLE_LENS_BEGIN <= focalLength && focalLength < WIDE_ANGLE_LENS_BEGIN) {
			focalCombo.SetCurSel(0);
		} else if (WIDE_ANGLE_LENS_BEGIN <= focalLength && focalLength < NORMAL_LENS_BEGIN) {
			focalCombo.SetCurSel(1);
		} else if (NORMAL_LENS_BEGIN <= focalLength && focalLength < LONG_FOCUS_LENS_BEGIN) {
			focalCombo.SetCurSel(2);
		} else if (LONG_FOCUS_LENS_BEGIN <= focalLength) {
			focalCombo.SetCurSel(3);
		} else {
			focalCombo.SetCurSel(4);
		}
	}

	showHideWarn();
}

void MyDialog2::bindFocalComboToEdit(const CComboBox &focalCombo, CEdit &focalEdit) {
	CString focalStringS;
	focalCombo.GetLBText(focalCombo.GetCurSel(), focalStringS);
	std::string focalString = focalStringS.GetString();

	if (focalString == ULTRA_WIDE_ANGLE_LENS) {
		focalEdit.SetWindowTextA(floatToStr((ULTRA_WIDE_ANGLE_LENS_BEGIN + WIDE_ANGLE_LENS_BEGIN) / 2.f).c_str());
	} else if (focalString == WIDE_ANGLE_LENS) {
		focalEdit.SetWindowTextA(floatToStr(30.f).c_str());
	} else if (focalString == NORMAL_LENS) {
		focalEdit.SetWindowTextA(floatToStr(50.f).c_str());
	} else if (focalString == LONG_FOCUS_LENS) {
		focalEdit.SetWindowTextA(floatToStr(LONG_FOCUS_LENS_BEGIN + 0.f).c_str());
	} else if (focalString == OTHER_LENS) {
		// ok
	} else {
		throw std::string("Ya tuta0090");
	}

	showHideWarn();
}

void MyDialog2::DoDataExchange(CDataExchange* pDX) {
	logDebug("Do data exchange\n");

	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_EDIT1, zAngleRotationEdit);
	DDX_Control(pDX, IDC_COMBO1, algorithmName);
	DDX_Control(pDX, IDC_COMBO3, focalLengthComboBox);
	DDX_Control(pDX, IDC_PROXY, picture);
	DDX_Control(pDX, IDC_STATIC1, sizesImage);
	DDX_Control(pDX, IDC_SLIDER2, zAngleRotationSlider);
	DDX_Control(pDX, IDC_EDIT4, xAngleRotationEdit);
	DDX_Control(pDX, IDC_SLIDER3, xAngleRotationSlider);
	DDX_Control(pDX, IDC_EDIT3, yAngleRotationEdit);
	DDX_Control(pDX, IDC_SLIDER4, yAngleRotationSlider);
	DDX_Control(pDX, IDC_EDIT5, focalLengthEdit);
	DDX_Control(pDX, IDC_EDIT6, gridEdit);
	DDX_Control(pDX, IDC_PROXY3, focalLengthWarn);
}

BOOL MyDialog2::OnInitDialog() {
	CDialog::OnInitDialog();

	logDebug("OnInitDialog\n");

	toolTip.Create(this);
	toolTip.SetMaxTipWidth(0xFFFFFF);
	toolTip.AddTool(&focalLengthWarn, "Plugin is not able to determine the equivalent focal length.\r\nPlease indicate the equivalent focal length manually");
	toolTip.Activate(TRUE);

	// angles
	zAngleRotationEdit.SetWindowTextA(ZERO);

	zAngleRotationSlider.SetRangeMin(-100*2, true);
	zAngleRotationSlider.SetRangeMax(100*2, true);
	zAngleRotationSlider.SetPos(0);
	zAngleRotationSlider.SetTic(0);

	zAngleRotationSave = 0;


	xAngleRotationEdit.SetWindowTextA(ZERO);

	xAngleRotationSlider.SetRangeMin(-180*2, true);
	xAngleRotationSlider.SetRangeMax(180*2, true);
	xAngleRotationSlider.SetPos(0);
	xAngleRotationSlider.SetTic(0);

	xAngleRotationSave = 0;


	yAngleRotationEdit.SetWindowTextA(ZERO);

	yAngleRotationSlider.SetRangeMin(-180*2, true);
	yAngleRotationSlider.SetRangeMax(180*2, true);
	yAngleRotationSlider.SetPos(0);
	yAngleRotationSlider.SetTic(0);

	yAngleRotationSave = 0;

	// all
	algorithmName.AddString(bicubickStr.c_str());
	algorithmName.AddString(bilinearStr.c_str());
	algorithmName.AddString(monteKarloStr.c_str());
	algorithmName.AddString(SazerlHodjmenStr.c_str());
	algorithmName.AddString(sincStr.c_str());
	algorithmName.SetCurSel(0);

	focalLengthComboBox.AddString(ULTRA_WIDE_ANGLE_LENS.c_str());
	focalLengthComboBox.AddString(WIDE_ANGLE_LENS.c_str());
	focalLengthComboBox.AddString(NORMAL_LENS.c_str());
	focalLengthComboBox.AddString(LONG_FOCUS_LENS.c_str());
	focalLengthComboBox.AddString(OTHER_LENS.c_str());

	focalLengthEdit.SetWindowTextA(floatToStr(focalLengthInMm).c_str());
	bindFocalEditToCombo(focalLengthComboBox, focalLengthEdit);

	gridEdit.SetWindowTextA(intToStr(stepGrid).c_str());

	// �������� ������� ����, � ������� ����� ��������.
	CWnd* pWnd = GetDlgItem(IDC_PROXY);
	if (pWnd == NULL) throw((char*)"No pWnd in MyMFCDialog::OnPaint()");

	CRect rect0;
	pWnd->GetWindowRect(rect0);
	ScreenToClient(rect0);

	CRect rect;
	this->GetWindowRect(rect);
	widthHH = rect.Width() - rect0.left - 10;
	heightHH = rect.Height() - rect0.top - 30;

	this->SetupFilterRecordForProxy();

	isInitialized = true;

	logDebug("End OnInitDialog\n");

	return true;
}

void MyDialog2::OnBnClickedCancel() {
	_instance->ExecuteCancel();

	CDialog::OnCancel();
}

void MyDialog2::getSizesOutImage(float zAngleRotation, float xAngleRotation, float yAngleRotation, int &widthNew, int &heightNew) {
	const float m_ScaleFactor2 = (float)param->imageSize.h / imageIn.width;
	const float focalLengthInPixels = getFocalLengthInPixelIsFromFocalLengthInMm(focalLengthInMm, param->imageSize.h, param->imageSize.v);

	const LinearConversion transform1 = LinearConversion::make(
		zAngleRotation, xAngleRotation, yAngleRotation,
		focalLengthInPixels,
		imageIn.xMin * m_ScaleFactor2, imageIn.yMin * m_ScaleFactor2,
		param->imageSize.h, param->imageSize.v,
		alg3
		).getObr();

	widthNew = getWidthConversion(param->imageSize.h, param->imageSize.v, transform1);
	heightNew = getHeightConversion(param->imageSize.h, param->imageSize.v, transform1);
}

void MyDialog2::OnBnClickedOk() {
	CString algRotStr;
	algorithmName.GetLBText(algorithmName.GetCurSel(), algRotStr);

	const float unsharpMaskFactor = 0.f;

	const bool linear = true;

	const float m_ScaleFactor2 = (float)param->imageSize.h / imageIn.width;

	const float focalLengthInPixels = getFocalLengthInPixelIsFromFocalLengthInMm(focalLengthInMm, param->imageSize.h, param->imageSize.v);
	const float zAngle = getAngleOnSlider(zAngleRotationSave);
	const float xAngle = getAngleOnSlider(xAngleRotationSave);
	const float yAngle = getAngleOnSlider(yAngleRotationSave);

	int widthNew, heightNew;
	getSizesOutImage(zAngle, xAngle, yAngle, widthNew, heightNew);

	_instance->ExecuteFilter(
		imageIn.xMin * m_ScaleFactor2,
		imageIn.yMin * m_ScaleFactor2,
		zAngle, xAngle, yAngle,
		focalLengthInPixels,
		std::string(algRotStr.GetString()), 
		unsharpMaskFactor, !linear, 
		param->imageSize.h, param->imageSize.v,
		widthNew, heightNew,
		alg3
	);

	logDebug("Execute filter ok\n");

	CDialog::OnOK();
}

void MyDialog2::SetupFilterRecordForProxy() {
	m_ScaleFactor = CalcProxyScaleFactor();
	logDebug("scalefactor %d\n", m_ScaleFactor);

	param->inputRate = m_ScaleFactor << 16;

	param->maskRate = param->inputRate;

	param->inputPadding = 255;
	param->outputPadding = param->inputPadding;
	param->maskPadding = param->inputPadding;

	VRect zeroRect = { 0, 0, param->imageSize.v / m_ScaleFactor, param->imageSize.h / m_ScaleFactor };
	SetInRect(zeroRect);
	SetOutRect(zeroRect);

	checkSPErr(param->advanceState());

	logDebug("before load image\n");
	imageIn = loadImage(param, param->bigDocumentData->inRect32.right, param->bigDocumentData->inRect32.bottom, lin2Lin);
	logDebug("after load image\n");

	imageIn.xMin = -imageIn.width / 2.f;
	imageIn.yMin = -imageIn.height / 2.f;
}

/* Computes the scaled down rectangle and the scale factor for the proxy */
int MyDialog2::CalcProxyScaleFactor() {
	long imageWidth = param->imageSize.h;
	long imageHeight = param->imageSize.v;

	long widthToHeightRatio = imageWidth / imageHeight;

	int m_ScaleFactor;
	if (widthToHeightRatio >= 1) {
		m_ScaleFactor = static_cast<int>(std::floor((float)imageWidth / widthHH));
	} else {
		m_ScaleFactor = static_cast<int>(std::floor((float)imageHeight / heightHH));
	}

	if (m_ScaleFactor < 1) {
		m_ScaleFactor = 1;
	}

	return m_ScaleFactor;
}

std::vector<unsigned char> outData2;

void MyDialog2::saveImage1(const Image &image, FilterRecord *param, PSPixelMap &pixels) {
	const int width = image.width;
	const int height = image.height;

	int mode = param->imageMode;
	if (mode == plugInModeRGB48 || mode == plugInModeGray16) {
		if (mode == plugInModeRGB48) {
			mode = plugInModeRGBColor;
		} else {
			mode = plugInModeGrayScale;
		}
	}

	const int centerX = (widthHH - width) / 2;
	const int centerY = (heightHH - height) / 2;

	pixels.version = 1;
	pixels.bounds.top = centerY;
	pixels.bounds.left = centerX;
	pixels.bounds.bottom = height + centerY;
	pixels.bounds.right = width + centerX;
	pixels.imageMode = mode;
	pixels.rowBytes = width * param->planes;
	pixels.colBytes = param->inHiPlane
		- param->inLoPlane + 1;
	pixels.planeBytes = 1;

	outData2.resize(width * height * param->planes);

	saveImage(image, image.width, image.height, mode, &outData2[0], width * param->planes, lin2Lin);

	pixels.baseAddr = &outData2[0];

	pixels.mat = NULL;
	pixels.masks = NULL;
	pixels.maskPhaseRow = 0;
	pixels.maskPhaseCol = 0;
}

void MyDialog2::OnPaint() {
	if (!isInitialized) {
		return;
	}

	logDebug("OnPaint\n");

	CPaintDC dc(this); // device context for painting

	float zAngleRotation = getAngleOnSlider(zAngleRotationSave);

	const float xAngleRotation = getAngleOnSlider(xAngleRotationSave);
	const float yAngleRotation = getAngleOnSlider(yAngleRotationSave);
	LinearConversion transformConversion = LinearConversion::make(
		zAngleRotation, xAngleRotation, yAngleRotation, 
		getFocalLengthInPixelIsFromFocalLengthInMm(focalLengthInMm, imageIn.width, imageIn.height),
		imageIn.xMin, imageIn.yMin,
		imageIn.width, imageIn.height,
		alg3
	);

	Image image2 = imageIn; // �������� �����������

	// ������ ��������� ��������� ����� �������������
	const int radiusD = 3;
	for (int dx = -radiusD; dx <= radiusD; dx++) {
		for (int dy = -radiusD; dy <= radiusD; dy++) {
			const int x = -(int)imageIn.xMin + dx;
			const int y = -(int)imageIn.yMin + dy;
			if (0 <= x && x < image2.getWidth() && 0 <= y && y < image2.getHeight()) {
				image2.setColorsInvertTo0Or1(x, y);
			}
		}
	}
	logDebug("Paint center %d %d\n", -(int)imageIn.xMin, -(int)imageIn.yMin);

	const int widthNew = getWidthConversion(image2.width, image2.height, transformConversion.getObr());
	const int heightNew = getHeightConversion(image2.width, image2.height, transformConversion.getObr());

	logDebug("Size 1 %d %d %d %d\n", widthNew, heightNew, widthHH, heightHH);

	// ���� ������ ����������� ������ �����������, ������������.
	if (widthNew > widthHH || heightNew > heightHH) {
		float mWidth = (float)widthNew / widthHH;
		float mHeight = (float)heightNew / heightHH;
		float mMax = std::max(mWidth, mHeight);

		const LinearConversion sc = LinearConversion::getMultiplier(mMax, mMax);

		transformConversion = LinearConversion::make(::multiplie(transformConversion.getMatrix(), sc.getMatrix()));
	}

	Image image = bilinearInterpolation::transform(image2, transformConversion);
	logDebug("Size 2 %d %d %d %d\n", image.width, image.height, widthHH, heightHH);

	// ������ �����
	if (stepGrid != 0) {
		for (int i = 0; i < image.getWidth(); i += stepGrid) {
			for (int j = 0; j < image.getHeight(); j++) {
				image.setColorsInvertTo0Or1(i, j);
			}
		}

		for (int j = 0; j < image.getHeight(); j += stepGrid) {
			for (int i = 0; i < image.getWidth(); i++) {
				image.setColorsInvertTo0Or1(i, j);
			}
		}
	}

	// ��������� �����������
	PSPixelMap pixels;
	saveImage1(image, param, pixels);
	pixelsBounds = pixels.bounds;
	scales.x = (float)image.width / imageIn.width;
	scales.y = (float)image.height / imageIn.height;
	logDebug("%d %d %d %d\n", pixels.bounds.top, pixels.bounds.left, pixels.bounds.bottom, pixels.bounds.right);

	// ������
	CWnd* pWnd = GetDlgItem(IDC_PROXY);
	if (pWnd == NULL) {
		throw(std::string("No pWnd in MyMFCDialog::OnPaint()"));
	}

	PAINTSTRUCT ps;
	CDC *dc2 = pWnd->BeginPaint(&ps);

	dc2->FillSolidRect(0, 0, widthHH, pixels.bounds.top, RGB(0, 0, 0));
	dc2->FillSolidRect(0, 0, pixels.bounds.left, heightHH, RGB(0, 0, 0));
	dc2->FillSolidRect(0, pixels.bounds.bottom, widthHH, heightHH, RGB(0, 0, 0));
	dc2->FillSolidRect(pixels.bounds.right, 0, widthHH, heightHH, RGB(0, 0, 0));

	check(param->displayPixels(&pixels, &pixels.bounds, pixels.bounds.top, pixels.bounds.left, (void*)ps.hdc) == NULL);

	pWnd->EndPaint(&ps);

	// ������� �� ����� ���������� � �������� ��������� �����������
	int widthNewReal;
	int heightNewReal;
	getSizesOutImage(zAngleRotation, xAngleRotation, yAngleRotation, widthNewReal, heightNewReal);

	long long imageNewSize = (long long)widthNewReal * heightNewReal;
	std::string measure;
	if (imageNewSize <= 1024) {
		measure = "p";
	} else if (imageNewSize <= 1024 * 1024) {
		measure = "Kp";
		imageNewSize /= 1024L;
	} else {
		measure = "Mp";
		imageNewSize /= 1024L * 1024L;
	}

	sizesImage.SetWindowTextA(
		(std::string("Image sizes ") 
		+ intToStr(widthNewReal) + "x" + intToStr(heightNewReal) 
		+ "=" + intToStr(static_cast<int>(imageNewSize)) + " " + measure).c_str()
	);
}

void MyDialog2::OnLButtonDown(UINT nFlags, CPoint point) {
	if (zAngleRotationSave == 0 && xAngleRotationSave == 0 && yAngleRotationSave == 0) {
		logDebug("MyPointDo %d %d\n", point.x, point.y);
		CRect rect;
		CWnd *pWnd = GetDlgItem(IDC_PROXY);
		pWnd->GetWindowRect(&rect);
		logDebug("MyPointPosle %d %d %d %d\n", point.x, point.y, rect.left, rect.top);
		CRect rect2;
		this->GetWindowRect(&rect2);
		ScreenToClient(&rect);
		CPoint point2;
		point2.x = point.x - rect.left;
		point2.y = point.y - rect.top;
		logDebug("MyPointPosle %d %d %d %d %d %d\n", point2.x, point2.y, rect.left, rect.top, rect2.left, rect2.top);

		if (
			pixelsBounds.left <= point2.x && point2.x < pixelsBounds.right &&
			pixelsBounds.top <= point2.y && point2.y < pixelsBounds.bottom
		) {
			imageIn.xMin = static_cast<float>(-(point2.x - pixelsBounds.left)) / scales.x;
			imageIn.yMin = static_cast<float>(-(point2.y - pixelsBounds.top)) / scales.y;

			logDebug("New point %f %f %f %f\n", imageIn.xMin, imageIn.yMin, scales.x, scales.y);
			Invalidate(false);
		}
	}

	CDialog::OnLButtonDown(nFlags, point);
}

int MyDialog2::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	this->ShowWindow(SW_SHOWMAXIMIZED);

	if (CDialog::OnCreate(lpCreateStruct) == -1) {
		return -1;
	}

	return 0;
}

void MyDialog2::bindEditToSlider(const CEdit &edit, CSliderCtrl &slider, float &saveValue) {
	float value = getFloatOfCEdit(edit);

	if (saveValue != value) {
		saveValue = value;
		slider.SetPos(static_cast<int>(std::round(value * 2.f)));
		Invalidate(false);
	}
}

void MyDialog2::bindSliderToEdit(const CSliderCtrl &slider, CEdit &edit, float &saveValue) {
	if (std::floor(saveValue) <= (float)slider.GetPos() / 2.f && (float)slider.GetPos() / 2.f <= std::ceil(saveValue)) {
		return;
	} else {
		saveValue = static_cast<float>(slider.GetPos()) / 2.f;
		edit.SetWindowTextA(floatToStr(saveValue).c_str());
		Invalidate(false);
	}
}

void MyDialog2::OnNMCustomdrawSlider2(NMHDR *pNMHDR, LRESULT *pResult) {
	bindSliderToEdit(zAngleRotationSlider, zAngleRotationEdit, zAngleRotationSave);

	*pResult = 0;
}

void MyDialog2::OnEnChangeEdit1() {
	bindEditToSlider(zAngleRotationEdit, zAngleRotationSlider, zAngleRotationSave);
}

void MyDialog2::OnEnChangeEdit4() {
	bindEditToSlider(xAngleRotationEdit, xAngleRotationSlider, xAngleRotationSave);
}


void MyDialog2::OnNMCustomdrawSlider3(NMHDR *pNMHDR, LRESULT *pResult) {
	bindSliderToEdit(xAngleRotationSlider, xAngleRotationEdit, xAngleRotationSave);

	*pResult = 0;
}

void MyDialog2::OnEnChangeEdit3() {
	bindEditToSlider(yAngleRotationEdit, yAngleRotationSlider, yAngleRotationSave);
}

void MyDialog2::OnNMCustomdrawSlider4(NMHDR *pNMHDR, LRESULT *pResult) {
	bindSliderToEdit(yAngleRotationSlider, yAngleRotationEdit, yAngleRotationSave);

	*pResult = 0;
}

// Focal length
void MyDialog2::OnEnChangeEdit5() {
	if (!(focalLengthEdit.GetStyle() & ES_READONLY)) {
		if (focalLengthInMm != getFloatOfCEdit(focalLengthEdit)) {
			isFocalLengthExists = true;
			focalLengthInMm = getFloatOfCEdit(focalLengthEdit);
			if (focalLengthInMm != 0) {
				bindFocalEditToCombo(focalLengthComboBox, focalLengthEdit);
				Invalidate(false);
			}
		}
	}
}

// Grid
void MyDialog2::OnEnChangeEdit6() {
	int value = static_cast<int>(std::round(getFloatOfCEdit(gridEdit)));
	if (stepGrid != value) {
		stepGrid = value;
		Invalidate(false);
	}
}

void MyDialog2::OnCbnSelchangeCombo3() {
	isFocalLengthExists = true;
	bindFocalComboToEdit(focalLengthComboBox, focalLengthEdit);
}

void MyDialog2::OnBnClickedButton1() {
	zAngleRotationEdit.SetWindowTextA(ZERO);
}

void MyDialog2::OnBnClickedButton2() {
	xAngleRotationEdit.SetWindowTextA(ZERO);
}

void MyDialog2::OnBnClickedButton3() {
	yAngleRotationEdit.SetWindowTextA(ZERO);
}

void MyDialog2::bindSpinToCEdit(int spinValue, CEdit &edit) {
	float saveValue = getFloatOfCEdit(edit);
	saveValue -= spinValue * 0.5f;

	edit.SetWindowTextA(floatToStr(saveValue).c_str());
}

void MyDialog2::bindSpinToCEditInt(int spinValue, CEdit &edit) {
	int saveValue = static_cast<int>(getFloatOfCEdit(edit));
	saveValue -= spinValue;

	edit.SetWindowTextA(intToStr(saveValue).c_str());
}

void MyDialog2::OnDeltaposSpin1(NMHDR *pNMHDR, LRESULT *pResult) {
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	
	int iPos = pNMUpDown->iDelta;
	bindSpinToCEdit(iPos, zAngleRotationEdit);

	*pResult = 0;
}

void MyDialog2::OnDeltaposSpin2(NMHDR *pNMHDR, LRESULT *pResult) {
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	int iPos = pNMUpDown->iDelta;
	bindSpinToCEdit(iPos, xAngleRotationEdit);

	*pResult = 0;
}

void MyDialog2::OnDeltaposSpin3(NMHDR *pNMHDR, LRESULT *pResult) {
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	int iPos = pNMUpDown->iDelta;
	bindSpinToCEdit(iPos, yAngleRotationEdit);

	*pResult = 0;
}

void MyDialog2::OnDeltaposSpin4(NMHDR *pNMHDR, LRESULT *pResult) {
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	int iPos = pNMUpDown->iDelta;
	bindSpinToCEditInt(iPos, gridEdit);

	*pResult = 0;
}

void MyDialog2::OnDeltaposSpin5(NMHDR *pNMHDR, LRESULT *pResult) {
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	int iPos = pNMUpDown->iDelta;
	bindSpinToCEdit(iPos, focalLengthEdit);

	*pResult = 0;
}

BOOL MyDialog2::PreTranslateMessage(MSG* pMsg) {
	toolTip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}
