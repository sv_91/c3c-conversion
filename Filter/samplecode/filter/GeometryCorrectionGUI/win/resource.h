//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GeometryCorrectionGUI.rc
//
#define IDD_DIALOG1                     101
#define PLUGINNAME                      255
#define IDC_BUTTON1                     1000
#define IDC_BUTTON2                     1001
#define IDC_PROXY                       1002
#define IDC_EDIT4                       1004
#define IDC_SLIDER3                     1005
#define IDC_EDIT1                       1006
#define IDC_EDIT3                       1007
#define IDC_SLIDER4                     1008
#define IDC_EDIT5                       1009
#define IDC_COMBO1                      1010
#define IDC_SLIDER1                     1011
#define IDC_SLIDER2                     1012
#define IDC_EDIT6                       1013
#define IDC_COMBO2                      1014
#define IDC_EDIT2                       1016
#define IDC_COMBO3                      1017
#define IDC_PROXY2                      1018
#define IDC_PROXY3                      1019
#define IDC_CHECK4                      1020
#define IDC_BUTTON3                     1021
#define IDC_SPIN1                       1022
#define IDC_SPIN2                       1023
#define IDC_SPIN3                       1024
#define IDC_SPIN4                       1025
#define IDC_SPIN5                       1026
#define IDC_STATIC1                     1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
