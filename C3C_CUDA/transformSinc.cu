#include "Algorithms.h"

#include "commonCUDA.h"
#include "ConversionCUDA.h"

namespace sinc {

#include "preTransform.inc"

cudaTextureFilterMode cudaFilterMode = cudaFilterModePoint;

__device__ float sinc(float x) {
	const float PI = 3.141592653589793238463f;
	if (x == 0.f) {
		return 1.f;
	} else {
		return std::sin(PI * x) / (PI * x);
	}
}

__device__ float sinc(float x, float y, float i, float j, float a) {
	return sinc(x - i) * sinc(y - j);
}

__device__ float lanczos(float x, float a) {
	if (std::abs(x) < a) {
		return sinc(x) * sinc(x / a);
	} else {
		return 0.f;
	}
}

__device__ float lanczos(float x, float y, float i, float j, float a) {
	return lanczos(x - i, a) * lanczos(y - j, a);
}

__device__ float jinc(float x) {
	const float PI = 3.141592653589793238463f;
	return (std::fabs(PI * x) < 1e-8) ? 0.5 : j1(PI * x) / (2 * x);
}

__device__ float jinc(float x, float y, float i, float j, float a) {
	return jinc(std::sqrt((x - i) * (x - i) + (y - j) * (y - j)));
}

#define RADIUS (3)

__global__ void sincKernel(float* __restrict__ rOut, float* __restrict__ gOut, float* __restrict__ bOut, int widthOut, int pitchOut, int heightOut, float xMin, float yMin) {
	const int x = threadIdx.x + blockIdx.x * blockDim.x;
	const int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= widthOut || y >= heightOut) {
		return;
	}

	const float x0 = x;
	const float y0 = y;

	const PointCUDA pointNew = getConversion(x0, y0);
	const float xNew = pointNew.x;
	const float yNew = pointNew.y;

	float ansR = 0;
	float ansG = 0;
	float ansB = 0;

	const float radiusF = static_cast<float>(RADIUS);

	for (int m = -RADIUS; m <= RADIUS; m++) {
		const float y1F = yNew + m;
		const float y1FFloor = floorf(y1F);
		float resR2 = 0;
		float resG2 = 0;
		float resB2 = 0;
		for (int n = -RADIUS; n <= RADIUS; n++) {
			const float x1F = xNew + n;
			const float x1FFloor = floorf(x1F);
			const float funcR = lanczos(xNew - x1FFloor, radiusF);
			resR2 += funcR * tex2D(texRefR, x1FFloor, y1FFloor);
			resG2 += funcR * tex2D(texRefG, x1FFloor, y1FFloor);
			resB2 += funcR * tex2D(texRefB, x1FFloor, y1FFloor);
		}

		const float funcR2 = lanczos(yNew - y1FFloor, radiusF);

		ansR += resR2 * funcR2;
		ansG += resG2 * funcR2;
		ansB += resB2 * funcR2;
	}

	clampCUDA(ansR);
	clampCUDA(ansG);
	clampCUDA(ansB);

	rOut[x + y * pitchOut] = ansR;
	gOut[x + y * pitchOut] = ansG;
	bOut[x + y * pitchOut] = ansB;
}

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start sinc interpolation CUDA\n";
}

inline void work(const int width, const int height, float *rOutDevice, float *gOutDevice, float *bOutDevice, const int outWidth, const int outPitchElements, const int outHeight, const float outXMin, const float outYMin) {
	checkCudaCall(cudaFuncSetCacheConfig(sincKernel, cudaFuncCachePreferL1));
	dim3 block(32, 4);
	dim3 grid((outWidth + block.x - 1) / block.x, (outHeight + block.y - 1) / block.y);
	checkAndGetTimeKernelRun(sincKernel<<<grid, block>>>(rOutDevice, gOutDevice, bOutDevice, outWidth, outPitchElements, outHeight, outXMin, outYMin));
}

#include "transform.inc"

}
