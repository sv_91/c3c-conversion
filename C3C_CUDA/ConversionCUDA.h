#pragma once

#include <cuda_runtime.h>
#include "commonCUDA.h"

struct PointCUDA {
	friend class SH::Polygon;

	float x;
	float y;

	__device__ PointCUDA(float x_, float y_)
		: x(x_)
		, y(y_)
	{}

	__device__ PointCUDA(float x_, float y_, float z_)
		: x(x_ / z_)
		, y(y_ / z_)
	{
		checkKernel(z_ != 0.f);
	}

	__device__ PointCUDA operator- (const PointCUDA &second) const {
		return PointCUDA(this->x - second.x, this->y - second.y);
	}

private:

	__device__ PointCUDA()
		: x(0)
		, y(0)
	{}
};

struct Point3CUDA {
public:

	float x;
	float y;
	float z;

	__device__ Point3CUDA(float x, float y)
		: x(x)
		, y(y)
		, z(1.f)
	{}

	__device__ Point3CUDA(float x, float y, float z)
		: x(x)
		, y(y)
		, z(z)
	{}

	__device__ float getX() const {
		checkKernel(z != 0.f);
		return x / z;
	}

	__device__ float getY() const {
		checkKernel(z != 0.f);
		return y / z;
	}

	__device__ Point3CUDA operator+ (const Point3CUDA &y) const {
		return Point3CUDA(this->x + y.x, this->y + y.y, this->z + y.z);
	}

};

__constant__ float matrixTransform[3 * 3];

static __device__ PointCUDA getConversion(float x, float y) {
	const int width = 3;
	return PointCUDA(
		matrixTransform[0 + 0 * width] * x + matrixTransform[0 + 1 * width] * y + matrixTransform[0 + 2 * width] * 1.f,
		matrixTransform[1 + 0 * width] * x + matrixTransform[1 + 1 * width] * y + matrixTransform[1 + 2 * width] * 1.f,
		matrixTransform[2 + 0 * width] * x + matrixTransform[2 + 1 * width] * y + matrixTransform[2 + 2 * width] * 1.f
	);
}

static __device__ Point3CUDA getConversion(const Point3CUDA &p) {
	const int width = 3;
	return Point3CUDA(
		matrixTransform[0 + 0 * width] * p.x + matrixTransform[0 + 1 * width] * p.y + matrixTransform[0 + 2 * width] * p.z,
		matrixTransform[1 + 0 * width] * p.x + matrixTransform[1 + 1 * width] * p.y + matrixTransform[1 + 2 * width] * p.z,
		matrixTransform[2 + 0 * width] * p.x + matrixTransform[2 + 1 * width] * p.y + matrixTransform[2 + 2 * width] * p.z
	);
}
