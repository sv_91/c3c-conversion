#include "Algorithms.h"

#include <vector>

#include "commonCUDA.h"
#include "ConversionCUDA.h"

namespace MKNormal1 {

#include "preTransform.inc"

	cudaTextureFilterMode cudaFilterMode = cudaFilterModePoint;

	inline void beginTransform(const LinearConversion &sc) {
		throw std::string("Not realised");
	}

	inline void work(const int width, const int height, float *rOutDevice, float *gOutDevice, float *bOutDevice, const int outWidth, const int outPitchElements, const int outHeight, const float outXMin, const float outYMin) {
	}

#include "transform.inc"

}

namespace MKNormal2 {

#include "preTransform.inc"

	cudaTextureFilterMode cudaFilterMode = cudaFilterModePoint;

	inline void beginTransform(const LinearConversion &sc) {
		throw std::string("Not realised");
	}

	inline void work(const int width, const int height, float *rOutDevice, float *gOutDevice, float *bOutDevice, const int outWidth, const int outPitchElements, const int outHeight, const float outXMin, const float outYMin) {
	}

#include "transform.inc"

}

namespace MKNormal3 {

#include "preTransform.inc"

	cudaTextureFilterMode cudaFilterMode = cudaFilterModePoint;

	inline void beginTransform(const LinearConversion &sc) {
		throw std::string("Not realised");
	}

	inline void work(int width, int height, float *rOutDevice, float *gOutDevice, float *bOutDevice, const int outWidth, const int outPitchElements, const int outHeight, const float outXMin, const float outYMin) {
	}

#include "transform.inc"

}

namespace MKNormal4 {

#include "preTransform.inc"

	cudaTextureFilterMode cudaFilterMode = cudaFilterModePoint;

	inline void beginTransform(const LinearConversion &sc) {
		throw std::string("Not realised");
	}

	inline void work(const int width, const int height, float *rOutDevice, float *gOutDevice, float *bOutDevice, const int outWidth, const int outPitchElements, const int outHeight, const float outXMin, const float outYMin) {
	}

#include "transform.inc"

}
