texture<float, cudaTextureType2D, cudaReadModeElementType> texRefR;
texture<float, cudaTextureType2D, cudaReadModeElementType> texRefG;
texture<float, cudaTextureType2D, cudaReadModeElementType> texRefB;

extern cudaTextureFilterMode cudaFilterMode;
