#include "Algorithms.h"

#include <vector>

#include <time.h>

#include "ConversionCUDA.h"
#include "commonCUDA.h"

namespace monteKarlo {

	const int COUNT_ITERATION = 500;

	std::vector<float> randoms;

	__constant__ float randomsCuda[COUNT_ITERATION * 3];

#include "preTransform.inc"

	cudaTextureFilterMode cudaFilterMode = cudaFilterModePoint;

	__global__ void monteKarloKernel(float* __restrict__ rOut, float* __restrict__ gOut, float* __restrict__ bOut, int widthOut, int pitchOut, int heightOut, float xMin, float yMin) {
		const int x = threadIdx.x + blockIdx.x * blockDim.x;
		const int y = threadIdx.y + blockIdx.y * blockDim.y;
		if (x >= widthOut || y >= heightOut) {
			return;
		}

		const float x0 = x;
		const float y0 = y;

		float resR = 0;
		float resG = 0;
		float resB = 0;

		const Point3CUDA point1 = getConversion(Point3CUDA(x0, y0));

		for (int k = 0; k < COUNT_ITERATION; k++) {
			const Point3CUDA point2 = Point3CUDA(randomsCuda[3 * k], randomsCuda[3 * k + 1], randomsCuda[3 * k + 2]);

			const Point3CUDA pointResult = point1 + point2;
			const float xResF = floorf(pointResult.getX());
			const float yResF = floorf(pointResult.getY());

			resR += tex2D(texRefR, xResF, yResF);
			resG += tex2D(texRefG, xResF, yResF);
			resB += tex2D(texRefB, xResF, yResF);
		}

		resR /= COUNT_ITERATION;
		resG /= COUNT_ITERATION;
		resB /= COUNT_ITERATION;

		clampCUDA(resR);
		clampCUDA(resG);
		clampCUDA(resB);

		rOut[x + y * pitchOut] = resR;
		gOut[x + y * pitchOut] = resG;
		bOut[x + y * pitchOut] = resB;
	}

	inline void beginTransform(const LinearConversion &sc) {
		std::cout << "Start Monte-Karlo CUDA\n";

		unsigned int randInitiate = static_cast<unsigned int>(time(NULL));
		std::cout << "rand initiate " << randInitiate << "\n";
		srand(randInitiate);

		randoms.clear();
		for (int i = 0; i < COUNT_ITERATION; i++) {
			const float x = nextRandom();
			const float y = nextRandom();

			const Point3 point = sc.getConversion(Point3(x, y, 0.f));

			randoms.push_back(point.x_());
			randoms.push_back(point.y_());
			randoms.push_back(point.z_());
		}

		check(randoms.size() / 3 == COUNT_ITERATION);

		checkCudaCall(cudaMemcpyToSymbol(randomsCuda, &randoms[0], COUNT_ITERATION * 3 * sizeof(float), 0, cudaMemcpyHostToDevice));
	}

	inline void work(int width, int height, float *rOutDevice, float *gOutDevice, float *bOutDevice, const int outWidth, const int outPitchElements, const int outHeight, const float outXMin, const float outYMin) {
		checkCudaCall(cudaFuncSetCacheConfig(monteKarloKernel, cudaFuncCachePreferL1));
		dim3 block(32, 8);
		dim3 grid((outWidth + block.x - 1) / block.x, (outHeight + block.y - 1) / block.y);
		checkAndGetTimeKernelRun(monteKarloKernel <<<grid, block>>>(rOutDevice, gOutDevice, bOutDevice, outWidth, outPitchElements, outHeight, outXMin, outYMin));
	}

#include "transform.inc"

}
