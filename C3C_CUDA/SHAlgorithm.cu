#include "Algorithms.h"

#include "commonCUDA.h"
#include "ConversionCUDA.h"

namespace SH {

#include "preTransform.inc"

cudaTextureFilterMode cudaFilterMode = cudaFilterModePoint;

class Polygon {
private:

	static const int MAX_POINTS = 10;

	PointCUDA points[MAX_POINTS];

	int size_;

public:

	__device__ Polygon()
		: size_(0)
	{}

	__device__ void push_back(const PointCUDA &p) {
		checkKernel(0 <= size_ && size_ < MAX_POINTS);
		points[size_] = p;
		size_++;
	}

	__device__ PointCUDA& operator[] (int index) {
		checkKernel(0 <= index && index < size_);
		return points[index];
	}

	__device__ const PointCUDA operator[] (int index) const {
		checkKernel(0 <= index && index < size_);
		return points[index];
	}

	__device__ int size() const {
		return size_;
	}

};

__device__  float sqalar(const PointCUDA &p1, const PointCUDA &p2) {
	return p1.x * p2.x + p1.y * p2.y;
}

__device__ Polygon otsech(const Polygon &sq, const PointCUDA &line0, const PointCUDA &line1, const PointCUDA &n) {
	const float EPSILON = 1e-5;

	PointCUDA vectLine(line1 - line0);
	checkKernel(std::abs(sqalar(vectLine, n)) < EPSILON); // ��� - �������.

	Polygon result;
	if (sq.size() == 0) {
		return result;
	}

	const PointCUDA vecPointLine(sq[0] - line1);
	float sqalarVecPointLine = sqalar(vecPointLine, n);
	const int sqSize = sq.size();
	for (int i = 0; i < sqSize; i++) {
		// ������ �� ����� ������ �� ������.
		if (sqalarVecPointLine >= 0) {
			// ����� ����������� �������������.
			result.push_back(sq[i]);
		}

		// ���� ����� ����������� 2-� ������
		const PointCUDA &p0 = sq[i];
		const PointCUDA &p1 = sq[i + 1 == sqSize ? 0 : i + 1];
		PointCUDA vec2PointLine(p1 - line1);
		const float sqalarVec2PointLine = sqalar(vec2PointLine, n);
		if (sqalarVec2PointLine * sqalarVecPointLine < 0.f) {
			const float a1 = p0.y - p1.y;
			const float b1 = -(p0.x - p1.x);
			const float c1 = p0.y * -b1 - p0.x * a1;

			const float a2 = line0.y - line1.y;
			const float b2 = -(line0.x - line1.x);
			const float c2 = line0.y * -b2 - line0.x * a2;

			const float znam = a1 * b2 - a2 * b1;

			const float x = -(c1 * b2 - c2 * b1) / znam;
			const float y = -(a1 * c2 - a2 * c1) / znam;

			result.push_back(PointCUDA(x, y));
		}
		sqalarVecPointLine = sqalarVec2PointLine;
	}

	return result;
}

__device__  float calcSquareTriangle(const PointCUDA &v1, const PointCUDA &v2) {
	return std::abs(v1.x * v2.y - v2.x * v1.y) / 2.f;
}

__device__ float calkSquare(const Polygon &sq) {
	float result = 0;
	if (sq.size() >= 3) {
		const PointCUDA &v0 = sq[0];
		PointCUDA vect1 = sq[1] - v0;
		for (int i = 2; i < sq.size(); i++) {
			const PointCUDA vect2 = sq[i] - v0;
			result += calcSquareTriangle(vect2, vect1);
			vect1 = vect2;
		}
	}

	return result;
}

__device__ float getSquare(const Polygon &sq1, const Polygon &sq2) {
	checkKernel(sq1.size() == 4);
	checkKernel(sq2.size() == 4);
	// ���������, ��� ������ ������ - ������������� � �������, ������������� ����� ����.
	if (sq2[0].x == sq2[1].x) {
		checkKernel(sq2[0].x == sq2[1].x);
		checkKernel(sq2[1].y == sq2[2].y);
		checkKernel(sq2[2].x == sq2[3].x);
		checkKernel(sq2[3].y == sq2[0].y);
	} else {
		checkKernel(sq2[0].y == sq2[1].y);
		checkKernel(sq2[1].x == sq2[2].x);
		checkKernel(sq2[2].y == sq2[3].y);
		checkKernel(sq2[3].x == sq2[0].x);
	}
	Polygon res = otsech(sq1, sq2[0], sq2[1], sq2[2] - sq2[1]);
	res = otsech(res, sq2[1], sq2[2], sq2[3] - sq2[2]);
	res = otsech(res, sq2[2], sq2[3], sq2[0] - sq2[3]);
	res = otsech(res, sq2[3], sq2[0], sq2[1] - sq2[0]);

	const float result = calkSquare(res);
	return result;
}

__device__  Polygon getNewFigure(const float xMin_, const float yMin_, const float xMax_, const float yMax_) {
	const PointCUDA p1 = getConversion(xMin_, yMin_);
	const PointCUDA p2 = getConversion(xMin_, yMax_);
	const PointCUDA p3 = getConversion(xMax_, yMax_);
	const PointCUDA p4 = getConversion(xMax_, yMin_);

	Polygon sq1;
	sq1.push_back(p1);
	sq1.push_back(p2);
	sq1.push_back(p3);
	sq1.push_back(p4);

	return sq1;
}

__global__ void workSH(int width, int height, float* __restrict__ rOut, float* __restrict__ gOut, float* __restrict__ bOut, int widthOut, int pitchOut, int heightOut, float xMinOut, float yMinOut) {
	const int x = threadIdx.x + blockIdx.x * blockDim.x;
	const int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= widthOut || y >= heightOut) {
		return;
	}

	const float x0 = x;
	const float y0 = y;

	const float x1 = x0 + 1.f;
	const float y1 = y0 + 1.f;

	Polygon sq1 = getNewFigure(x0, y0, x1, y1);

	const float x1Min = fminf(sq1[0].x, fminf(sq1[1].x, fminf(sq1[2].x, sq1[3].x)));
	const float x1Max = fmaxf(sq1[0].x, fmaxf(sq1[1].x, fmaxf(sq1[2].x, sq1[3].x)));
	const float y1Min = fminf(sq1[0].y, fminf(sq1[1].y, fminf(sq1[2].y, sq1[3].y)));
	const float y1Max = fmaxf(sq1[0].y, fmaxf(sq1[1].y, fmaxf(sq1[2].y, sq1[3].y)));

	const int xMin = (int)fmaxf(0.f, x1Min);
	const int xMax = (int)fminf(x1Max + 1.f, width);
	const int yMin = (int)fmaxf(0.f, y1Min);
	const int yMax = (int)fminf(y1Max + 1.f, height);
	// �� �������� ����� ���������� +1 !!!

	const float square0 = calkSquare(sq1);

	float resR = 0;
	float resG = 0;
	float resB = 0;

	float rSquare = 0;
	for (int m = xMin; m < xMax; m++) {
		for (int n = yMin; n < yMax; n++) {
			const float xIn0 = m;
			const float yIn0 = n;
			const float xIn1 = xIn0 + 1.f;
			const float yIn1 = yIn0 + 1.f;

			Polygon sq2;
			sq2.push_back(PointCUDA(xIn0, yIn0));
			sq2.push_back(PointCUDA(xIn0, yIn1));
			sq2.push_back(PointCUDA(xIn1, yIn1));
			sq2.push_back(PointCUDA(xIn1, yIn0));

			const float square = getSquare(sq1, sq2);
			rSquare += square;

			resR += square * tex2D(texRefR, xIn0, yIn0);
			resG += square * tex2D(texRefG, xIn0, yIn0);
			resB += square * tex2D(texRefB, xIn0, yIn0);
		}
	}

	const float square0Tmp = 1.f / square0;

	resR *= square0Tmp;
	resG *= square0Tmp;
	resB *= square0Tmp;

	rSquare *= square0Tmp;
	// �� ����, rSquare ������ ���� ����� 1, �� � ���� ������������ ������������ ������ �� ������ ���� ��������.
	// � ����� ���� ������ 1, �� ��� ���������.
	if (rSquare > 1.f) {
		resR /= rSquare;
		resG /= rSquare;
		resB /= rSquare;
	}

	clampCUDA(resR);
	clampCUDA(resG);
	clampCUDA(resB);

	rOut[x + y * pitchOut] = resR;
	gOut[x + y * pitchOut] = resG;
	bOut[x + y * pitchOut] = resB;
}

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start SH interpolation CUDA\n";
}

inline void work(const int width, const int height, float *rOutDevice, float *gOutDevice, float *bOutDevice, const int outWidth, const int outPitchElements, const int outHeight, const float outXMin, const float outYMin) {
	checkCudaCall(cudaFuncSetCacheConfig(workSH, cudaFuncCachePreferL1));
	dim3 block(32, 2);
	dim3 grid((outWidth + block.x - 1) / block.x, (outHeight + block.y - 1) / block.y);
	checkAndGetTimeKernelRun(workSH <<<grid, block>>>(width, height, rOutDevice, gOutDevice, bOutDevice, outWidth, outPitchElements, outHeight, outXMin, outYMin));
}

#include "transform.inc"

}
