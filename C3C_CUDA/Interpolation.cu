#include "Algorithms.h"

#include "ConversionCUDA.h"
#include "commonCUDA.h"

namespace bilinearInterpolation {

#include "preTransform.inc"

cudaTextureFilterMode cudaFilterMode = cudaFilterModeLinear;

__global__ void runKernel(float* __restrict__ rOut, float* __restrict__ gOut, float* __restrict__ bOut, int widthOut, int pitchOut, int heightOut) {
	const int x = threadIdx.x + blockIdx.x * blockDim.x;
	const int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= widthOut || y >= heightOut) {
		return;
	}

	const float x0 = x;
	const float y0 = y;

	const PointCUDA pointNew = getConversion(x0, y0);
	const float xNew = floorf(pointNew.x);
	const float yNew = floorf(pointNew.y);

	const float rRes = tex2D(texRefR, xNew, yNew);
	const float gRes = tex2D(texRefG, xNew, yNew);
	const float bRes = tex2D(texRefB, xNew, yNew);

	rOut[x + y * pitchOut] = rRes;
	gOut[x + y * pitchOut] = gRes;
	bOut[x + y * pitchOut] = bRes;
}

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start bilinear interpolation2 CUDA\n";
}

inline void work(int width, int height, float *rOutDevice, float *gOutDevice, float *bOutDevice, const int outWidth, const int outPitchElements, const int outHeight, const float outXMin, const float outYMin) {
	checkCudaCall(cudaFuncSetCacheConfig(runKernel, cudaFuncCachePreferL1));
	dim3 block(32, 8);
	dim3 grid((outWidth + block.x - 1) / block.x, (outHeight + block.y - 1) / block.y);
	checkAndGetTimeKernelRun(runKernel <<<grid, block>>>(rOutDevice, gOutDevice, bOutDevice, outWidth, outPitchElements, outHeight));
}

#include "transform.inc"

}

namespace bicubicInterpolation {

#include "preTransform.inc"

cudaTextureFilterMode cudaFilterMode = cudaFilterModePoint;

__device__ float cubicInterpolate(float *p, float x) {
	return p[1] + (-0.5f * p[0] + 0.5f * p[2]) * x
		+ (p[0] - 2.5f * p[1] + 2.0f * p[2] - 0.5f * p[3]) * x * x
		+ (-0.5f * p[0] + 1.5f * p[1] - 1.5f * p[2] + 0.5f * p[3]) * x * x * x;
}

__device__ float bicubicInterpolate(float *p, float x, float y) {
	float tmp[4];
	tmp[0] = cubicInterpolate(&p[0 * 4], x);
	tmp[1] = cubicInterpolate(&p[1 * 4], x);
	tmp[2] = cubicInterpolate(&p[2 * 4], x);
	tmp[3] = cubicInterpolate(&p[3 * 4], x);
	return cubicInterpolate(tmp, y);
}

__device__ float bicubickInterpolation(float *p, float u, float t, int width, int height) {
	float result = bicubicInterpolate(p, u, t);
	clampCUDA(result);
	return result;
}

__global__ void bicubickKernel(int width, int height, float* __restrict__ rOut, float* __restrict__ gOut, float* __restrict__ bOut, int widthOut, int pitchOut, int heightOut, float xMin, float yMin) {
	const int x = threadIdx.x + blockIdx.x * blockDim.x;
	const int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= widthOut || y >= heightOut) {
		return;
	}

	const float x0 = static_cast<float>(x);
	const float y0 = static_cast<float>(y);

	const PointCUDA pointNew = getConversion(x0, y0);
	const float xNew = pointNew.x;
	const float yNew = pointNew.y;

	const float xInt = floorf(xNew);
	const float yInt = floorf(yNew);

	float pR[4 * 4];
	float pG[4 * 4];
	float pB[4 * 4];
#pragma unroll 4
	for (int yInc = -1; yInc <= 2; yInc++) {
#pragma unroll 4
		for (int xInc = -1; xInc <= 2; xInc++) {
			pR[(xInc + 1) + (yInc + 1) * 4] = tex2D(texRefR, xInt + xInc, yInt + yInc);
			pG[(xInc + 1) + (yInc + 1) * 4] = tex2D(texRefG, xInt + xInc, yInt + yInc);
			pB[(xInc + 1) + (yInc + 1) * 4] = tex2D(texRefB, xInt + xInc, yInt + yInc);
		}
	}

	const float rRes = bicubickInterpolation(pR, xNew - xInt, yNew - yInt, width, height);
	const float gRes = bicubickInterpolation(pG, xNew - xInt, yNew - yInt, width, height);
	const float bRes = bicubickInterpolation(pB, xNew - xInt, yNew - yInt, width, height);

	rOut[x + y * pitchOut] = rRes;
	gOut[x + y * pitchOut] = gRes;
	bOut[x + y * pitchOut] = bRes;
}

inline void beginTransform(const LinearConversion &sc) {
	std::cout << "Start bicubic interpolation CUDA\n";
}

inline void work(int width, int height, float *rOutDevice, float *gOutDevice, float *bOutDevice, const int outWidth, const int outPitchElements, const int outHeight, const float outXMin, const float outYMin) {
	checkCudaCall(cudaFuncSetCacheConfig(bicubickKernel, cudaFuncCachePreferL1));
	dim3 block(32, 8);
	dim3 grid((outWidth + block.x - 1) / block.x, (outHeight + block.y - 1) / block.y);
	checkAndGetTimeKernelRun(bicubickKernel<<<grid, block>>>(width, height, rOutDevice, gOutDevice, bOutDevice, outWidth, outPitchElements, outHeight, outXMin, outYMin));
}

#include "transform.inc"

}
