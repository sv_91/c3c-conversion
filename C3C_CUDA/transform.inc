Image transform(const Image &image1, const LinearConversion &sc) {
	Image imageOut(image1.getWidth(), image1.getHeight(), sc.getObr());

	beginTransform(sc);

	// ������ �����������
	std::vector<std::vector<float> > matrix = sc.getMatrix();
	float *matrixCPU = new float[3 * 3];
	for (int i = 0; i < matrix.size(); i++) {
		for (int j = 0; j < matrix[i].size(); j++) {
			matrixCPU[i + j * 3] = matrix[i][j];
		}
	}
	checkCudaCall(cudaMemcpyToSymbol(matrixTransform, matrixCPU, 3 * 3 * sizeof(float), 0, cudaMemcpyHostToDevice));
	delete[] matrixCPU;


	// �������� ������
	float *rOutDevice;
	size_t pitchOut;
	checkCudaCall(cudaMallocPitch(&rOutDevice, &pitchOut, imageOut.width * sizeof(float), imageOut.height));

	float *gOutDevice;
	checkCudaCall(cudaMallocPitch(&gOutDevice, &pitchOut, imageOut.width * sizeof(float), imageOut.height));

	float *bOutDevice;
	checkCudaCall(cudaMallocPitch(&bOutDevice, &pitchOut, imageOut.width * sizeof(float), imageOut.height));


	// ������� ��������
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(sizeof(float) * 8, 0, 0, 0, cudaChannelFormatKindFloat);

	// ������� ������
	cudaArray* cuArrayR;
	checkCudaCall(cudaMallocArray(&cuArrayR, &channelDesc, image1.width, image1.height));
	checkCudaCall(cudaMemcpy2DToArray(cuArrayR, 0, 0, image1.r, image1.width * sizeof(float), image1.width * sizeof(float), image1.height, cudaMemcpyHostToDevice));

	cudaArray* cuArrayG;
	checkCudaCall(cudaMallocArray(&cuArrayG, &channelDesc, image1.width, image1.height));
	checkCudaCall(cudaMemcpy2DToArray(cuArrayG, 0, 0, image1.g, image1.width * sizeof(float), image1.width * sizeof(float), image1.height, cudaMemcpyHostToDevice));

	cudaArray* cuArrayB;
	checkCudaCall(cudaMallocArray(&cuArrayB, &channelDesc, image1.width, image1.height));
	checkCudaCall(cudaMemcpy2DToArray(cuArrayB, 0, 0, image1.b, image1.width * sizeof(float), image1.width * sizeof(float), image1.height, cudaMemcpyHostToDevice));

	texRefR.addressMode[0] = cudaAddressModeBorder;
	texRefR.addressMode[1] = cudaAddressModeBorder;
	texRefR.filterMode = cudaFilterMode;

	texRefG.addressMode[0] = texRefR.addressMode[0];
	texRefG.addressMode[1] = texRefR.addressMode[1];
	texRefG.filterMode = texRefR.filterMode;

	texRefB.addressMode[0] = texRefR.addressMode[0];
	texRefB.addressMode[1] = texRefR.addressMode[1];
	texRefB.filterMode = texRefR.filterMode;

	checkCudaCall(cudaBindTextureToArray(texRefR, cuArrayR, channelDesc));
	checkCudaCall(cudaBindTextureToArray(texRefG, cuArrayG, channelDesc));
	checkCudaCall(cudaBindTextureToArray(texRefB, cuArrayB, channelDesc));


	// ������ ����
	work(image1.width, image1.height, rOutDevice, gOutDevice, bOutDevice, imageOut.width, static_cast<int>(pitchOut / sizeof(float)), imageOut.height, imageOut.xMin, imageOut.yMin);


	// ����������� ����������.
	checkCudaCall(cudaMemcpy2D(imageOut.r, imageOut.width * sizeof(float), rOutDevice, pitchOut, imageOut.width * sizeof(float), imageOut.height, cudaMemcpyDeviceToHost));
	checkCudaCall(cudaMemcpy2D(imageOut.g, imageOut.width * sizeof(float), gOutDevice, pitchOut, imageOut.width * sizeof(float), imageOut.height, cudaMemcpyDeviceToHost));
	checkCudaCall(cudaMemcpy2D(imageOut.b, imageOut.width * sizeof(float), bOutDevice, pitchOut, imageOut.width * sizeof(float), imageOut.height, cudaMemcpyDeviceToHost));


	// �����������
	checkCudaCall(cudaFreeArray(cuArrayR));
	checkCudaCall(cudaFreeArray(cuArrayG));
	checkCudaCall(cudaFreeArray(cuArrayB));
	checkCudaCall(cudaFree(rOutDevice));
	checkCudaCall(cudaFree(gOutDevice));
	checkCudaCall(cudaFree(bOutDevice));

	return imageOut;
}
