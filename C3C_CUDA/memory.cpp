#include "commonCUDA.h"

#include "Image.h"

unsigned char* alloc(const int sizeBytes) {
	void *ptr;
	checkCudaCall(cudaHostAlloc(&ptr, sizeBytes, cudaHostAllocDefault | cudaHostAllocWriteCombined));
	return reinterpret_cast<unsigned char*>(ptr);
}

void memcpyArray(unsigned char* dst, unsigned char *src, int sizeBytes) {
	checkCudaCall(cudaMemcpy(dst, src, sizeBytes, cudaMemcpyHostToHost));
}

void freeMemory(void* memory) {
	if (memory != NULL) {
		checkCudaCall(cudaFreeHost(memory));
	}
}
