#pragma once

#include "common.h"

#include <cuda_runtime.h>
#include <texture_fetch_functions.h>
#include <cuda_texture_types.h>
#include <texture_indirect_functions.h>
#include <cuda.h>

__inline__ __device__ void clampCUDA(float &f) {
	if (f < 0.f) {
		f = 0.f;
	} // �� ����������� else! �� ����������� �����������.
	if (f > 1.f) {
		f = 1.f;
	}
}

static volatile __device__ int kernelErrorDevice = 0;

#define checkCudaCall(function, ...) { \
	cudaError_t result = function; \
	check2(result == cudaSuccess, (std::string(#function) + std::string(cudaGetErrorString(result)) + std::string(" ") + std::string(##__VA_ARGS__)).c_str()); \
}

#define printKernelError() { \
	int error; \
	checkCudaCall(cudaMemcpyFromSymbol((void**)&error, kernelErrorDevice, sizeof(kernelErrorDevice), 0, cudaMemcpyDeviceToHost)); \
	if (error != 0) { \
		int buf = 0; \
		checkCudaCall(cudaMemcpyToSymbol(kernelErrorDevice, &buf, sizeof(kernelErrorDevice), 0, cudaMemcpyHostToDevice)); \
		check2(false, (std::string("Kernel error ") + intToStr(error)).c_str()); \
	} \
}

#define checkKernel(v) {\
	if (!(v)) { \
		if (kernelErrorDevice == 0) { \
			kernelErrorDevice = __LINE__; \
		} \
	} \
}

#define checkAndGetTimeKernelRun(function, ...) { \
	cudaEvent_t start, stop; \
	cudaEventCreate(&start); \
	cudaEventCreate(&stop); \
	cudaEventRecord(start, 0); \
	\
	function, ##__VA_ARGS__; \
	\
	cudaEventRecord(stop, 0); \
	cudaThreadSynchronize(); \
	cudaEventSynchronize(stop); \
	float elapsedTime; \
	cudaEventElapsedTime(&elapsedTime, start, stop); \
	printf("Time %-50s %f\n", #function, elapsedTime); \
	\
	cudaError_t result = cudaGetLastError(); \
	check2(result == 0, (std::string(cudaGetErrorString(result)) + std::string(" ") + std::string(#function)).c_str()); \
	printKernelError(); \
	\
	cudaEventDestroy(start); \
	cudaEventDestroy(stop); \
}
